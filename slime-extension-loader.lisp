(require :slime-events)
(require :asdf)
(require :swank)

(defmethod asdf:operate :after ((operation asdf:load-op) system &key)
  (let ((slime-extension-file (asdf:system-relative-pathname system "slime-extension.el")))
    (when (probe-file slime-extension-file)
      (swank:ed-rpc 'slime-load-extension (princ-to-string slime-extension-file)))))

(provide :slime-extension-loader)
