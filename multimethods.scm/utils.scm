(define (plist-ref property-list key . default)
  (cond ((null? property-list) (if (not (null? default))
                                   (car default)
                                   #f))
        ((null? (cdr property-list))
         (error 'chezcl (format "Malformed property list: ~s" property-list)))
        ((equal? key (car property-list))
         (cadr property-list))
        (else (apply plist-ref (cddr property-list) key default))))

;; (plist-ref '(a 22) 'a)
;; (plist-ref '(a 22) 'b)
;; (plist-ref '(a 22) 'b 'no)

(define (identity x)
  x)

(define-record cell
  (value))

;; (define *c* (make-cell 22))
;; (cell-value *c*)
;; (set-cell-value! *c* 44)
;; (cell-value *c*)

(define (flatten lst)
  (if (null? lst)
      lst
      (if (list? (car lst))
          (append (car lst) (flatten (cdr lst)))
          (cons (car lst) (flatten (cdr lst))))))

;; (flatten '(1 2 3))
;; (flatten '(1 (2 3)))
;; (flatten '((1 2) 3))

(define (alist-update alist key value)
  (cond
   [(null? alist)
    (list (list key value))]
   [(equal? (caar alist) key)
    (cons (list key value) (cdr alist))]
   [else
    (cons (car alist)
          (alist-update (cdr alist) key value))]))

(let ((alist '((a . 22) (b . 33))))
  (set! alist (alist-update alist 'a 44))
  alist)

(define (some? predicate lst)
  (call/cc
   (lambda (ret)
     (for-each
      (lambda (x)
        (when (predicate x)
          (ret #t)))
      lst)
     #f)))

;; (some? odd? '(2 3 4))
;; (some? odd? '(2 4))

(define (every? predicate lst)
  (not (some? (lambda (x)
                (not (predicate x)))
              lst)))

;; (every? odd? '(1 2 3))
;; (every? odd? '(1 3 7))
