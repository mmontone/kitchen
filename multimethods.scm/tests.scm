;; Example from: https://clojure.org/about/runtime_polymorphism

(defgeneric (encounter x y))

(defmethod (encounter (bunny bunny) (lion lion))
  'run-away)

(defmethod (encounter (lion lion) (bunny bunny))
  'eat)

(defmethod (encounter (l1 lion) (l2 lion))
  'fight)

(defmethod (encounter (b1 bunny) (b2 bunny))
  'mate)

(encounter 'bunny 'lion)
(encounter 'lion 'bunny)
(encounter 'lion 'lion)
(encounter 'bunny 'bunny)

;; (find-generic-function 'encounter)

;; https://www.rangakrish.com/index.php/2015/10/13/multimethods-in-lisp/

(is-a! 'asteroid 'space-object)
(is-a! 'spaceship 'space-object)
(is-a! 'planet 'space-object)
(is-a! 'mars 'planet)

(defgeneric (collide obj1 obj2))

(defmethod (collide (obj1 space-object) (obj2 space-object))
  "space-object collides with space-object")

(defmethod (collide (asteroid asteroid) (planet planet))
  "asteroid collides with planet")

(collide 'planet 'mars)
(collide 'asteroid 'mars)
(collide 'asteroid 'planet)

;; From: https://dl.acm.org/doi/pdf/10.1145/202176.202182

(defgeneric (process x y)
  'accessor type-of)

(defmethod (process (a integer?) (b integer?))
  "processing integers")

(defmethod (process (a real?) (b real?))
  "processing reals")

(process 2 4)
(process 2.3 4.5)
(process 2.3 4)
(process 2 4.5)

(defgeneric (add x y)
  'accessor type-of)

(defmethod (add (x number?) (y number?))
  (+ x y))

(defmethod (add (x string?) (y string?))
  (string-append x y))

(add 3 4)
(add "auto" "mobile")
