;; https://dl.acm.org/doi/pdf/10.1145/202176.202182
;; https://clojure.org/reference/multimethods
;; https://clojure.org/about/runtime_polymorphism

(define *generic-functions*
  (make-hash-table))

(define-record generic-function
  (name
   args
   doc
   ;; methods is an association list with types of arguments as key
   ;; hashtables in Chez scheme don't support equal?
   methods
   accessor
   classifier
   hierarchy
   dispatcher
   sorted-methods))

(define-record method
  (name
   qualifiers
   args
   func))

;; hierarchy modelled as a list of pairs
(define (make-hierarchy)
  (make-cell '()))

(define (is-a! x y . hierarchy)
  (let ((hierarchy (if (null? hierarchy)
                       *default-hierarchy*
                       (car hierarchy))))
    (set-cell-value! hierarchy
                     (cons (cons x y) (cell-value hierarchy)))))

(define (is-a? x y . hierarchy)
  (if (eqv? x y)
      #t
      (let ((hierarchy (if (null? hierarchy)
                           *default-hierarchy*
                           (car hierarchy))))
        (and (find (lambda (x) (eq? y x)) (ancestors x))
             #t))))

(define (direct-ancestors x . hierarchy)
  (let ((hierarchy (if (null? hierarchy)
                       *default-hierarchy*
                       (car hierarchy))))
    (map cdr
         (filter (lambda (entry)
                   (eq? (car entry) x))
                 (cell-value hierarchy)))))

(define (ancestors x . hierarchy)
  (let ((hierarchy (if (null? hierarchy)
                       *default-hierarchy*
                       (car hierarchy))))
    (let ((x-direct-ancestors (direct-ancestors x hierarchy)))
      (apply append x-direct-ancestors (map ancestors x-direct-ancestors)))))

(define (direct-descendants x . hierarchy)
  (let ((hierarchy (if (null? hierarchy)
                       *default-hierarchy*
                       (car hierarchy))))
    (map cdr
         (filter (lambda (entry)
                   (eq? (cdr entry) x))
                 (cell-value hierarchy)))))

(define (descendants x . hierarchy)
  (let ((hierarchy (if (null? hierarchy)
                       *default-hierarchy*
                       (car hierarchy))))
    (let ((x-direct-descendants (direct-descendants x hierarchy)))
      (apply append x-direct-descendants (map descendants x-direct-descendants)))))

(define (match-first-method gf args)
  (error 'match-first-method "TODO"))

;; This is to be used as generic-function accessor
(define (type-of object)
  (cond
   ((symbol? object) 'symbol?)
   ((boolean? object) 'boolean?)
   ((pair? object) 'pair?)
   ((list? object) 'list?)
   ((integer? object) 'integer?)
   ((rational? object) 'rational?)
   ((complex? object) 'complex?)
   ((real? object) 'real?)
   ((number? object) 'number?)
   ((string? object) 'string?)
   ((vector? object) 'vector?)
   ((char? object) 'char?)
   ((record? object) 'record?)
   ((hashtable? object) 'hashtable?)
   (else 'datum?)))

(define (register-generic-function name args . options)
  (let ((gf (make-generic-function
             name args
             (plist-ref options 'documentation "")
             '() ;; methods
             (plist-ref options 'accessor identity)
             (plist-ref options 'classifier is-a?)
             (plist-ref options 'hierarchy #f)
             (plist-ref options 'dispatcher dispatch)
             '() ;; sorted methods
             )))
    (hashtable-set! *generic-functions* name gf)
    gf))

(define-syntax defgeneric
  (syntax-rules ()
    ((_ (name args ...) options ...)
     (begin
       (register-generic-function 'name '(args ...) options ...)
       (define (name args ...)
         (dispatch 'name args ...))))))

(define (find-generic-function name)
  (or (hashtable-ref *generic-functions* name #f)
      (error 'find-generic-function (format "Generic function not defined: ~s" name) name)))

;; First Match versus Best Match
;; Given subtyping, it is possible for more than one method of a generic function to match a provided set of
;; arguments. The dispatcher described will select the first match it finds. It will favor matching a specific type over a
;; general type early in the argument list over a match that may contain several better matches later in the argument
;; list. This behavior is subtly different than that of other languages, but it has the definite advantage of being more
;; efficient and simpler to implement. Also, it is worth noting that it (serendipitously) models object-oriented
;; methodology better than 'straight' best match. Methods are typically written in a way that considers the first
;; argument to be the object to which the the method belongs. First match gives the first argument the greatest weight
;; in determining which method is called

;; Sort methods from more specific to less
;; TODO: consider a more efficient tree-structure for sorted methods.
;; See "Method Database" in https://dl.acm.org/doi/pdf/10.1145/202176.202182

(define (method-more-specific? gf m1 m2)
  (define (more-specific? t1 t2)
    ((generic-function-classifier gf) t1 t2))
  (define (method-args-more-specific? args1 args2)
    (if (null? args1)
        #t
        (let ((arg1 (car args1))
              (arg2 (car args2)))
          (or (more-specific? arg1 arg2)
              (and (eqv? arg1 arg2)
                   (method-args-more-specific? (cdr args1) (cdr args2)))))))
  (method-args-more-specific? (method-args m1) (method-args m2)))

(define (sort-methods gf)
  (let ((methods (generic-function-methods gf)))
    (set-generic-function-sorted-methods!
     gf
     (list-sort (lambda (m1 m2)
                  (method-more-specific? gf m1 m2))
                (map cadr (generic-function-methods gf))))))

(define (method-appliable? gf method args)
  (let ((classifier (generic-function-classifier gf))
        (accessor (generic-function-accessor gf)))
    (every? (lambda (arg)
              (classifier (car arg) (cdr arg)))
            (map cons
                 (map accessor args)
                 (method-args method)))))

(define (dispatch gf-name . args)
  (let* ((gf (find-generic-function gf-name))
         (method?
          (call/cc
           (lambda (return)
             (for-each (lambda (method)
                         (when (method-appliable? gf method args)
                           (return method)))
                       (generic-function-sorted-methods gf))
             #f))))
    (if (not method?)
        (error 'dispatch (format "Can't dispatch: ~a with: ~s"
                                 (generic-function-name gf)
                                 args))
        (apply (method-func method?) args))))

(define (add-method method gf)
  ;; add method to generic function
  (set-generic-function-methods!
   gf
   (alist-update (generic-function-methods gf)
                 (method-args method)
                 method))
  (sort-methods gf))

(define (register-method name args func)
  (let ((gf (find-generic-function name)))
    (add-method (make-method name '() args func) gf)))

(define-syntax defmethod
  (syntax-rules ()
    ((_ (name (a a-type) ...) body ...)
     (register-method 'name '(a-type ...) (lambda (a ...) body ...)))))

;; initialize

(define *default-hierarchy* (make-hierarchy))

(is-a! 'integer? 'rational?)
(is-a! 'rational? 'real?)
(is-a! 'real? 'complex?)
(is-a! 'complex? 'number?)
(is-a! 'list? 'pair?)

(direct-ancestors 'integer?)
(ancestors 'integer?)

(is-a? 'integer? 'rational?)
(is-a? 'rational? 'integer?)

(is-a? 'integer? 'real?)
(is-a? 'real? 'integer?)

(is-a? 'pair? 'string?)
