(defpackage :py4cl-sugar
  (:export #:with-sugar)
  (:use :cl))

(in-package :py4cl-sugar)

(defun python-name (symbol)
  (symbol-munger:lisp->camel-case symbol))

(defun sugar-form (form)
  (cond
    ((symbolp form)
     form)
    ((and (listp form)
          (symbolp (car form))
          (str:starts-with-p #\/ (symbol-name (car form))))
     ;; call syntax
     (destructuring-bind (fname &rest args) form
       `(py4cl:python-call ,(python-name (str:substring 1 nil (symbol-name fname)))
                           ,@(mapcar #'sugar-form args))))
    ((and (listp form)
          (symbolp (car form))
          (str:ends-with-p #\. (symbol-name (car form))))
     ;; class instantiation
     ;; (class.) => (new 'class)
     (error "TODO")
     (destructuring-bind (class-name &rest args)
         `(py4cl:python-call ,(intern (str:substring (symbol-name (car form)))))))
    ((and (listp form)
          (symbolp (car form))
          (str:starts-with-p ".-" (symbol-name (car form))))
     ;; property access
     `(py4cl:python-getattr ',(cadr form)
                            ,(intern (str:substring 2 nil (symbol-name (car form)))
                                     (symbol-package (car form)))))
    ((and (listp form)
          (symbolp (car form))
          (str:starts-with-p #\. (symbol-name (car form))))
     ;; (.method obj &rest args)
     (destructuring-bind (method obj &rest args) form
       (let ((method-name (intern
                           (str:substring 1 nil (symbol-name method))
                           (symbol-package method))))
         `(py4cl:python-method ,obj ,(python-name method-name) ,@args))))
    ((and (listp form)
          (symbolp (car form))
          (find #\. (symbol-name (car form))))
     ;; (obj.foo.bar &rest args)
     (destructuring-bind (access-chain &rest args) form
       (let ((chain-elems (mapcar (alexandria:rcurry #'intern (symbol-package access-chain))
                                  (str:split #\. (symbol-name access-chain)))))
         `(arrows:-<> ,(car chain-elems)
                      ,@(mapcar (lambda (field)
                                  `(py4cl:python-getattr <> ',field))
                                (butlast (rest chain-elems)))
                      (py4cl:python-method <>
                                           ,(python-name (alexandria:lastcar chain-elems))
                                           ,@(mapcar #'sugar-form args))))))
    ((listp form)
     (mapcar #'sugar-form form))
    (t form)))

;; (sugar-form '(.write sw "Hello"))
;; (sugar-form '(sw.write "Hello"))
;; (sugar-form '(.-foo sw))
;; (sugar-form '(/print "lala"))
;; (sugar-form '(let ((x 22)) x))

(defmacro with-sugar (&body body)
  (sugar-form `(progn ,@body)))

;;(py4cl:import-module "numpy" :as "np")
;;(py4cl:import-module "scipy.integrate" :as "integrate")

;; (let ((my-str "testing"))
;;   (py4cl:python-call "len" my-str))

#+test
(with-sugar
  (let ((my-str "testing"))
    (/len my-str)))

#+test
(with-sugar
  (/print "lala"))

#+test
(with-sugar
  (.format "hello {0}" "world"))

#+test
(with-sugar
  (let ((str "hello {0}"))
    (str.format  "world")))
