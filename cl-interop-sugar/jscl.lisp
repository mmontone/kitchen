(defpackage :jscl-sugar
  (:export #:with-sugar)
  (:use :cl))

(in-package :jscl-sugar)

(defun js-name (symbol)
  (symbol-munger:lisp->camel-case symbol))

(defun sugar-form (form)
  (cond
    ((symbolp form)
     form)
    ((and (listp form)
          (symbolp (car form))
          (str:starts-with-p #\/ (symbol-name (car form))))
     ;; call syntax
     (destructuring-bind (fname &rest args) form
       `(jscl:js-call ,(js-name (str:substring 1 nil (symbol-name fname)))
                           ,@(mapcar #'sugar-form args))))
    ((and (listp form)
          (symbolp (car form))
          (str:ends-with-p #\. (symbol-name (car form))))
     ;; class instantiation
     ;; (class.) => (new 'class)
     (error "TODO")
     (destructuring-bind (class-name &rest args)
         `(jscl:js-call ,(intern (str:substring (symbol-name (car form)))))))
    ((and (listp form)
          (symbolp (car form))
          (str:starts-with-p ".-" (symbol-name (car form))))
     ;; property access
     `(jscl:js-getattr ',(cadr form)
                            ,(intern (str:substring 2 nil (symbol-name (car form)))
                                     (symbol-package (car form)))))
    ((and (listp form)
          (symbolp (car form))
          (str:starts-with-p #\. (symbol-name (car form))))
     ;; (.method obj &rest args)
     (destructuring-bind (method obj &rest args) form
       (let ((method-name (intern
                           (str:substring 1 nil (symbol-name method))
                           (symbol-package method))))
         `(jscl:js-method ,obj ,(js-name method-name) ,@args))))
    ((and (listp form)
          (symbolp (car form))
          (find #\. (symbol-name (car form))))
     ;; (obj.foo.bar &rest args)
     (destructuring-bind (access-chain &rest args) form
       (let ((chain-elems (mapcar (alexandria:rcurry #'intern (symbol-package access-chain))
                                  (str:split #\. (symbol-name access-chain)))))
         `(arrows:-<> ,(car chain-elems)
                      ,@(mapcar (lambda (field)
                                  `(jscl:js-getattr <> ',field))
                                (butlast (rest chain-elems)))
                      (jscl:js-method <>
                                           ,(js-name (alexandria:lastcar chain-elems))
                                           ,@(mapcar #'sugar-form args))))))
    ((listp form)
     (mapcar #'sugar-form form))
    (t form)))

(sugar-form '(.write sw "Hello"))
(sugar-form '(sw.write "Hello"))
(sugar-form '(.-foo sw))
(sugar-form '(/print "lala"))
(sugar-form '(let ((x 22)) x))

(defmacro with-sugar (&body body)
  (sugar-form `(progn ,@body)))

;;(jscl:import-module "numpy" :as "np")
;;(jscl:import-module "scipy.integrate" :as "integrate")

(let ((my-str "testing"))
  (jscl:js-call "len" my-str))

(with-sugar
  (let ((my-str "testing"))
    (/len my-str)))

(with-sugar
  (/print "lala"))

(with-sugar
  (.format "hello {0}" "world"))

(with-sugar
  (let ((str "hello {0}"))
    (str.format  "world")))



;; (let ()
;;   (labels
;;       ((make-number (value) 
;;                     (jscl::make-new #j:Number value))
;;        (float-Exponential (value &optional (fraction 5))
;;                           ((jscl::oget (make-Number value) "toExponential") fraction))
;;        (number-to-fixed (value &optional (digits 0))
;;                         ((jscl::oget  (make-Number value) "toFixed") digits)))
;;     (print (float-exponential 123.1 2))
;;     (print (number-to-fixed 123.012345 2))))

;; (with-sugar
;;   (let ()
;;     (labels ((make-number (value)
;;                (new /Number value))
;;              (float-exponential (value &optional (fraction 5))
;;                (.to-exponential (make-number value) fraction))
;;              (number-to-fixed (value &optional (digits 0))
;;                (.to-fixed (make-number value) digits)))
;;       (print (float-exponential 123.1 2))
;;       (print (number-to-fixed 123.012345 2)))))

;; (let ((v1 #(mediane)))
;;   ((jscl::oget v1 "push") 'right)
;;   ((jscl::oget v1 "unshift") 'left)
;;   (list ((jscl::oget v1 "indexOf") 'mediane)
;;         ((jscl::oget v1 "indexOf") 'left)
;;         ((jscl::oget v1 "indexOf") 'right)))

(with-sugar
  (let ((v1 #(mediane)))
    (v1.push  'right)
    (v1.unshift 'left)
    (list (v1.index-of 'mediane)
          (v1.index-of 'left)
          (v1.index-of 'right))))
