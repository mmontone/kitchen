(asdf:defsystem py4cl-interop-sugar
  :description "Interop syntax sugar for py4cl."
  :version "0.1"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license "MIT"
  :depends-on (:py4cl :alexandria :symbol-munger :str :arrows)
  :components
  ((:file "py4cl")))
