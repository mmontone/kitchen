(defpackage :abcl-sugar
  (:export #:with-sugar)
  (:use :cl))

(in-package :abcl-sugar)

(defun java-name (symbol)
  (symbol-munger:lisp->camel-case symbol))

(defun sugar-form (form)
  (cond
    ((symbolp form)
     form)
    ((and (listp form)
          (symbolp (car form)))
     (cond
       ((and (listp form)
             (str:ends-with-p #\. (symbol-name (car form))))
        ;; class instantiation
        ;; (class.) => (new 'class)
        (error "TODO")
        (destructuring-bind (class-name &rest args)
            `(jss:new ',(intern (str:substring (symbol-name (car form)))))))
       ((str:starts-with-p ".-" (symbol-name (car form)))
        ;; property access
        `(java:jfield ',(cadr form)
                      ,(intern (str:substring 2 nil (symbol-name (car form)))
                               (symbol-package (car form)))))
       ((str:starts-with-p #\. (symbol-name (car form)))
        ;; (.method obj &rest args)
        (destructuring-bind (method obj &rest args) form
          (let ((method-name (intern
                              (str:substring 1 nil (symbol-name method))
                              (symbol-package method))))
            `(jss:invoke-restargs ,(java-name method-name) ,obj (list ,@args)))))
       ((find #\. (symbol-name (car form)))
        ;; (obj.foo.bar &rest args)
        (destructuring-bind (access-chain &rest args) form
          (let ((chain-elems (mapcar (alexandria:rcurry #'intern (symbol-package access-chain))
                                     (str:split #\. (symbol-name access-chain)))))
            `(arrows:-<> ,(car chain-elems)
                         ,@(mapcar (lambda (field)
                                     `(java:jfield <> ',field))
                                   (butlast (rest chain-elems)))
                         (jss:invoke-restargs ,(java-name (alexandria:lastcar chain-elems))
                                              <>
                                              (list ,@(mapcar #'sugar-form args)))))))
       (t (mapcar #'sugar-form form))))
    ((listp form)
     (mapcar #'sugar-form form))
    (t form)))

(sugar-form '(.write sw "Hello"))
(sugar-form '(sw.write "Hello"))
(sugar-form '(.-foo sw))

(defmacro with-sugar (&body body)
  (sugar-form `(progn ,@body)))

#+test
(with-sugar
  (let ((sw (jss:new 'java.io.StringWriter)))
     (.write sw "Hello ")
     (sw.write "World")
     (print (.to-string sw))))
