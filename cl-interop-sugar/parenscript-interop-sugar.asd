(asdf:define-system parenscript-interop-sugar
  :description "Interop syntax sugar for parenscript."
  :version "0.1"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license "MIT"
  :depends-on (:parenscript :alexandria :str)
  :components
  ((:file "parenscript")))
