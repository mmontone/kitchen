(asdf:define-system abcl-interop-sugar
  :description "Interop syntax sugar for ABCL."
  :version "0.1"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license "MIT"
  :depends-on (:jss :alexandria :symbol-munger :str)
  :components
  ((:file "abcl")))
