(defpackage :parenscript-sugar
  (:nicknames :ps-sugar)
  (:export #:with-sugar)
  (:use :cl))

(in-package :parenscript-sugar)

(defmethod sugar-form (form)
  (cond
    ((symbolp form)
     form)
    ((and (listp form)
          (symbolp (car form)))
     (cond
       ((str:starts-with-p ".-" (symbol-name (car form)))
        ;; property access
        `(ps:getprop ,(cadr form)
                     ,(intern (str:substring 2 nil (symbol-name (car form)))
                              (symbol-package (car form)))))
       ((str:starts-with-p #\. (symbol-name (car form)))
        ;; (.method obj &rest args)
        (destructuring-bind (method obj &rest args) form
          (let ((method-name (intern
                              (str:substring 1 nil (symbol-name method))
                              (symbol-package method))))
            `(ps:chain ,obj (,method-name ,@args)))))
       ((find #\. (symbol-name (car form)))
        ;; (obj.foo.bar &rest args)
        (destructuring-bind (access-chain &rest args) form
          (let ((chain-elems (mapcar (alexandria:rcurry #'intern (symbol-package access-chain))
                                     (str:split #\. (symbol-name access-chain)))))
            `(ps:chain ,(car chain-elems)
                       ,@(butlast (rest chain-elems))
                       (,(alexandria:lastcar chain-elems)
                        ,@(mapcar #'sugar-form args))))))
       (t (mapcar #'sugar-form form))))
    ((listp form)
     (mapcar #'sugar-form form))
    (t form)))


(equalp
 (sugar-form '(.query-selector-all document "input.post-select"))
 '(PARENSCRIPT:CHAIN DOCUMENT (QUERY-SELECTOR-ALL "input.post-select")))

(equalp (sugar-form '(document.query-selector-all "input.post-select"))
        '(PARENSCRIPT:CHAIN DOCUMENT (QUERY-SELECTOR-ALL "input.post-select")))

(equalp (sugar-form '(checkboxes.for-each (lambda (elem) (document.alert elem))))
        '(PARENSCRIPT:CHAIN CHECKBOXES
          (FOR-EACH
           (lambda (ELEM) (PARENSCRIPT:CHAIN DOCUMENT (ALERT ELEM))))))

(equalp (sugar-form '(setf (.-checked element)
                      (not (.-checked element))))
        '(SETF (PARENSCRIPT:GETPROP ELEMENT CHECKED)
          (NOT (PARENSCRIPT:GETPROP ELEMENT CHECKED))))

(defmacro with-sugar (&body body)
  (sugar-form `(progn ,@body)))

(with-sugar
  (ps:ps
    (let ((checkboxes (document.query-selector-all "input.post-select")))
      (checkboxes.for-each
       (lambda (element)
         (setf (.-checked element)
               (not (.-checked element))))))
    (submit-selections)))

(with-sugar
  (ps:ps
    (let ((checkboxes (document.query-selector-all "input.post-select")))
      (checkboxes.for-each
       (lambda (element)
         (setf (ps:@ element "checked")
               (not (ps:@ element "checked"))))))
    (submit-selections)))
