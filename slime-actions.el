;; TODO:
;; Have a look at menu-bar-update-hook for dynamic menus in Emacs

(require 'slime)
(require 'slime-events)

(defun define-slime-action-command (name options)
  (let ((command-name (intern (substring (symbol-name name)
					 (+ (search "::" (symbol-name name)) 2)))))
    (defalias command-name
      (lambda ()
	(interactive)
	(slime-eval `(,name)))
      (getf options :description))))

(slime-add-event-handler :def-slime-action
			 (lambda (event name options)
			   (define-slime-action-command name options)))

(slime-reset-event-handlers)

(slime-eval '(slime-actions::emacs-get-grouped-slime-actions))

(easy-menu-define slime-actions-menu global-map "SLIME actions"
  '("SLIME Actions"))

(defun create-menu ()
  (let (menuitems)
    (dolist (group (slime-eval '(slime-actions::emacs-get-grouped-slime-actions)))
      (push (list*
	     (symbol-name (car group))
	     (mapcar (lambda (action)
		       (vector
			(symbol-name (cdr (assoc :name action)))
			(cdr (assoc :action action))
			:help (cdr (assoc :description action))))
		     (cdr group)))
	    menuitems))
    ;;(debug menuitems)
  (easy-menu-create-menu
   "Actions" menuitems)))

(easy-menu-add-item slime-actions-menu '() (create-menu))

(provide 'slime-actions)
