(mgl-pax:define-package :progdesktop/clog-gui
  (:documentation "CLOG-GUI a desktop GUI abstraction for CLOG")
  (:import-from :cl-css :inline-css :px :em)
  (:use #:cl #:parse-float #:clog #:mgl-pax))

(defpackage :progdesktop
  (:use :cl :clog #:progdesktop/clog-gui #:access)
  (:local-nicknames
    (:alex :alexandria))
  (:import-from :cl-who :with-html-output
		:with-html-output-to-string :htm :str :fmt)
  (:export :start))
