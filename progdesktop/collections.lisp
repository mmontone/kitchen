(in-package :progdesktop)

(defclass filtered-collection (collection)
  ((collection :initarg :collection
     :accessor collection)
    (filter :initarg :filter
      :accessor filter)))

(defmethod items ((collection filtered-collection))
  (remove-if-not (filter collection)
    (items (collection collection))))

(defclass mapped-collection (collection)
  ((collection :initarg :collection
     :accessor collection)
    (function :initarg :function
      :accessor map-function)))

(defmethod items ((collection mapped-collection))
  (mapcar (map-function collection)
    (items (collection collection))))

#+example
(open-object-in-window 
  (make-instance 'filtered-collection
    :collection (make-instance 'collection 
		  :items *objects*)
    :filter (lambda (object)
	      (typep object 'video)))
  *body*)
