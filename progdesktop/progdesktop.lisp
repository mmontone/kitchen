(in-package :progdesktop)

;; Utilities

;; html

(defvar *html*)
(defmacro html* (&body body)
  `(who:with-html-output (*html*)
     ,@body))
(defmacro html (&body body)
  `(who:with-html-output-to-string (*html*)
     ,@body))

;; declarative gui spec

(defmacro with-gui (obj spec &body body)
  (let ((let-bindings ())
         (used-bindings ()))
    (labels ((create-from-spec (spec parent-binding)
               (destructuring-bind (gui-func-name args &body children)
                 spec
                 (let* ((gui-func-args (alexandria:remove-from-plist args :bind))
                         (bind (getf args :bind))
                         (binding (or bind (gensym)))
                         (create-func-name (intern (concatenate 'string "CREATE-" (symbol-name gui-func-name)))))
                   (push `(,binding (,create-func-name ,parent-binding ,@gui-func-args)) let-bindings)
                   (when (or bind children)
                     (push binding used-bindings))
                   (dolist (child-spec children)
                     (create-from-spec child-spec binding))))))
      (create-from-spec spec obj)
      `(let* ,(reverse let-bindings)
         (declare (ignore ,@(set-difference (mapcar #'first let-bindings) used-bindings)))
         ,@body))))

;; arrows

(defmacro -> (first &rest rest)
  (let ((binding (gensym)))
    `(let ((,binding ,first))
       ,@(loop for r in rest
           collect (list* (first r)
                     binding
                     (rest r))))))

(defmethod element-value ((obj clog-obj))
  (clog-connection:query (clog::connection-id obj)
    (format nil "$('#~A').val()" (html-id obj))))


;;; Demostrate a virtual desktop using CLOG GUI
(defun on-file-count (obj)
  (let ((win (create-gui-window obj :title "Count")))
    (dotimes (n 100)
      ;; window-content is the root element for the clog-gui
      ;; windows
      (create-div (window-content win) :content n))))

(defun browse-in-window (obj title url)
  (let ((win (create-gui-window obj :title title)))
    (create-child (window-content win)
      (html (:iframe :style "width:100%;height:97%;"
              :src url)))))

(defun about-common-lisp (obj)
  (browse-in-window obj "About Common Lisp" "https://common-lisp.net/"))

(defun open-clog-manual (obj)
  (browse-in-window obj "CLOG manual" "https://rabbibotton.github.io/clog/clog-manual.html"))

(defun open-markdown-in-window (obj title markdown-file)
  (let ((win (create-gui-window obj :title title))
         (html (with-output-to-string (s)
                 (cl-markdown:markdown markdown-file :stream s))))
    (create-div (window-content win) :content html)))

(defun open-learn-clog-window (obj)
  ;;(browse-in-window obj "Learn CLOG" "https://github.com/rabbibotton/clog/blob/main/LEARN.md")
  (open-markdown-in-window obj "Learn CLOG" (asdf:system-relative-pathname :clog "LEARN.md")))

(defun open-readme-window (obj)
  (open-markdown-in-window obj "README" (asdf:system-relative-pathname :clog "README.md")))

(defun open-clog-concept-window (obj)
  (open-markdown-in-window obj "CONCEPT" (asdf:system-relative-pathname :clog "CONCEPT.md")))

(defun on-file-drawing (obj)
  (let* ((win (create-gui-window obj :title "Drawing"))
          (canvas (create-canvas (window-content win) :width 600 :height 400))
          (cx     (create-context2d canvas)))
    (set-border canvas :thin :solid :black)
    (fill-style cx :green)
    (fill-rect cx 10 10 150 100)
    (fill-style cx :blue)
    (font-style cx "bold 24px serif")
    (fill-text cx "Hello World" 10 150)
    (fill-style cx :red)
    (begin-path cx)
    (ellipse cx 200 200 50 7 0.78 0 6.29)
    (path-stroke cx)
    (path-fill cx)))

(defun on-file-movies (obj)
  (let* ((win  (create-gui-window obj :title "Movie"))
          (movie (create-video (window-content win)
                   :source "https://www.w3schools.com/html/mov_bbb.mp4")))
    (set-geometry movie :units "%" :width 100 :height 100)))

(defun on-file-pinned (obj)
  (let ((win (create-gui-window obj :title "Pin me!"
               :has-pinner t
               :keep-on-top t
               :top 200
               :left 0
               :width 200
               :height 200)))
    (create-div win :content "I can be pinned. Just click the pin on window bar.")))

(defun on-dlg-alert (obj)
  (alert-dialog obj "This is a modal alert box"))

(defun on-dlg-confirm (obj)
  (confirm-dialog obj "Shall we play a game?"
    (lambda (input)
      (if input
        (alert-dialog obj "How about Global Thermonuclear War.")
        (alert-dialog obj "You are no fun!")))
    :ok-text "Yes" :cancel-text "No"))

(defun on-dlg-input (obj)
  (input-dialog obj "Would you like to play a game?"
    (lambda (input)
      (alert-dialog obj input))))

(defun on-dlg-file (obj)
  (server-file-dialog obj "Server files" "./" (lambda (fname)
                                                (alert-dialog obj fname))))

(defun on-dlg-form (obj)
  (form-dialog obj "Please enter your information."
    '(("Title" "title" :select (("Mr." "mr")
                                 ("Mrs." "mrs" :selected)
                                 ("Ms." "ms")
                                 ("Other" "other")))
       ("Eye Color" "color" :radio (("Blue" "blue")
                                     ("Brown" "brown")
                                     ("Green" "green" :checked)
                                     ("Other" "other")))
       ("Send Mail" "send-mail" :checkbox t)
       ("Name" "name" :text "Real Name")
       ("Address" "address")
       ("City" "city")
       ("State" "st")
       ("Zip" "zip")
       ("E-Mail" "email" :email))
    (lambda (results)
      (alert-dialog obj results))
    :height 550))

(defun on-toast-alert (obj)
  (alert-toast obj "Stop!" "To get rid of me, click the X. I have no time-out"))

(defun on-toast-warn (obj)
  (alert-toast obj "Warning!" "To get rid of me, click the X. I time-out in 5 seconds"
    :color-class "w3-yellow" :time-out 5))

(defun on-toast-success (obj)
  (alert-toast obj "Success!" "To get rid of me, click the X. I time-out in 2 seconds"
    :color-class "w3-green" :time-out 2))

(defun on-help-about (obj)
  (let* ((about (create-gui-window obj
                  :title   "About"
                  :content "<div class='w3-black'>
                                         <center><img src='/img/clogwicon.png'></center>
                                         <center>ProgDesktop</center>
                                         <center>The Programmable Desktop</center></div>
                                         <center>(c) 2022 - Mariano Montone</center></p></div>"
                  :hidden  t
                  :width   200
                  :height  215)))
    (window-center about)
    (setf (visiblep about) t)
    (set-on-window-can-size about (lambda (obj)
                                    (declare (ignore obj))()))))

(defparameter *objects* nil)
(defparameter *url-parsers* (list))

(defclass object ()
  ((name :initarg :name :accessor object-name)
    (id :initarg :id :accessor object-id)
    (url :initarg :url :accessor object-url :initform nil)
    (description :initarg :description :accessor object-description)
    (metadata :initarg :metadata :accessor metadata
      :initform nil)
    (type :initarg :type :accessor object-type)
    (image :initarg :image :accessor object-image :initform nil)
    (icon :initarg :icon :accessor object-icon :initform nil)
    (category :initarg :category :accessor category :initform nil)
    (tags :initarg :tags :accessor tags :initform nil)))

(defun create-object-from-url (url)
  (dolist (url-parser *url-parsers*)
    (let ((object (funcall url-parser url)))
      (when object
        (return-from create-object-from-url object))))
  (make-instance 'unrecognized-object :url url))

(defgeneric create-object-view (object clog-obj &rest args))
(defgeneric create-thumbnail-view (object clog-obj &rest args))

(defclass unrecognized-object (object)
  ())

(defun fetch-preview-data (url)
  ;; Try to build a preview of the web page
  ;; https://andrejgajdos.com/how-to-create-a-link-preview/

  (let* ((html (drakma:http-request url))
          (dom (lquery:$ (lquery:initialize html))))
    (flet ((meta (what name)
             (let ((selector (format nil "meta[~a=~s]" what name)))
               (alex:when-let (el (lquery:$1 dom selector))
                 (plump:attribute el "content")))))
      (let ((title (or (meta "property" "og:title")
                     (meta "name" "twitter:title")
                     (alex:when-let (el (lquery:$1 dom "title")) (plump:text el))
                     (alex:when-let (el (lquery:$1 dom "h1")) (plump:text el))
                     (alex:when-let (el (lquery:$1 dom "h2")) (plump:text el))))
             (description (or (meta "property" "og:description")
                            (meta "name" "twitter:description")
                            (meta "name" "description")
                            (alex:when-let (el (lquery:$1 dom "p")) (plump:text el))))
             (image (or (meta "property" "og:image")
                      (alex:when-let (el (lquery:$1 dom "link[rel=\"image_src\"]"))
                        (plump:attribute el "href"))
                      (meta "name" "twitter:image")
                      (alex:when-let (img (lquery:$1 dom "img")) (plump:attribute img "src")))))
        (when (and image (eql (aref image 0) #\/))
          ;; Convert relative url to absolute
          (let* ((uri (quri:uri url)))
            (setf image (str:concat (quri:uri-scheme uri) "://" (quri:uri-domain uri) image))))
        (values title description image)))))


(defmethod create-object-view ((object unrecognized-object) clog-obj &rest args)
  (declare (ignore args))

  (multiple-value-bind (title description image)
    (fetch-preview-data (object-url object))
    (create-child clog-obj
      (html
        (:div
          (:h3 (who:str title))

          (when image
            (who:htm (:img :src image
                       :style "margin: 5px; max-width: 200px; max-height: 200px; float: left;")))
          (:p (who:str description)))))))

(defmethod create-thumbnail-view ((object object) clog-obj &rest args)
  (declare (ignore args))
  (multiple-value-bind (title description image)
    (fetch-preview-data (object-url object))
    (create-child clog-obj
      (html
        (:div
          (:h5 (who:str title))

          (when image
            (who:htm (:img :src image
                       :style "margin: 5px; max-width: 200px; max-height: 200px; float: left;")))
          (:p (:small (who:str description))))))))

(defclass collection (object)
  ((collection-type :initarg :type :accessor collection-type)
    (items :accessor items :initarg :items)))

(defgeneric create-item-view (object clog-obj &rest args))

(defmethod create-item-view ((object object) li &rest args)
  (declare (ignore args))
  (when (object-icon object)
    (create-child li (html (:i :class (format nil "fa fa-solid fa-~a"
                                        (object-icon object))
                             :style "margin-right: 3px;"))))
  (let ((link
          (create-a li :content (if (slot-boundp object 'name)
                                  (object-name object)
                                  (object-url object)))))

    (set-on-click link (lambda (clog-obj)
                         (open-object-in-window object clog-obj)))))

(defmethod create-object-view ((collection collection) clog-obj &rest args)
  (let ((ul (create-unordered-list clog-obj)))
    (dolist (item (items collection))
      (let ((li (create-list-item ul)))
        (apply #'create-item-view item li (getf args :item))))
    ul))

(defmethod create-thumbnail-view ((collection collection) clog-obj &rest args)
  (let ((thumbnails (create-div clog-obj :class "thumbnails")))
    (setf (clog:attribute thumbnails "style") "background-color: black;")
    (dolist (item (subseq (items collection) 0 20))
      (let ((thumbnail (create-div thumbnails)))
	(setf (clog:attribute thumbnail "style")
	  "margin: 2px; width: 200px; height: 200px;display: inline-block;overflow: hidden;background-color: white;")
	(apply #'create-thumbnail-view item thumbnail (getf args :item))))))

(defun open-object-in-window (object clog-obj &optional (view #'create-object-view))
  (let ((wnd (create-gui-window clog-obj
               :title (cond
                        ((slot-boundp object 'name) (object-name object))
                        ((slot-boundp object 'url) (object-url object))
                        (t (type-of object)))
               :object object)))
    (let ((spinner (create-child (window-content wnd)
                     (html (:i :class "fa fa-spinner w3-spin" :style "font-size:32px")))))
      (funcall view object (window-content wnd))
      (remove-from-dom spinner))))

(defun add-and-show-object (object clog-obj)
  (push object *objects*)
  (open-object-in-window object clog-obj))

(defun add-object-from-url (url clog-obj)
  (let ((object (create-object-from-url url)))
    (add-and-show-object object clog-obj)))

(defun add-object-window (clog-obj)
  (form-dialog clog-obj "Add object" '(("URL" "url"))
    (lambda (results)
      (let ((url (cadar results)))
        (when url
          (add-object-from-url url clog-obj))))
    :height 550))

(defparameter *object-settings*
  '(:info :edit :properties :settings :meta))

(defmethod display-object-settings ((setting (eql :info)) tab-content info)
  (create-child tab-content (html (:h3 "Info"))))

(defmethod display-object-settings ((setting (eql :edit)) tab-content info)
  (create-child tab-content (html (:h3 "Edit"))))

(defmethod display-object-settings ((setting (eql :properties)) tab-content info)
  (create-child tab-content (html (:h3 "Properties"))))

(defmethod display-object-settings ((setting (eql :settings)) tab-content info)
  (create-child tab-content (html (:h3 "Settings"))))

(defmethod display-object-settings ((setting (eql :meta)) tab-content info)
  (create-child tab-content (html (:h3 "Meta"))))

(defun open-object-settings (object clog-obj)
  (let* ((wnd (progdesktop/clog-gui:create-gui-window clog-obj :title "Settings"))
	  (tabbar (create-div (progdesktop/clog-gui:window-content wnd) :class "w3-bar w3-black"))
	  (tab-content (create-div (progdesktop/clog-gui:window-content wnd) :class "w3-container")))
    (dolist (setting *object-settings*)
      (->
	(create-button tabbar
	  :class "w3-bar-item w3-button"
	  :content (string-capitalize (string-downcase (symbol-name setting))))
	(set-on-click (lambda (button)
			(declare (ignore button))
			(setf (text tab-content) "")
			(display-object-settings setting tab-content object)))))))    

(defvar *body*)

(defun open-collection-with-all-objects (clog-obj &optional (view 'create-object-view))
  (open-object-in-window
    (make-instance 'collection
      :name "All objects"
      :items *objects*)
    clog-obj view))

(defun on-new-window (body)
  (setf *body* body)
  ;; Icons
  (load-css (html-document body) "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css")
  ;; For tags input
  (load-script (html-document body) "https://cdn.jsdelivr.net/npm/suggestags@1.27.0/js/jquery.amsify.suggestags.min.js")
  
  (setf (title (html-document body)) "Programmable Desktop")
  
  ;; For web oriented apps consider using the :client-movement option.
  ;; See clog-gui-initialize documentation.
  (clog-gui-initialize body)
  (add-class body "w3-cyan")
  (with-gui body
    (div ()
      (gui-menu-bar ()
        (gui-menu-icon (:on-click 'on-help-about))
        (gui-menu-drop-down (:content "Objects")
          (gui-menu-item (:content "Add object" :on-click 'add-object-window))
          (gui-menu-item (:content "All objects" :on-click 'open-collection-with-all-objects)))
        (gui-menu-drop-down (:content "Window")
          (gui-menu-item (:content "Maximize All" :on-click 'maximize-all-windows))
          (gui-menu-item (:content "Normalize All" :on-click 'normalize-all-windows))
          (gui-menu-window-select ()))
        (gui-menu-drop-down (:content "Dialogs")
          (gui-menu-item (:content "Alert Dialog Box" :on-click 'on-dlg-alert))
          (gui-menu-item (:content "Input Dialog Box" :on-click 'on-dlg-input))
          (gui-menu-item (:content "Confirm Dialog Box" :on-click 'on-dlg-confirm))
          (gui-menu-item (:content "Form Dialog Box" :on-click 'on-dlg-form))
          (gui-menu-item (:content "Server File Dialog Box" :on-click 'on-dlg-file)))
        (gui-menu-drop-down (:content "Toasts")
          (gui-menu-item (:content "Alert Toast" :on-click 'on-toast-alert))
          (gui-menu-item (:content "Warning Toast" :on-click 'on-toast-warn))
          (gui-menu-item (:content "Success Toast" :on-click 'on-toast-success)))
        (gui-menu-drop-down (:content "Help")
          (gui-menu-item (:content "About" :on-click 'on-help-about)))
        (gui-menu-full-screen ()))
      (div (:bind app-content)))
    (let ((app (clog-gui::connection-data-item body "clog-gui")))
      (setf (progdesktop/clog-gui::app-content app) app-content))
    (clog-gui::set-styles app-content '(("height" "6000px")
                                         ("overflow" "scroll")))
    (set-on-before-unload (window body) (lambda(obj)
                                          (declare (ignore obj))
                                          ;; return empty string to prevent nav off page
                                          ""))
    ))

;; Bookmarklet:

;; post('http://127.0.0.1:8081/add', { url: window.location })

;; function post (url, formData) {
;;   const h = (tag, props) => Object.assign(document.createElement(tag), props)
;;   const iframe = h('iframe', {id: 'progdesktop', name:'progdesktop',hidden: true})
;;   const form = h('form', { action: url, method: 'post', hidden: true, target: 'progdesktop' })
;;   for (const [name, value] of Object.entries(formData)) {
;;     form.appendChild(h('input', { name, value }))
;;   }
;;   document.body.appendChild(iframe)
;;   document.body.appendChild(form)
;;   form.submit()
;; }

;; https://www.yourjs.com/bookmarklet/

;; Bookmarks

(defun import-firefox-bookmarks (file)
  "Imports bookmarks from Firefox JSON file."
  (let ((bookmarks
          (loop for category in
            (accesses (json:decode-json-from-source file) :children)
            appending (access:accesses category :children))))
    (dolist (bookmark bookmarks)
      (push (create-object-from-url (access  bookmark :uri))
        *objects*))))

(hunchentoot:define-easy-handler (add-object-handler :uri "/add")
  (url)
  (add-object-from-url url *body*)
  "Done")

(hunchentoot:define-easy-handler (render-object-handler :uri "/render")
  (url)
  (setf (hunchentoot:header-out "Access-Control-Allow-Origin") "*")
  (setf (hunchentoot:header-out "Access-Control-Allow-Methods") "GET, OPTIONS")
  (let* ((object (create-object-from-url url))
	  (view (create-object-view object *body*)))
    (clog:outer-html view)))

(defun start ()
  "Start desktop."
  (initialize 'on-new-window)
  (open-browser)
  (hunchentoot:start (make-instance 'hunchentoot:easy-acceptor :port 8081)))
