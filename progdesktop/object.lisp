(in-package :progdesktop)

(defmacro defobject (name superclasses slots &rest options))

;; Mark attributes with :attribute t, settings with :setting t. Type of attribute or setting can be inferred from :type. Types may need to be parsed for more complex, like (member "foo" "bar")

(defobject youtube-video (video)
  ((youtube-api-key :type string :setting t))
  (:url-parser parse-youtube-url)
  (:create-view (video clog-obj &rest args))
  (:create-item (video clog-obj &rest args)))
