(in-package :progdesktop)

(defun init-table-component (body)
  (load-css (html-document body)
    "https://www.w3.org/TR/wai-aria-practices-1.1/examples/treeview/treeview-2/css/treeLinks.css")
  (load-script (html-document body)
    "https://www.w3.org/TR/wai-aria-practices-1.1/examples/treeview/treeview-2/js/treeLinks.js")
  (load-script (html-document body)
    "https://www.w3.org/TR/wai-aria-practices-1.1/examples/treeview/treeview-2/js/treeitemLinks.js"))

(defclass table-object (object)
  ((collection :initarg :collection
     :accessor collection)
    (columns :initarg :columns
      :accessor columns)))

(defmethod create-object-view ((object table-object) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
    (html
      (:table :class "w3-table w3-striped w3-bordered w3-small"
	;; Headings
	(:tr
	  (dolist (column (columns object))
	    (who:htm
	      (:td (who:str (princ-to-string column))))))
	(dolist (item (items (collection object)))
	  (who:htm (:tr
		     (dolist (column (columns object))
		       (let ((colval (when (slot-boundp item column)
				       (slot-value item column))))
			 (who:htm (:td (who:str (princ-to-string colval)))))))))))))

#+example
(open-object-in-window (make-instance 'table-object
			 :collection (make-instance 'collection
				       :items *objects*)
			 :columns '(id name url))
  *body*)
