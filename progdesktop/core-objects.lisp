(in-package :progdesktop)

(defclass image (object)
  ()
  (:default-initargs
    :icon "image"))

(defun parse-image-url (url)
  (when (some (lambda (img-ext)
		(str:ends-with? url img-ext))
	  '(".png" ".jpg" ".jpeg"))
    (make-instance 'image
      :url url)))

(pushnew 'parse-image-url *url-parsers*)

(defmethod create-object-view ((image image) clog-obj &rest args)
  (declare (ignore args))
  (create-img clog-obj
    :url-src (object-url image)))

(defclass video (object)
  ()
  (:default-initargs
    :icon "video"))

(defclass youtube-video (video)
  ((video-id :initarg :video-id :accessor video-id))
  (:default-initargs :category "videos"))

;; <iframe width="560" height="315" src="https://www.youtube.com/embed/AgYB13ntuzg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

(defmethod create-object-view ((video youtube-video) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
    (html (:iframe :width 560 :height 315
	    :src (format nil "https://www.youtube.com/embed/~a" (video-id video))
	    :title "YouTube video player" :frameborder 0
	    :allow "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
	    :allowfullscreen t))))


;; https://www.youtube.com/watch?v=AgYB13ntuzg
(defun parse-youtube-url (url)
  "Return the video id."
  (when (not (str:starts-with? "https://www.youtube.com/watch?v" url))
    (return-from parse-youtube-url))
  (let* ((uri (quri:uri url))
	  (video-id (access:access (quri:uri-query-params uri) "v"))
	  (video (make-instance 'youtube-video
		   :name "video-1"
		   :id video-id
		   :video-id video-id
		   :url url
		   :type 'youtube-video
		   )))
    video))

(pushnew 'parse-youtube-url *url-parsers*)
    
(parse-youtube-url "https://www.youtube.com/watch?v=AgYB13ntuzg")

(defclass cuevana-movie (object)
  ((description :initarg :description :accessor object-description))
  (:default-initargs :category '("videos" "movies")))

(defun parse-cuevana-movie-url (url)
  (when (not (str:starts-with? "https://cuevana2.io/pelicula" url))
    (return-from parse-cuevana-movie-url))
  (let* ((html (drakma:http-request url))
	  (dom (lquery:$ (lquery:initialize html))))
    (make-instance 'cuevana-movie
      :name "cuevana-obj1"
      :id "aa"
      :url url
      :description (format nil "<div>~a</div>" (lquery-funcs:html  (aref (lquery:$ dom "div.mvic-desc") 0))))))

(pushnew 'parse-cuevana-movie-url *url-parsers*)

(parse-cuevana-movie-url "https://cuevana2.io/pelicula/break-every-chain/")


(defmethod create-object-view ((object cuevana-movie) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj (object-description object)))

;; https://open.spotify.com/track/0AjWPhrgNzEAO7FTnQIlEf?si=8f1b5191876f440a

(defclass spotify-track (object)
  ())

(defun parse-spotify-track-url (url)
  (when (not (str:starts-with? "https://open.spotify.com/track" url))
    (return-from parse-spotify-track-url))
  (make-instance 'spotify-track
    :url url
    :id (subseq url (length "https://open.spotify.com/track/"))))

(pushnew 'parse-spotify-track-url *url-parsers*)

(defmethod create-object-view ((object spotify-track) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
    (html
      (:iframe :style "border-radius:12px"
	:src (format nil "https://open.spotify.com/embed/track/~a?utm_source=generator" (object-id object))
	:width "100%" :height "380" :frameBorder "0" :allowfullscreen t
	:allow "autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"))))

;; https://calendar.google.com/event?action=TEMPLATE&tmeid=NGU1N3ExczRiaGE4YTJoMXVwNWRtaGo1cDcgYnVubnltb3JyaXNvbjgzQG0&tmsrc=bunnymorrison83%40gmail.com

(defclass google-calendar-event (object)
  ())

(defparameter *google-api-key* nil)

(defclass google-api-object ()
  ((api-key :initarg :api-key :accessor api-key
	    :initform *google-api-key*)))

;; <a target="_blank" href="https://calendar.google.com/event?action=TEMPLATE&amp;tmeid=NGU1N3ExczRiaGE4YTJoMXVwNWRtaGo1cDcgYnVubnltb3JyaXNvbjgzQG0&amp;tmsrc=bunnymorrison83%40gmail.com"><img border="0" src="https://www.google.com/calendar/images/ext/gc_button1_en.gif"></a>

;; https://calendar.google.com/calendar/htmlembed?src=addressbook%23contacts%40group.v.calendar.google.com&ctz=America%2FArgentina%2FBuenos_Aires

(defclass google-calendar-calendar (object google-api-object)
  ()
  (:default-initargs :category "calendar"))

;; <iframe src="https://calendar.google.com/calendar/htmlembed?src=addressbook%23contacts%40group.v.calendar.google.com&ctz=America%2FArgentina%2FBuenos_Aires" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>Q


;; <iframe  width="600"  height="450"  style="border:0"  loading="lazy"  allowfullscreen  referrerpolicy="no-referrer-when-downgrade"  src="https://www.google.com/maps/embed/v1/place?key=API_KEY    &q=Space+Needle,Seattle+WA"></iframe>

;; https://developers.google.com/maps/documentation/embed/get-started

(defclass google-map (object google-api-object)
  ()
  (:default-initargs :category "maps"))

(defun parse-gmaps-url (url)
  (when (not (str:starts-with? "https://www.google.com/maps/place" url))
    (return-from parse-gmaps-url))
  (make-instance 'google-map :url url))

(pushnew 'parse-gmaps-url *url-parsers*)

(defmethod create-object-view ((object google-map) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
		(html
		  (:iframe :width "600" :height "450"
		  :style "border:0" :loading "lazy"
			   :allowfullscreen t :src (format nil "https://www.google.com/maps/embed/v1/place?key=~a&q=Space+Needle,Seattle+WA" (api-key object))))))

;; <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-57.954913973808296%2C-34.920260017294346%2C-57.9528620839119%2C-34.919019616272855&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/-34.91964/-57.95389">Ver mapa más grande</a></small>

;; https://www.openstreetmap.org/#map=19/-34.91964/-57.95389

(defclass osm-map (object)
  ()
  (:default-initargs :category "maps"))

(defun parse-osm-map-url (url)
  (when (not (str:starts-with? "https://www.openstreetmap.org" url))
    (return-from parse-osm-map-url))
  (make-instance 'osm-map
		 :url url))

(pushnew 'parse-osm-map-url *url-parsers*)

(defmethod create-object-view ((object osm-map) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
		(html (:iframe :width "425" :height "350" :frameborder "0" :scrolling "no" :marginheight "0" :marginwidth "0" :src "https://www.openstreetmap.org/export/embed.html?bbox=-57.954913973808296%2C-34.920260017294346%2C-57.9528620839119%2C-34.919019616272855&amp;layer=mapnik" :style "border: 1px solid black"))))

;; TODO:

(defclass twit (object)
  ())

(defun parse-twit-id (url)
  (let ((status-pos (search "status/" url)))
    (subseq url (+ status-pos (length "status/")))))

(parse-twit-id "https://twitter.com/tatut/status/1525464619548086278")

(defun parse-twit-url (url)
  (when (not (str:starts-with-p "https://twitter.com" url))
    (return-from parse-twit-url))
  (make-instance 'twit
    :id (parse-twit-id url)
    :url url))

(pushnew 'parse-twit-url *url-parsers*)

(parse-twit-url "https://twitter.com/tatut/status/1525464619548086278")

(defmethod create-object-view ((twit twit) clog-obj &rest args)
  (declare (ignore args))
  #+nil(create-child clog-obj 
	 (format nil "<blockquote class=\"twitter-tweet\"><a href=\"~a\"></a></blockquote> <script async src=\"https://platform.twitter.com/widgets.js\" charset=\"utf-8\"></script>" (object-url twit)))
  (create-child clog-obj
    (alexandria:assoc-value (json:decode-json-from-source (drakma:http-request "https://publish.twitter.com/oembed" :parameters `(("url" . ,(object-url twit))) :want-stream t)) :html))
  )
  
(defclass twitter-account (object)
  ())

(defclass reddit-channel (object)
  ())

;; https://openweathermap.org/
(defvar *owm-api-key* nil)
(defclass owm-weather (object)
  ;; TODO: the slots should all be configurable properties from ui
  ((api-key :initarg :api-key
     :accessor api-key
     :initform *owm-api-key*)
    (lat :initarg :lat
      :accessor lat
      :initform "34.9205")
    (lon :initarg :lon
      :accessor lon
      :initform "57.9536")))

(defmethod fetch-data ((object owm-weather))
  (json:decode-json-from-source
    (drakma:http-request
      (format nil "https://api.openweathermap.org/data/2.5/onecall?lat=~a&lon=~a&appid=~a" (lat object) (lon object) (api-key object))
      :want-stream t)))

(defun parse-owm-url (url)
  (when (not (search "openweathermap.org" url))
    (return-from parse-owm-url))
  (make-instance 'owm-weather :url url))

(pushnew 'parse-owm-url *url-parsers*)

(defmethod create-object-view ((object owm-weather) clog-obj &rest args)
  (declare (ignore args))
  (let ((weather-data (fetch-data object)))
    (create-child clog-obj
      (with-output-to-string (html)
	(write-string "<pre>" html)
	(json:encode-json weather-data html)
	(write-string "</pre>" html)))))

;; https://www.aakashweb.com/articles/google-news-rss-feed-url/
;; https://news.google.com/rss
;; https://news.google.com/rss/search?q=<KEYWORD>

(defclass rss-feed (object)
  ((read-count :initarg :read-count ;; this should be a configurable object attribute
     :accessor read-count
     :initform 10
     :type integer
     )
    (data :initarg :data :accessor rss-data)))

(defun parse-rss-url (url)
  (let ((rss (and (search "rss" url)
	       (ignore-errors (rss:parse-rss-stream
				(drakma:http-request url :want-stream t))))))
    (when (not rss)
      (return-from parse-rss-url))
    (let ((rss-data (rss:parse-rss-stream (drakma:http-request url :want-stream t))))
      (make-instance 'rss-feed
	:name (rss:title rss-data)
	:url url
	:data rss-data))))

(pushnew 'parse-rss-url *url-parsers*)

;; (add-object-from-url "https://news.google.com/rss" *body*)
;; (add-object-from-url "https://news.google.com/rss?hl=es&gl=AR&ceid=AR:es" *body*)

(defmethod create-object-view ((rss-feed rss-feed) clog-obj &rest args)
  (declare (ignore args))
  (let ((ul (create-unordered-list clog-obj)))
    (dolist (item (rss:items (rss-data rss-feed)))
      (let* ((li (create-list-item ul))
	      (a (create-a li :content (rss:title item))))
	(set-on-click a (lambda (obj)
			  (let ((wnd (create-gui-window obj :title (rss:title item))))
			    (create-child (window-content wnd)
			      (rss:description item)))))))))

(defclass clock-object (object)
  ())

(defmethod create-object-view ((object clock-object) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
    "<iframe src=\"https://free.timeanddate.com/clock/i8bthbm0/n541/szw80/szh80/hocf00/hbw0/hfcc00/cf100/hnca32/fas20/facfff/fdi86/mqcfff/mqs2/mql3/mqw4/mqd70/mhcfff/mhs2/mhl3/mhw4/mhd70/mmv0/hhcfff/hhs2/hmcfff/hms2/hsv0\" frameborder=\"0\" width=\"80\" height=\"80\"></iframe>"))

(defclass book (object)
  ((portrait-image :initarg :portrait-image :accessor portrait-image)))

(defclass google-book (book)
  ()
  (:default-initargs :category "books"))

;; https://www.googleapis.com/books/v1/volumes/zyTCAlFPjgYC?key=yourAPIKey
(defun read-google-book-data (google-book)
  (let ((book-data (json:decode-json-from-source
		     (drakma:http-request (format nil "https://www.googleapis.com/books/v1/volumes/~a" (object-id google-book))
		       :parameters (list (cons "key" *google-api-key*))
		       :want-stream t))))
    (setf (object-name google-book) (accesses book-data :volume-info :title))
    (setf (object-description google-book)
      (accesses book-data :volume-info :description))
    (setf (portrait-image google-book)
      (accesses book-data :volume-info :image-links :thumbnail))
    google-book))

(defun parse-google-book-url (url)
  (when (not (str:starts-with? "https://books.google.com" url))
    (return-from parse-google-book-url))
  ;; TODO: use google api to retrieve book info here ...
  (let* ((uri (quri:uri url))
	  (book-id (access:access (quri:uri-query-params uri) "id")))
    (read-google-book-data (make-instance 'google-book :id book-id :url url))))

(pushnew 'parse-google-book-url *url-parsers*)

(defmethod create-object-view ((book book) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
    (html (:div
	    (:h2 (str (object-name book)))
	    (:img :src (portrait-image book))
	    (:p (who:esc (object-description book)))))))

(defclass chess-game (object)
  ((fen :initarg :fen)
    (players :initarg :players :accessor chess-players)
    (date :initarg :date :accessor chess-game-date))
  (:default-initargs
    :icon "chess"))

(defclass lichess-game (chess-game)
  ())

(defun parse-lichess-url (url)
  (when (not (str:starts-with? "https://lichess.org" url))
    (return-from parse-lichess-url))
  (make-instance 'lichess-game
    :url url
    :id (subseq url (length "https://lichess.org/")
	  (or (position #\# url)
	    (position #\? url)))))

(pushnew 'parse-lichess-url *url-parsers*)

(defmethod create-object-view ((chess-game lichess-game) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
    (html (:iframe :src (format nil "https://lichess.org/embed/~a?theme=auto&bg=auto" (object-id chess-game))
	    :width 600 :height 397 :frameborder  0))))

;; <iframe src="https://lichess.org/embed/MPJcy1JW?theme=auto&bg=auto" width=600 height=397 frameborder=0></iframe>

(defclass reddit-comment (object)
  ((info :initarg :info :accessor reddit-info)))

;; https://www.reddit.com/api/info.json?id=t1_d99c0y1

(defun parse-reddit-comment-url (url)
  (when (and (str:starts-with? "https://www.reddit.com" url)
	  (search "/comments/" url))
    (let* ((from (+ (search "/comments/" url) (length "/comments/")))
	    (cid (subseq url from (position #\/ url :start from))))
    (make-instance 'reddit-comment
      :id cid
      :url url
      :info (json:decode-json-from-source
	      (drakma:http-request (format nil "https://www.reddit.com/api/info.json?id=t1_~a" cid) :want-stream t))))))

(pushnew 'parse-reddit-comment-url *url-parsers*)

(defmethod create-object-view ((reddit-comment reddit-comment) clog-obj &rest args)
  (declare (ignore args)))

(defclass remote-document (object)
  ((content :accessor document-content :initarg :content)
    (format :accessor document-format :initarg :format)))

(defun parse-remote-document-url (url)
  (when (str:ends-with? ".md" url)
    (make-instance 'remote-document
      :url url
      :content (drakma:http-request url)
      :format :markdown)))

(pushnew 'parse-remote-document-url *url-parsers*)

(defmethod create-object-view ((doc remote-document) clog-obj &rest args)
  (declare (ignore args))
  (when (eql (document-format doc) :markdown)
    (create-child clog-obj
      (with-output-to-string (s)
	(cl-markdown:markdown (document-content doc) :stream s)))))

(defclass filesystem-directory (collection)
  ((directory :initarg :directory))
  (:default-initargs
    :icon "folder"))

(defclass filesystem-file (object)
  ((pathname :initarg :pathname)
    (file-type :initarg :file-type))
  (:default-initargs
    :icon "file"))

(defmethod items ((collection filesystem-directory))
  (with-slots (directory) collection
    (mapcar (lambda (pathname)
	      (if (pathname-utils:directory-p pathname)
		(make-instance 'filesystem-directory
		  :name (princ-to-string pathname)
		  :directory pathname)
		(make-instance 'filesystem-file
		  :name (pathname-utils:file-name pathname)
		  :pathname pathname)))
      (append
	(uiop/filesystem:subdirectories directory)
	(uiop:directory-files directory)))))

(defmethod create-object-view ((file-object filesystem-file) clog-obj &rest args)
  (declare (ignore args))
  (with-slots (pathname) file-object
    (create-child clog-obj
      (html (:pre (who:str (alexandria:read-file-into-string pathname)))))))

(defmethod create-item-view ((object filesystem-directory) li &rest args)
  (declare (ignore args))
  
  (when (object-icon object)
    (create-child li (html (:i :class (format nil "fa fa-solid fa-~a"
					(object-icon object))
			     :style "margin-right: 3px;"))))
  (with-slots (directory) object
    (let* ((dirname (car (last (pathname-directory directory))))
	    (link
	      (create-a li :content (concatenate 'string dirname "/"))))
      (set-on-click link (lambda (clog-obj)
			   (open-object-in-window object clog-obj))))))
  

(defun show-directory (pathname)
  (add-and-show-object (make-instance 'filesystem-directory
			    :name (princ-to-string pathname)
			 :directory pathname)
    *body*))

#+nil(show-directory #p"/mnt/sdb2/home/marian/src/")
