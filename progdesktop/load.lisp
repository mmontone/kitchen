(ql:quickload '(:cl-who :clog :cl-markdown :drakma :plump :lquery :str :access :rss :cl-json :cl-css :cl-markdown :pathname-utils))

(defun load-from-here (files)
  (dolist (file files)
    (load (merge-pathnames file *load-pathname*))))

(load-from-here
  '("package"
     "clog-gui.lisp"
     "progdesktop.lisp"
     "core-objects.lisp"))

(setf progdesktop/clog-gui::*client-movement* t)

(progdesktop:start)
