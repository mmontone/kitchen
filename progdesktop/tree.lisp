(in-package :progdesktop)

;; https://www.w3.org/TR/wai-aria-practices-1.1/examples/treeview/treeview-2/treeview-2b.html

(defun init-tree-component (body)
  (load-css (html-document body)
    "https://www.w3.org/TR/wai-aria-practices-1.1/examples/treeview/treeview-2/css/treeLinks.css")
  (load-script (html-document body)
    "https://www.w3.org/TR/wai-aria-practices-1.1/examples/treeview/treeview-2/js/treeLinks.js")
  (load-script (html-document body)
    "https://www.w3.org/TR/wai-aria-practices-1.1/examples/treeview/treeview-2/js/treeitemLinks.js"))

(defclass tree-object (object)
  ((items :initarg :items :accessor items)))

(defmethod create-object-view ((object tree-object) clog-obj &rest args)
  (declare (ignore args))
  (let ((tree
	  (create-child clog-obj
    (html
      (:ul :role "tree" :aria-labelledby "tree1"
	(:li :role "treeitem"
	  :aria-level 1
	  :aria-setsize 3
	  :aria-posinset 1
	  :aria-expanded "false"
	  (:span (who:str "Fruits"))
	  (:ul
	    (:li :role "none"
	      (:a :role "treeitem"
		:href "/oranges"
		(who:str "Oranges")))
	    (:li :role "none"
	      (:a :role "treeitem"
		:href "/pinnaple"
		(who:str "Pinnaple"))))))))))
    (clog:js-execute clog-obj (format nil "
var tree = document.getElementById('~a');
var t = new TreeLinks(tree);
    t.init();"
    (html-id tree)))))
	      
