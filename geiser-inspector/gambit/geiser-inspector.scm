(define-library (geiser-inspector)
  (export
   inspector-max-contents
   inspect-object
   inspector-inspect-object
   inspector-access-object
   inspector-go-back
   inspector-pprint-object
   read-and-inspect)
  (import
    (gambit)
    ;;(scheme base)
    (scheme file)
    (scheme write)
    (scheme read)
    (scheme eval)
    (scheme repl)
    (scheme process-context)
    ;;(std srfi |13|)
    ;;(std srfi |1|)
    )
  (include "../geiser-inspector-srfi.scm")
  (include "geiser-inspector-gambit.scm")
  (include "../geiser-inspector.scm"))
