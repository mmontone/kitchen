;;; geiser-inspector.el --- Inspector tool for Geiser  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Inspector tool for Geiser

;;; Code:

(require 'geiser)
(require 'geiser-compile)
(require 'geiser-eval)
(require 'cl-macs)

(defvar geiser-inspector--load-path (file-name-directory load-file-name))

(defun geiser-inspector--load-chez ()
  (geiser-load-file (concat geiser-inspector--load-path "chez/geiser-inspector.scm")))

(defun geiser-inspector--load-guile ()
  (geiser-eval--send/wait '(:eval (:scm "(use-modules (geiser modules) (srfi srfi-1))")))
  (geiser-load-file (concat geiser-inspector--load-path "guile/geiser-inspector.scm")))

(defun geiser-inspector--load-chicken ()
  (geiser-load-file (concat geiser-inspector--load-path "chicken/geiser-inspector.scm")))

(defun geiser-inspector--load-gambit ()
  (geiser-load-file (concat geiser-inspector--load-path "gambit/geiser-inspector.scm")))

(defun geiser-inspector--load-implementation ()
  (cl-case geiser-impl--implementation
    (guile (geiser-inspector--load-guile))
    (chicken (geiser-inspector--load-chicken))
    (chez (geiser-inspector--load-chez))
    (gambit (geiser-inspector--load-gambit))))

(add-hook 'geiser-repl-startup-hook #'geiser-inspector--load-implementation)

(defun geiser-inspector--read-result (result-str)
  (cl-check-type result-str string)

  (geiser-syntax--read-from-string result-str))

(defun geiser-inspector--response-object-do (func)
  (lambda (res)
    (let ((obj (car (geiser-inspector--read-result
                     (cadr (assoc 'result res))))))
      (funcall func obj))))

(defun geiser-inspector--get-create-inspector-buffer ()
  (or (get-buffer "*geiser-inspector*")
      (let ((buffer (get-buffer-create "*geiser-inspector*"))
            (geiser-repl (or geiser-repl--repl
                             (user-error "No Geiser repl")))
            (geiser-impl geiser-impl--implementation))
        (cl-assert geiser-repl)
        (cl-assert geiser-impl)
        (with-current-buffer buffer
          (setq buffer-read-only t)
          (geiser-inspector-mode)
          ;; Propagate the geiser repl to the inspector buffer
          (setq-local geiser-repl--repl geiser-repl)
          ;; Propagate the geiser implementation
          (setq-local geiser-impl--implementation geiser-impl)
          (setq geiser-repl--repl geiser-repl)
          buffer))))

(defun geiser-inspector-inspect (object)
  (let ((display (cdr (assoc :display object)))
        (object-type (cdr (assoc :type object))))
    (let ((buffer (geiser-inspector--get-create-inspector-buffer)))
      (with-current-buffer buffer
        (let ((inhibit-read-only t))
          (erase-buffer)
          (goto-char 0)
          (insert (prin1-to-string object-type))
          (insert " ")
          (insert display)
          (newline 2)
          (geiser-inspector--insert-contents object))
        (goto-char 0)
        (pop-to-buffer buffer)))))

(defun geiser-inspector--get-inspector-func (name)
  (cl-case geiser-impl--implementation
    (guile
     `(eval ',name (find-module '(geiser-inspector))))
    (chez
     `(eval ',name (environment '(geiser-inspector))))
    (gambit
     (intern (format "geiser-inspector#%s" name)))
    (t
     `(eval ',name (environment '(geiser-inspector))))))

(defun geiser-inspector--access-object (index)
  (geiser-inspector--eval-send
   (format "(%s '%s)" (geiser-inspector--get-inspector-func 'inspector-access-object)
           index)
   #'geiser-inspector-inspect))

(defun geiser-inspector--access-object-button (label index)
  (insert-button label
                 'action (lambda (_btn)
                           (geiser-inspector--access-object index))
                 'follow-link t
                 'help-echo "Inspect"))

(defun geiser-inspector-eval (string)
  "Eval a Scheme expression in the context of the inspected object.
The $1 variable will be bound to the inspected object."
  (geiser-eval--send
   (format "(%s %s)" (geiser-inspector--get-inspector-func 'inspect-object) string)
   (geiser-inspector--response-object-do
    #'geiser-inspector-inspect)))

(defun geiser-inspector--insert-contents (object)
  (let ((contents (cdr (assoc :contents object))))
    (cl-case (cdr (assoc :type object))
      (pair?
       (insert "(")
       (geiser-inspector--access-object-button (car contents) 'car)
       (insert " . ")
       (geiser-inspector--access-object-button (cdr contents) 'cdr)
       (insert ")"))
      (hash-table?
       ;; Contents of a hash-table are in alist form
       ;; Index is a pair formed by index of element in the list plus either :key or :value
       (let ((index 0))
         (dolist (entry contents)
           (geiser-inspector--access-object-button (car entry) (cons index 'key))
           (insert ": ")
           (geiser-inspector--access-object-button (cdr entry) (cons index 'value))
           (newline)
           (cl-incf index))))
      (list?
       (let ((index 0))
         (dolist (elem contents)
           (insert (prin1-to-string index))
           (insert ": ")
           (geiser-inspector--access-object-button elem index)
           (newline)
           (cl-incf index))))
      (vector?
       (let ((index 0))
         (dolist (elem contents)
           (insert (prin1-to-string index))
           (insert ": ")
           (geiser-inspector--access-object-button elem index)
           (newline)
           (cl-incf index))))
      (record?
       (dolist (field-and-value contents)
         (let ((field-name (car field-and-value))
               (field-value (cdr field-and-value)))
           (insert (prin1-to-string field-name))
           (insert ": ")
           (geiser-inspector--access-object-button field-value field-name)
           (newline))))
      (t (insert "")))))

;; ** Commands

(defun geiser-inspector-pop ()
  "Inspect previous object in inspector history."
  (interactive)
  (geiser-inspector--eval-send
   (format "(%s)" (geiser-inspector--get-inspector-func 'inspector-go-back))
   #'geiser-inspector-inspect))

(defun geiser-inspector-quit ()
  "Quit the Geiser inspector."
  (interactive)
  (kill-buffer "*geiser-inspector*"))

(defun geiser-inspector--eval-send (expr callback &rest dont-parse-response)
  ;; FIXME: with-current-buffer should not be necessary here.
  ;; But the evaluation call doesn't always work otherwise :(
  (let ((repl-buffer geiser-repl--repl))
    (with-current-buffer repl-buffer
      (geiser-eval--send
       `(:eval (:scm ,expr))
       (if dont-parse-response
           callback
         (geiser-inspector--response-object-do callback))))))

(defun geiser-inspector-eval-and-inspect-expr (expr)
  "Evaluate scheme EXPRession and inspect its result."
  (interactive "sEvaluate and inspect: ")
  (geiser-inspector--eval-send
   (format "(%s %s)"
           (geiser-inspector--get-inspector-func 'inspect-object)
           expr)
   #'geiser-inspector-inspect
   ))

(defun geiser-inspector--read-last-expr ()
  (let* (bosexp
         (eosexp (save-excursion (backward-sexp)
                                 (setq bosexp (point))
                                 (forward-sexp)
                                 (point))))
    (buffer-substring-no-properties bosexp eosexp)))

(defun geiser-inspector-eval-and-inspect-last-expr ()
  (interactive)
  (geiser-inspector-eval-and-inspect-expr
   (geiser-inspector--read-last-expr)))

(defun geiser-inspector-read-and-inspect-last-expr ()
  (interactive)
  (geiser-inspector--eval-send
   (format "(%s %S)"
           (geiser-inspector--get-inspector-func 'read-and-inspect)
           (geiser-inspector--read-last-expr))
   #'geiser-inspector-inspect
   ))

(defun geiser-inspector-pprint-inspected-object ()
  "Pretty print the object being inspected."
  (interactive)
  (geiser-inspector--eval-send
   (prin1-to-string (list (geiser-inspector--get-inspector-func 'inspector-pprint-object)))
   (lambda (pretty-printed)
     (with-current-buffer-window "*geiser-inspector pprint*"
         nil nil
       ;; local-set-key modifies the mode map of the entire buffer's major mode (emacs-lisp-mode-map).
       ;; to modify the map for this buffer only, we need to use a copy of the mode-map:
       (use-local-map (copy-keymap geiser-mode-map))
       (local-set-key "q" #'kill-this-buffer)
       (insert pretty-printed)
       ;; Jump to this buffer
       (switch-to-buffer-other-window (current-buffer))))))

(defun geiser-inspector-refresh ()
  (interactive)

  (geiser-inspector--eval-send
   (format "(%s)" (geiser-inspector--get-inspector-func 'inspector-inspect-object))
   #'geiser-inspector-inspect))

(defvar geiser-inspector-mode-map
  (let ((map (make-keymap)))
    (define-key map "q" #'geiser-inspector-quit)
    (define-key map "l" #'geiser-inspector-pop)
    (define-key map "e" #'geiser-inspector-eval)
    (define-key map "n" #'forward-button)
    (define-key map "p" #'backward-button)
    (define-key map "P" #'geiser-inspector-pprint-inspected-object)
    (define-key map "g" #'geiser-inspector-refresh)
    map))

(easy-menu-define
  geiser-inspector-mode-menu geiser-inspector-mode-map
  "Menu for inspector."
  '("Geiser Inspector"
    ["Previous" geiser-inspector-pop :help "Inspect previous object"]
    ["Evaluate" geiser-eval-expression :help "Evaluate expression with current inspected object as context"]
    ["Pretty print inspected object" geiser-inspector-pprint-inspected-object]
    ["Refresh" geiser-inspector-refresh :help "Refresh inspector buffer"]
    ["Exit" geiser-inspector-quit :help "Quit the Emacs Lisp inspector"]))

(defvar geiser-inspector-tool-bar-map
  (let ((map (make-sparse-keymap)))
    (tool-bar-local-item-from-menu
     'geiser-inspector-pop "left-arrow" map geiser-inspector-mode-map
     :rtl "left-arrow"
     :label "Back"
     :vert-only t)
    (tool-bar-local-item-from-menu
     'geiser-inspector-quit "exit" map geiser-inspector-mode-map
     :vert-only t)
    map))

(define-derived-mode geiser-inspector-mode fundamental-mode "Geiser Inspector"
  "Major mode for the Emacs Lisp Inspector."
  (setq-local tool-bar-map geiser-inspector-tool-bar-map))

(add-hook 'geiser-mode-hook
          (lambda ()
            (local-set-key (kbd "C-x i") 'geiser-inspector-eval-and-inspect-last-expr)
            (local-set-key (kbd "C-c i") 'geiser-inspector-eval-and-inspect-expr)))

(add-hook 'geiser-repl-mode-hook
          (lambda ()
            (local-set-key (kbd "C-x i") 'geiser-inspector-eval-and-inspect-last-expr)
            (local-set-key (kbd "C-c i") 'geiser-inspector-eval-and-inspect-expr)))

(provide 'geiser-inspector)

;;; geiser-inspector.el ends here
