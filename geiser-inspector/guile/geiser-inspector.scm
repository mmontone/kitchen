(define-library (geiser-inspector)
  (export
   inspector-max-contents
   inspect-object
   inspector-inspect-object
   inspector-access-object
   inspector-go-back
   inspector-pprint-object
   read-and-inspect
   inspector-object
   current-inspector)
  (import
    (guile)
    (srfi srfi-1)
    (srfi srfi-9) ;; records
    (srfi srfi-13)
    (srfi srfi-43)
    (srfi srfi-69) ;; hash tables
    (ice-9 pretty-print))
  (include "../geiser-inspector-srfi.scm")
  (include "geiser-inspector-guile.scm")
  (include "../geiser-inspector.scm"))
