(define (hash-table->alist hash-table)
  (let ((alist '()))
    (hash-table-walk
     hash-table
     (lambda (key value)
       (set! alist
         (cons (cons key value)
               alist))))
    alist))

(define-record-type inspector
  (make-inspector object history state)
  inspector?
  (object inspector-object inspector-object-set!)
  (history inspector-history inspector-history-set!)
  (state inspector-state inspector-state-set!))

(define (record-field-value record field-name)
  (let* ((rtd (record-type-descriptor record))
         (field-acc (record-accessor rtd field-name)))
    (field-acc record)))

(define (record-field-names record)
  (record-type-fields (record-type-descriptor record)))
