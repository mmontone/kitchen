(define-syntax unless
  (syntax-rules ()
    ((unless ?form ?clause ...) ;=>
     (if (not ?form) (begin ?clause ...)))))


(define-syntax when
  (syntax-rules ()
    ((when ?form ?clause ...) ;=>
     (if ?form (begin ?clause ...)))))
