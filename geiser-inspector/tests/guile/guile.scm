;; Record inspection
(define-record-type foo-record
  (make-foo-record foo bar baz)
  foo-record?
  (foo foo-record-foo)
  (bar foo-record-bar)
  (baz foo-record-baz))
