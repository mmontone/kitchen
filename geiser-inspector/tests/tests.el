(with-current-buffer "scheme-inspector.scm"
  (geiser-inspector-inspect-expression "33"))

(with-current-buffer "scheme-inspector.scm"
  (geiser-inspector-inspect-expression "'asdf"))

(with-current-buffer "scheme-inspector.scm"
  (geiser-inspector-inspect-expression "(cons 2323 434)"))

(with-current-buffer "*geiser-inspector*"
  (geiser-inspector--access-object 'car))

(with-current-buffer "scheme-inspector.scm"
  (geiser-inspector-inspect-expression "(list 1 2 3)"))

(with-current-buffer "scheme-inspector.scm"
  (geiser-inspector-inspect-expression "(vector 1 2 3)"))

(with-current-buffer "*geiser-inspector*"
  (geiser-inspector--access-object 0))

(with-current-buffer "scheme-inspector.scm"
  (geiser-inspector-inspect-expression
   "(let ((ht (make-hash-table)))
                  (hashtable-set! ht 'foo \"foo\")
                  (hashtable-set! ht 'bar \"bar\")
                  ht)"))
