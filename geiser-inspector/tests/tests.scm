;; set! #t to avoid breaking into the debugger
(define treat-errors-as-warnings #f)

(define (test-warn msg)
  (display "WARNING: ")
  (display msg)
  (newline))

(define (test-error msg)
  (display "ERROR:")
  (display msg)
  (newline))

;; Light weight unit testing..
;; TODO: wrap w general exception handler & restart
(define-syntax expect
  (syntax-rules ()
    ((expect ?desc ?form ?expected ) ;=>
     (expect ?desc ?form ?expected equal?))
    ((expect ?desc ?form ?expected  ?compare) ;=>
     (let ((expected ?expected)
           (actual   ?form)
           (desc ?desc))
       (display (symbol->string desc))
       (display " . ")
       (unless (?compare expected actual)
         ((if treat-errors-as-warnings test-warn test-error)
          (format #f
                  "~%~s: expected: ~s~% got: ~s~% from: ~s"
                  desc
                  expected
                  actual
                  '?form)))))))

;; The tests
;; Check these tests pass for the Scheme implementation

(define (aget alist key)
  (cdr (assoc key alist)))

(define (run-tests)

  ;; Number inspection
  (define iobj (inspect-object 1))
  (expect 'number-type (aget iobj ':type ) 'number?)
  (expect 'number-display (aget iobj ':display) "1")

  ;; String inspection
  (set! iobj (inspect-object "foo"))
  (expect 'string-type (aget iobj ':type ) 'string?)
  (expect 'string-display (aget iobj ':display) "\"foo\"")

  ;; Symbol inspection
  (set! iobj (inspect-object 'foo))
  (expect 'symbol-type (aget iobj ':type) 'symbol?)
  (expect 'symbol-display (aget iobj ':display) "foo")

  ;; List inspection
  (set! iobj (inspect-object (list 1 2 3)))
  (expect 'list-type (aget iobj ':type) 'list?)
  (expect 'list-contents (list? (aget iobj ':contents)) #t)  

  (set! iobj (inspect-object (make-foo-record 'foo 'bar 'baz)))
  (expect 'record-type (aget iobj ':type) 'record?)
  (expect 'record-contents (aget (aget iobj ':contents) 'foo) "foo")
  (expect 'record-contents-2 (aget (aget iobj ':contents) 'bar) "bar")
  (expect 'record-contents-3 (aget (aget iobj ':contents) 'baz) "baz")

  ;; Hashtable inspection
  (set! iobj
    (inspect-object (let ((ht (make-hash-table)))
                      (hash-table-set! ht 'foo "foo")
                      (hash-table-set! ht 'bar "bar")
                      ht)))
  (expect 'hash-table-type (aget iobj ':type) 'hash-table?)
  (expect 'hash-table-contents (length (aget iobj ':contents)) 2)

  ;; Pairs inspection
  (set! iobj (inspect-object (cons 1 2)))
  (expect 'pairs-type  (aget iobj ':type) 'pair?)
  (expect 'pairs-content (pair? (aget iobj ':contents)) #t)

  ;; Navigation

  (inspect-object 'foo)
  (expect 'nav-1 (inspector-object (current-inspector)) 'foo)
  (inspector-go-back)
  (expect 'nav-2 (inspector-object (current-inspector)) 'foo)

  ;; Navigation of conses
  (let ((cons-obj (cons 1 2)))
    (inspect-object cons-obj)
    (expect 'cons-nav (inspector-object (current-inspector)) cons-obj)
    (inspector-access-object 'car)
    (expect 'cons-nav-2 (inspector-object (current-inspector)) 1)
    (inspector-go-back)
    (expect 'nav-go-back (inspector-object (current-inspector)) cons-obj)
    (inspector-go-back)
    (inspector-access-object 'cdr)
    (expect 'nav-go-back-2 (inspector-object (current-inspector)) 2))

  ;; Navigation of lists
  (let ((list-obj (list 1 2 3)))
    (inspect-object list-obj)
    (expect 'list-nav (inspector-object (current-inspector)) list-obj)
    (inspector-access-object 0)
    (expect 'list-nav-2 (inspector-object (current-inspector)) 1)
    (inspector-go-back)
    (inspector-access-object 1)
    (expect 'list-nav-3 (inspector-object (current-inspector)) 2))

  ;; Navigation of hash-tables
  (let ((ht (make-hash-table)))
    (hash-table-set! ht 'foo "foo")
    (hash-table-set! ht 'bar "bar")
    (inspect-object ht)
    (inspector-access-object '(0 . key))
    (expect 'hash-nav (inspector-object (current-inspector)) 'foo)
    (inspector-go-back)
    (inspector-access-object '(0 . value))
    (expect 'hash-nav-2 (inspector-object (current-inspector)) "foo")
    (inspector-go-back)
    (inspector-access-object '(1 . key))
    (expect 'hash-nav-3 (inspector-object (current-inspector)) 'bar)
    (inspector-go-back)
    (inspector-access-object '(1 . value))
    (expect 'hash-nav-4 (inspector-object (current-inspector)) "bar"))

  ;; Vector navigation
  (let ((obj (vector 1 2 3)))
    (inspect-object obj)
    (inspector-access-object 0)
    (expect 'vector-nav (inspector-object (current-inspector)) 1)
    (inspector-go-back)
    (inspector-access-object 2)
    (expect 'vector-nav-1 (inspector-object (current-inspector)) 3)))
