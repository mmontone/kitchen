;; Max content fetched for lists, vectors, hashtables, etc.
(define inspector-max-contents 100)

(define (vector-index-of vector elem)
  (let ((idx 0))
    (call/cc
     (lambda (return)
       (vector-for-each
        (lambda (el)
          (when (eq? el elem)
            (return idx))
          (set! idx (1+ idx)))
        vector)
       #f))))

(define (write-to-string object)
  (with-output-to-string
    (lambda ()
      (write object))))

(define (display-to-string object)
  (write-to-string object))

;; Current implementation only supports a single inspector at a time.
;; It would be possible to implement support for multiple inspectors.
;; They should be assigned an ID and pass it around for that.
(define *inspector* '())

(define (current-inspector)
  *inspector*)

;; Inspect current inspector object
(define (inspector-inspect-object)
  (let ((object (inspector-object *inspector*)))
    (cond
     ((number? object)
      (list (cons ':type 'number?)
            (cons ':display (number->string object))))
     ((symbol? object)
      (list (cons ':type 'symbol?)
            (cons ':display (symbol->string object))))
     ((boolean? object)
      (list (cons ':type 'boolean?)
            (cons ':display (display-to-string object))))
     ((null? object)
      (list (cons ':type 'null?)
            (cons ':display (display-to-string object))))
     ((string? object)
      (list (cons ':type 'string?)
            (cons ':display (display-to-string object))))
     ((list? object)
      (list (cons ':type 'list?)
            ;; TODO: shorten
            (cons ':display (display-to-string object))
            (cons ':contents (map display-to-string object))))
     ((pair? object)
      (list (cons ':type 'pair?)
            (cons ':display (display-to-string object))
            (cons ':contents (cons (display-to-string (car object))
                                   (display-to-string (cdr object))))))
     ((hash-table? object)
      (list (cons ':type 'hash-table?)
            (cons ':display (display-to-string object))
            (cons ':contents (map (lambda (pair)
                                    (cons (display-to-string (car pair))
                                          (display-to-string (cdr pair))))
                                  (hash-table->alist object)))))
     ((vector? object)
      (list (cons ':type 'vector?)
            (cons ':display (display-to-string object))
            (cons ':contents (map display-to-string (vector->list object)))))
     ((record? object)
      (list (cons ':type 'record?)
            (cons ':display (display-to-string object))
            (cons ':contents (map (lambda (field-name)
                                    (cons field-name (display-to-string (record-field-value object field-name))))
                                  (record-field-names object)))))
     (else
      (list (cons ':type 'datum?)
            (cons ':display (display-to-string object)))))))

;; Creates a new inspector instance on OBJECT
;; Called by Geiser from Emacs side
(define (inspect-object object)
  (set! *inspector* (make-inspector object '() '()))
  (inspector-inspect-object))

(define (inspector-list-ref list index)
  (list-ref list index))

(define (inspector-pair-ref pair index)
  (cond
   ((eq? index 'car)
    (car pair))
   ((eq? index 'cdr)
    (cdr pair))))

(define (inspector-hash-table-ref ht index)
  ;; Index is a pair with entry index and either :key or :value
  (let ((entry (list-ref (hash-table->alist ht) (car index))))
    (cond
     ((eq? (cdr index) 'key)
      (car entry))
     ((eq? (cdr index) 'value)
      (cdr entry)))))

(define (inspector-object-ref obj index)
  (cond
   ((list? obj) (inspector-list-ref obj index))
   ((pair? obj) (inspector-pair-ref obj index))
   ((vector? obj) (vector-ref obj index))
   ((hash-table? obj) (inspector-hash-table-ref obj index))
   ((record? obj) (record-field-value obj index))
   (else
    (error 'inspector-object-ref "TODO"))))

;; Access current inspector object at index
(define (inspector-access-object index)
  (let ((obj (inspector-object-ref (inspector-object *inspector*) index)))
    ;; Add current object to history
    (inspector-history-set!
     *inspector*
     (cons (inspector-object *inspector*)
           (inspector-history *inspector*)))
    ;; Set current object to inspect
    (inspector-object-set! *inspector* obj)
    ;; Inspect the accessed object
    (inspector-inspect-object)))

(define (inspector-go-back)
  (if (null? (inspector-history *inspector*))
      (inspect-object (inspector-object *inspector*))
      (let ((prev-object (car (inspector-history *inspector*))))
        (inspector-history-set! *inspector* (cdr (inspector-history *inspector*)))
        (inspector-object-set! *inspector* prev-object)
        (inspector-inspect-object))))

(define (inspector-pprint-object)
  (let ((obj (inspector-object *inspector*)))
    (with-output-to-string
      (lambda ()
        (pretty-print obj)))))

(define (read-and-inspect str)
  (let ((obj (with-input-from-string str
               (lambda () (read)))))
    (inspect-object obj)))
