(define-record-type inspector
  (make-inspector object history state)
  inspector?
  ;; The object being inspected
  (object inspector-object inspector-object-set!)
  ;; The navigation history. A list with objects inspected.
  (history inspector-history inspector-history-set!)
  ;; Inspector state. Used for storing current object status.
  (state inspector-state inspector-state-set!))
