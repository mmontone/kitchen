(library (srfi-69)
  (export
   hash-table-ref
   hash-table-set!)
  (import (scheme))
  
  (define (hash-table-ref table key)
    (hashtable-ref table key #f))

  (define (hash-table-set! table key new-value)
    (hashtable-set! table key new-value)))
