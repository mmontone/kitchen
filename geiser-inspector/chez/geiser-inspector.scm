(library (geiser-inspector)
  (export
   inspector-max-contents
   inspect-object
   inspector-inspect-object
   inspector-access-object
   inspector-go-back
   inspector-pprint-object
   read-and-inspect
   inspector-object
   current-inspector)
  (import (scheme))
  (import (srfi-69))
  (include "geiser-inspector-chez.scm")
  (include "geiser-inspector-common.scm"))
