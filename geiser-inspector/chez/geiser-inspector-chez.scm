(define-record-type inspector
  (fields
   (mutable object) ;; The object being inspected
   (mutable history) ;; The navigation history. A list with objects inspected.
   (mutable state) ;; Inspector state. Used for storing current object status.
   ))

(define (record-field-value record field-name)
  (let* ((rtd (record-rtd record))
         (field-names (record-type-field-names rtd))
         (field-index (vector-index-of field-names field-name))
         (field-acc (record-accessor rtd field-index)))
    (field-acc record)))

(define (record-field-names record)
  (vector->list (record-type-field-names (record-rtd record))))

(define (hash-table->alist hash-table)
  (let ((alist '()))
    (hash-table-for-each hash-table
                         (lambda (key value)
                           (set! alist
                             (cons (cons key value)
                                   alist))))
    alist))
