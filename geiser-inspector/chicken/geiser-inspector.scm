(module geiser-inspector ()
  (export
   inspector-max-contents
   inspect-object
   inspector-inspect-object
   inspector-access-object
   inspector-go-back
   inspector-pprint-object
   read-and-inspect)
  (import
    (scheme)
    (chicken base)
    (chicken file)
    (chicken format)
    (chicken pretty-print)
    (chicken module)
    (chicken eval)
    (chicken io)
    (srfi-1)
    (srfi-13)
    (srfi-69))
;; (import (chicken condition))
;; (import (chicken foreign))
;; (import (chicken keyword))
;; (import (chicken file))
;; (import (chicken file posix))
;; (import (chicken fixnum))
;; (import (chicken format))
;; (import (chicken irregex))
;; (import (chicken module))
;; (import (chicken tcp))
;; (import (chicken port))
;; (import (chicken platform))
;; (import (chicken internal))
;; (import (chicken io))
;; (import (chicken sort))
;; (import (chicken time))
;; (import (chicken pathname))
;;(import (chicken process))
;;(import (chicken process-context))
;;(import (chicken pretty-print))
;;(import (chicken string))

  (include "../geiser-inspector-srfi.scm")
  (include "geiser-inspector-chicken.scm")
  (include "../geiser-inspector.scm"))
