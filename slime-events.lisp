(require :swank)

(in-package :swank)

;; (swank:ed-rpc-no-wait 'slime-trigger-event :my-event)

(defun trigger-slime-event (event &rest args)
  (when swank::*emacs-connection*
    (apply #'swank:ed-rpc-no-wait
	   'slime-trigger-event
	   event
	   args)))

(export 'trigger-slime-event)

(provide :slime-events)

