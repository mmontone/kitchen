(require 'slime)
(require 'direx)
(require 'cl-lib)
(require 'eieio)

(defun aget (assoc key)
  (cdr (assoc key assoc)))

(defclass slime-direx:system-node (direx:node)
  ((packages :initarg :packages
	     :accessor slime-direx::packages))
  (:documentation "direx node for ASDF systems."))

(defclass slime-direx:package-node (direx:node)
  ((package-name :initarg :package-name
		 :accessor slime-direx::package-name))
  (:documentation "direx node for Lisp packages"))

(defclass slime-direx:package-item (direx:item)
  ())

(defclass slime-direx:definition-node (direx:node)
  ((symbol :initarg :symbol :accessor slime-direx::definition-symbol)
   (type :accessor :initarg :type slime-direx::definition-type)))

(cl-defmethod direx:make-item ((package-node slime-direx:package-node) parent)
  (make-instance 'slime-direx:package-item
		 :tree package-node
		 :parent parent))

(cl-defmethod direx:node-children ((package-node slime-direx:package-node))
  (let ((package-info (slime-eval `(swank-help:read-emacs-package-info ,(slime-direx::package-name package-node)))))
    (cl-loop for def in (aget package-info :external-symbols)
	     collect (make-instance 'slime-direx:definition-node
				    :symbol (aget def :symbol)
				    :type (aget def :type)))))

(cl-defmethod direx:node-children ((system-node slime-direx:system-node))
  (let ((packages (slime-eval `(cl:mapcar 'cl:package-name (cl:list-all-packages)))))
    (cl-loop for package in packages
	     collect
	     (make-instance 'slime-direx:package-node
			    :name package
			    :package-name package))))

(cl-defmethod direx:make-item ((system-node slime-direx:system-node) parent)
  (make-instance 'slime-direx:system-item))

(defun slime-direx:browse-system ()
  (interactive)
  (let ((root (make-instance 'slime-direx:system-node :name "Packages")))
    ;;(direx:ensure-buffer-for-root root)
    (switch-to-buffer (direx:make-buffer-for-root root))))

(slime-direx:browse-system)

(direx:find-root-item-for-root
 (make-instance 'slime-direx:system-node
		:name "CL"))

(cl-defmethod direx:make-buffer ((root slime-direx:system-node))
  (let ((buffer (generate-new-buffer (direx:tree-name root))))
    (with-current-buffer buffer
      (direx:direx-mode)
      (setq-local revert-buffer-function 'direx:revert-buffer))
    buffer))

(direx:make-buffer
 (make-instance 'slime-direx:system-node :name "CL"))

    
(provide 'slime-direx)
