(defpackage :contexts.modules
  (:use :cl)
  (:export
   #:defmodule
   #:init-module))

(in-package :contexts.modules)

(defun set-package-context (package context)
  (setf (symbol-value (intern "*MODULE-CONTEXT*" package))
        context))

(defun get-package-context (package)
  (let ((binding (intern "*MODULE-CONTEXT*" package)))
    (when (boundp binding)
      (symbol-value binding))))

(defmacro defmodule (name &body options)
  (let ((context (find :context options :key #'car))
        (package-options (remove :context options :key #'car)))
    (eval `(cl:defpackage ,name ,@package-options))
    (let ((*package* (find-package name)))
      `(eval-when (:compile-toplevel :load-toplevel :execute)
         ,@(loop for binding in (cdr context)
                 for binding-name := (intern (symbol-name binding) name)
                 collect `(define-symbol-macro ,binding-name
                              (contexts:context-value ',binding-name (get-package-context ',name))))))))

(defmacro in-module (string-designator)
  `(cl:in-package ,string-designator))

(defun init-module (module-name args)
  (set-package-context module-name args))

;; Example

;; Define a module with context variables
(defmodule my-database-module
  (:use :cl)
  (:export #:connect-db)
  ;; The context variables ..
  ;; I prefix context variables with % so that they can be distinguished from
  ;; other variables, but it is not obligatory .
  (:context %db-driver %db-host %db-port))

(cl:in-package my-database-module)

(defun driver-connect-db (driver host port)
  (format t "Connecting to db: ~s~%" (list driver host port)))

;; Database connection function that uses variables from module context
(defun connect-db ()
  (driver-connect-db %db-driver %db-host %db-port))

;; This call signals context error: Not bound in current context: %DB-DRIVER
(connect-db)

;; Initialize context variables in module
;; There's room for improvement: should remove the 'my-database-module::' prefix ...
(contexts.modules:init-module
 'my-database-module
 '((my-database-module::%db-driver . :postgres)
   (my-database-module::%db-host . "localhost")
   (my-database-module::%db-port . 5432)))

;; Now the call to connect-db works:
(connect-db)
