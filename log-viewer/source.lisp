(in-package :log-viewer)

(defclass log-source ()
  ()
  (:documentation "Source of the log"))

(defclass file-log-source (log-source)
  ((filename :initarg :filename
             :accessor filename
             :type pathname)
   (log-format :initarg :log-format
               :accessor log-format))
  (:documentation "Log file"))

(defvar *default-dbi-fields-mapping*
  '(("category" . category)
    ("timestamp" . timestamp)
    ("message" . message)
    ("data" . data)))

(defclass dbi-log-source (log-source)
  ((connection-spec :initarg :connection-spec
                    :accessor connection-spec)
   (table-name :initarg :table-name
               :accessor table-name)
   (fields-mapping :accessor fields-mapping
                   :initform *default-dbi-fields-mapping*))
  (:documentation "Read log from a relational database table."))

(defclass elastic-log-source (log-source)
  ((connection-spec :initarg :connection-spec
                    :accessor connection-spec))
  (:documentation "Read log from an elastic search database."))

;; API

(defgeneric start-reading-from-source (source)
  (:documentation "Prepare for reading from SOURCE. Returns a handle (for example, a cursor or database connection) to access the source."))

(defgeneric stop-reading-from-source (source cursor)
  (:documentation "Stops reading from source. Clear resources."))

(defgeneric read-next-item (cursor source log-parser)
  (:documentation "Read next LOG-ITEM from SOURCE using CURSOR."))

(defgeneric read-next-items (cursor source log-parser n)
  (:documentation "Read the next N LOG-ITEMs from SOURCE using CURSOR."))

(defclass log-cursor ()
  ())

(defgeneric close-cursor (log-cursor))

(defclass file-cursor (log-cursor)
  ((file-stream :initarg :file-stream
                :accessor cursor-file-stream)
   (file-position :initarg :file-position
                  :accessor cursor-file-position
                  :initform :end)))

(defmethod close-cursor ((log-cursor file-cursor))
  (close (cursor-file-stream log-cursor)))

(defclass buffered-file-cursor (log-cursor)
  ((buffered-file-stream)
   (file-stream :initarg :file-stream
		:accessor cursor-file-stream)))

(defmethod close-cursor ((log-cursor buffered-file-cursor))
  (close (cursor-file-stream log-cursor)))

(defmethod initialize-instance :after ((cursor buffered-file-cursor) &rest initargs)
  (declare (ignore initargs))
  (with-slots (buffered-file-stream file-stream) cursor
    (setf buffered-file-stream (make-instance 'buffered-file-stream::reverse-buffered-file-stream :file-stream file-stream))))

(defmethod print-object ((file-cursor file-cursor) stream)
  (print-unreadable-object (file-cursor stream :type t :identity t)
    (format stream "~a" (cursor-file-position file-cursor))))

(defmethod start-reading-from-source ((source file-log-source))
  #+nil(make-instance 'file-cursor
		 :file-stream (open (filename source) :direction :input
						      :external-format :utf8))
  (make-instance 'buffered-file-cursor
		 :file-stream (open (filename source) :direction :input
						      :external-format :utf8))
  )

(defmethod stop-reading-from-source (source cursor)
  (close-cursor cursor))
		 
(defmethod read-next-item ((cursor file-cursor) (source file-log-source)
                           log-parser)
  #+todo
  (let ((log-item-source "")
	(file-stream (cursor-file-stream cursor)))
    (let ((file-position (cond
                           ((eq (cursor-file-position cursor) :end)
                            (1- (file-length file-stream)))
                           ((integerp (cursor-file-position cursor))
                            (cursor-file-position cursor))
                           (t (error "Invalid file position: ~s" (cursor-file-position cursor))))))
        (file-position file-stream file-position)
        (loop for line = (read-line file-stream)
              do (setf log-item-source (concatenate 'string log-item-source
                                                    line))
	      while (and (> (file-position file-stream) 0)
                         (not (match-item-start log-parser line))))
      (prog1
	  (parse-item log-parser log-item-source)
	(setf (cursor-file-position cursor) (file-position file-stream))))))

(defmethod read-next-item ((cursor buffered-file-cursor) (source file-log-source)
                           log-parser)
  (let ((log-item-source ""))
    (with-slots (buffered-file-stream) cursor
      (loop for line = (nreverse (buffered-file-stream:read-line buffered-file-stream nil nil))
            do (setf log-item-source
		     (concatenate 'string line (make-string 1 :initial-element #\newline) log-item-source))
	    while (and (not (match-item-start log-parser line))
		       (not (buffered-file-stream:stream-at-end buffered-file-stream))))
      (when (and (not (zerop (length log-item-source)))
		 (match-item-start log-parser log-item-source))
	(parse-item log-parser log-item-source)))))
