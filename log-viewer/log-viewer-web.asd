(asdf:defsystem #:log-viewer-web
  :description "Log parser and viewer tool."
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license  "GPL"
  :version "0.0.1"
  :serial t
  :components ((:file "web"))
  :depends-on (:log-viewer :hunchentoot :easy-routes :spinneret))
