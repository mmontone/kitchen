(in-package :log-viewer)

(defclass log-item ()
  ((timestamp :initarg :timestamp
              :accessor timestamp
              :type integer)
   (category :initarg :category
             :accessor category
             :type string)
   (log-level :initarg :log-level
              :accessor log-level
              ;;:type (member :info :debug :warning :error)
              )
   (message :initarg :message
            :accessor message
            :type string)
   (data :initarg :data
         :accessor data)
   (source :initarg :source
           :accessor source
           :type string)))

(defmethod print-object ((log-item log-item) stream)
  (print-unreadable-object (log-item stream :type t :identity t)
    (with-slots (timestamp category log-level message) log-item
    (format stream "~a [~a.~a]: ~s" timestamp category log-level message))))

(defclass log-parser ()
  ()
  (:documentation "Parse logs in a particular format."))

(defclass unstructured-log-parser (log-parser)
  ())

(defgeneric match-item-start (log-parser string))
(defgeneric parse-item (log-parser string))

(defclass pluggable-log-parser (unstructured-log-parser)
  ((item-start-matcher :initarg :item-start-matcher
                       :type (or function symbol)
                       :documentation "A function that returns T when a log line is the start of a log item.")
   (item-parser :initarg :item-parser
                :type (or function symbol)
                :documentation "A function for parsing the matched item string into a proper LOG-ITEM object.")))

(defmethod match-item-start ((log-parser pluggable-log-parser) string)
  (funcall (slot-value log-parser 'item-start-matcher) string))

(defmethod parse-item ((log-parser pluggable-log-parser) string)
  (funcall (slot-value log-parser 'item-parser) string))

(defgeneric parse-log-item (format string)
  (:documentation "Create a LOG-ITEM from STRING, that has format FORMAT."))

(defmethod parse-log-item ((format (eql :json)) string)
  (let ((json (json:decode-json-from-string string)))
    (make-instance 'log-item
                   :source string
                   :timestamp (alexandria:assoc-value json :timestamp)
                   :category (alexandria:assoc-value json :category)
                   :message (alexandria:assoc-value json :message)
                   :log-level (alexandria:assoc-value json :log-level)
                   :data (alexandria:assoc-value json :data))))

;; Hunchentoot log example:

;; Thu, 14 Apr 2022 06:25 INFO:(  8) Request: GET http:bonanza.copyleft.no/   remote-ip-addr: 178.255.144.127   user-agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0  referer: NIL
;; Thu, 14 Apr 2022 06:25 INFO:(  8) New session session-id-93t=ESXYFPIM0RJ8Z4F0OT25ON664
;; Thu, 14 Apr 2022 06:25 INFO:(  8) Set-Cookie: session-id-93t=ESXYFPIM0RJ8Z4F0OT25ON664; Path=/

(defmethod parse-log-item ((format (eql :hunchentoot)) string)
  (let ((timestamp (chronicity:parse (subseq string 0 22)))
        (category (subseq string 23 (+ 23 (position #\: (subseq string 23)))))
        (message (subseq string (1+ (position #\) string)))))
    (make-instance 'log-item
                   :timestamp (local-time:timestamp-to-universal timestamp)
                   :category category
                   :log-level (alexandria:make-keyword category)
                   :message message
                   :source string)))

(defclass hunchentoot-log-parser (unstructured-log-parser)
  ())

(defmethod match-item-start ((parser hunchentoot-log-parser) line-string)
  (and (> (length line-string) 22)
       (not (null (ignore-errors (chronicity:parse (subseq line-string 0 22)))))))

(defmethod parse-item ((parser hunchentoot-log-parser) string)
  (let ((timestamp (chronicity:parse (subseq string 0 22)))
        (category (subseq string 23 (+ 23 (position #\: (subseq string 23)))))
        (message (subseq string (1+ (position #\) string)))))
    (make-instance 'log-item
                   :timestamp (local-time:timestamp-to-universal timestamp)
                   :category category
                   :log-level (alexandria:make-keyword category)
                   :message message
                   :source string)))

;; Hunchentoot error log

;; [2022-04-21 11:48:36 [ERROR]] this is an error

;; Format is:
;; [<iso-time> [<log-level>]] <error message>

(defclass hunchentoot-error-log-parser (unstructured-log-parser)
  ())

(defmethod match-item-start ((parser hunchentoot-error-log-parser) line-string)
  (and (> (length line-string) 22)
       (char= (aref line-string 0) #\[)
       (not (null (ignore-errors (chronicity:parse (subseq line-string 1 20)))))))

(defmethod parse-item ((parser hunchentoot-error-log-parser) string)
  (let ((timestamp (chronicity:parse (subseq string 1 20)))
        (log-level (subseq string 22 (+ 22 (position #\] (subseq string 22)))))
        (message (subseq string (+ (search "]]" string) 2) (position #\newline string))))
    (make-instance 'log-item
                   :timestamp (local-time:timestamp-to-universal timestamp)
                   :category "HUNCHENTOOT"
                   :log-level (alexandria:make-keyword log-level)
                   :message message
                   :source string)))

(match-item-start (make-instance 'hunchentoot-error-log-parser) "[2022-04-21 11:48:36 [ERROR]] this is an error")
(parse-item (make-instance 'hunchentoot-error-log-parser) "[2022-04-21 11:48:36 [ERROR]] this is an error")

;; Monolog log example:

;; [2022-04-13 09:48:42] PRODUCTION.INFO: USER LOGIN SUCCESS. helsestue@hotmail.com via Mozilla/5.0 (iPad; CPU OS 15_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/100.0.4896.77 Mobile/15E148 Safari/604.1  
;; [2022-04-13 09:49:50] PRODUCTION.INFO: USER LOGIN SUCCESS. einaraa@live.no via Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.60 Safari/537.36  
;; [2022-04-13 17:31:51] PRODUCTION.ERROR: Method App\Http\Controllers\Auth\PasswordController::getEmail does not exist. {"exception":"[object] (BadMethodCallException(code: 0): Method App\\Http\\Controllers\\Auth\\PasswordController::getEmail does not exist. at /home/prod/50pluss/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:68)
;; [stacktrace]
;; #0 [internal function]: Illuminate\\Routing\\Controller->__call()
;; #1 /home/prod/50pluss/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(54): call_user_func_array()
;; #2 /home/prod/50pluss/vendor/laravel/framework/src/Illuminate/Routing/ControllerD

(defclass monolog-log-parser (unstructured-log-parser)
  ())

(defmethod match-item-start ((parser monolog-log-parser) string)
  (and (position #\[ string)
       (zerop (position #\[ string))
       (>= (length string) 20)
       (chronicity:parse (subseq string 1 20))))

(defmethod parse-item ((parser monolog-log-parser) string)
  (let ((timestamp (chronicity:parse (subseq string 1 20)))
        (category (subseq string (1+ (position #\] string))
                          (+ 20 (position #\: (subseq string 20)))))
        (message (subseq string (+ (position #\: (subseq string 20)) 21)
                         (position #\newline string))))
    (make-instance 'log-item
                   :timestamp (local-time:timestamp-to-universal timestamp)
                   :category category
                   :log-level (alexandria:make-keyword category)
                   :message message
                   :source string)))

(parse-item (make-instance 'monolog-log-parser) "[2022-04-13 09:48:42] PRODUCTION.INFO: USER LOGIN SUCCESS. helsestue@hotmail.com via Mozilla/5.0 (iPad; CPU OS 15_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/100.0.4896.77 Mobile/15E148 Safari/604.1  ")

;; unix syslog

;; Apr 20 00:00:13 gimli systemd[1]: logrotate.service: Succeeded.
;; Apr 20 00:00:13 gimli systemd[1]: Finished Rotate log files.
;; Apr 20 00:05:01 gimli CRON[604929]: (root) CMD (command -v debian-sa1 > /dev/null && debian-sa1 1 1)
;; Apr 20 00:05:48 gimli NetworkManager[306017]: <info>  [1650423948.6433] manager: NetworkManager state is now CONNECTED_SITE
;; Apr 20 00:05:48 gimli dbus-daemon[815]: [system] Activating via systemd: service name='org.freedesktop.nm_dispatcher' unit='dbus-org.freedesktop.nm-dispatcher.service' requested by ':1.2997' (uid=0 pid=306017 comm="/usr/sbin/NetworkManager --no-daemon " label="unconfined")
;; Apr 20 00:05:48 gimli systemd[1]: Starting Network Manager Script Dispatcher Service...
;; Apr 20 00:05:48 gimli dbus-daemon[815]: [system] Successfully activated service 'org.freedesktop.nm_dispatcher'
;; Apr 20 00:05:48 gimli systemd[1]: Started Network Manager Script Dispatcher Service.
;; Apr 20 00:05:49 gimli NetworkManager[306017]: <info>  [1650423949.0059] manager: 

(defclass unix-syslog-log-parser (unstructured-log-parser)
  ())

(defmethod match-item-start ((parser unix-syslog-log-parser) string)
  (and (>= (length string) 15)
       (chronicity:parse (subseq string 0 15))))

(defmethod parse-item ((parser unix-syslog-log-parser) string)
  (let ((timestamp (chronicity:parse (subseq string 0 15)))
        (category (subseq string 15
                          (+ 15 (position #\: (subseq string 15)))))
        (message (subseq string (+ (position #\: (subseq string 15)) 16)
                         (position #\newline string))))
    (make-instance 'log-item
                   :timestamp (local-time:timestamp-to-universal timestamp)
                   :category category
                   :log-level :info
                   :message message
                   :source string)))

;;(parse-item (make-instance 'unix-syslog-log-parser) "Apr 20 00:00:13 gimli systemd[1]: logrotate.service: Succeeded.")

;; -- tests ----------------------

(defun test-parse-hunchentoot-log (pathname)
  (let ((source (make-instance 'file-log-source
			       :log-format :hunchentoot
			       :filename pathname))
	(log-parser (make-instance 'hunchentoot-log-parser)))
    
    (let ((cursor (start-reading-from-source source)))
      (unwind-protect
	   (loop for log-item := (read-next-item cursor source log-parser)
		 while log-item
		 collect log-item)		 
	(stop-reading-from-source source cursor)))))

(defun test-parse-monolog-log (pathname)
  (let ((source (make-instance 'file-log-source
			       :log-format :monolog
			       :filename pathname))
	(log-parser (make-instance 'monolog-log-parser)))
    
    (let ((cursor (start-reading-from-source source)))
      (unwind-protect
	   (loop for log-item := (read-next-item cursor source log-parser)
		 while log-item
		 collect log-item)		 
	(stop-reading-from-source source cursor)))))

(defun test-parse-unix-syslog (pathname)
  (let ((source (make-instance 'file-log-source
			       :log-format :unix-syslog
			       :filename pathname))
	(log-parser (make-instance 'unix-syslog-log-parser)))
    
    (let ((cursor (start-reading-from-source source)))
      (unwind-protect
	   (loop for log-item := (read-next-item cursor source log-parser)
		 while log-item
		 collect log-item)		 
	(stop-reading-from-source source cursor)))))

(defun parse-log (pathname log-parser)
  (let ((source (make-instance 'file-log-source :filename pathname)))
    (let ((cursor (start-reading-from-source source)))
      (unwind-protect
	   (loop for log-item := (read-next-item cursor source log-parser)
		 while log-item
		 collect log-item)		 
	(stop-reading-from-source source cursor)))))

;;(test-parse-unix-syslog #p"/var/log/syslog")

;; flask

;; [2020-06-26 16:00:36,292] INFO in app: main info
;; [2020-06-26 16:00:36,292] WARNING in app: main warning
;; [2020-06-26 16:00:36,293] ERROR in app: main error
;; [2020-06-26 16:00:36,293] CRITICAL in app: main critical


