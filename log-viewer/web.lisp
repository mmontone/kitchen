(require :spinneret)
(require :easy-routes)

(defpackage :log-viewer/web
  (:use :cl)
  (:local-nicknames
   (:routes :easy-routes)
   (:hunch :hunchentoot)
   (:spinn :spinneret))
  (:export :start))

(in-package :log-viewer/web)

(defvar *acceptor*)

(defun start (&key (port 0))
  (setf *acceptor* (hunch:start (make-instance 'routes:easy-routes-acceptor :port port))))

(defun stop ()
  (hunch:stop *acceptor*))

(defun format-timestamp (timestamp)
  (local-time:format-timestring nil (local-time:universal-to-timestamp timestamp)
                                :format local-time:+asctime-format+))

(defun render-log-page (log)
  (spinn:with-html-string
    (:html
     (:head
      (:title "Log")
      (:link :rel "stylesheet" :href "https://cdnjs.cloudflare.com/ajax/libs/w3-css/4.1.0/w3.css" :integrity "sha512-Ef5r/bdKQ7JAmVBbTgivSgg3RM+SLRjwU0cAgySwTSv4+jYcVeDukMp+9lZGWT78T4vCUxgT3g+E8t7uabwRuw==" :crossorigin "anonymous"))
     (:body
      (:span "Log level: ")
      (:select
          (:option :value "error" "Error")
        (:option :value "info" "Info"))
      (:span :style "margin-left: 10px;" "Category: ")
      (:input)
      (:span :style "margin-left: 10px;" "Time:")
      (:input :type "date")
      (:span :style "margin-left: 10px;" "Search:")
      (:input)
      (:table :class "w3-table w3-striped"
              (:thead
               (:tr
                (:th "Timestamp")
                (:th "Message")
                (:th "Log level")))
              (:tbody
               (loop for log-item in log
                     do
                        (:tr
                         (:td (format-timestamp (log-viewer::timestamp log-item)))
                         (:td
                          (log-viewer::message log-item)
                          (let ((details-id (gensym))
                                (source-id (gensym))
                                (modal-id (gensym)))
                            (:span :class "w3-tag w3-orange" :style "cursor:pointer; margin-right: 5px; color: white;" :onclick (format nil "javascript:$('#~a').toggle();" details-id)
                                   "details")
                            (:span :class "w3-tag w3-orange" :style "cursor:pointer; margin-right: 5px;" :onclick (format nil "javascript:$('#~a').toggle();" source-id) "source")
                            (:span :class "w3-tag w3-orange" :style "cursor:pointer;" :onclick (format nil "javascript:document.getElementById('~a').style.display='block'" modal-id) "open")

                            (:div :id (princ-to-string modal-id) :class "w3-modal"
                                  (:div :class "w3-modal-content"
                                        (:div :class "w3-container"
                                              (:span :onclick (format nil "document.getElementById('~a').style.display='none'" modal-id) :class "w3-button w3-display-topright"
                                                     (:raw "&times;"))
                                              (:pre :style "overflow: scroll;"
                                               (log-viewer::source log-item)))))

                            (:div :id (princ-to-string details-id))
                            (:textarea :id (princ-to-string source-id)
                                       :cols 80 :rows 20
                                       :style "display:none;"
                                       (log-viewer::source log-item))))
                         (:td :style "color:red;"(log-viewer::log-level log-item)))))))
     (:script :src "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" :integrity "sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" :crossorigin "anonymous"))))

(routes:defroute hunchentoot-test ("/hunchentoot-test")
    ()
  (let ((log (log-viewer::test-parse-hunchentoot-log #p"/home/marian/work/bonanza-aa")))
    (render-log-page log)))

(routes:defroute monolog-test ("/monolog-test")
    ()
  (let ((log (log-viewer::test-parse-monolog-log #p"/home/marian/work/50plusstreffen/storage/logs/laravel-2022-04-04.log")))
    (render-log-page log)))
