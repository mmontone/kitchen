(defpackage :buffered-file-stream
  (:use :cl)
  (:shadow
   :read-char
   :read-line)
  (:export
   #:buffered-file-stream
   #:read-char
   #:read-line
   #:stream-at-end))

(in-package :buffered-file-stream)

(defclass buffered-file-stream ()
  ((file-stream :initarg :file-stream)
   (position :initform 0)
   (buffer :initform nil)
   (buffer-extent)
   (buffer-size :initarg :buffer-size
		:initform 4096)))

(defmethod fill-buffer-if-needed ((stream buffered-file-stream))
  (with-slots (buffer buffer-extent position buffer-size file-stream) stream
    (when (or (null buffer)
	      (< position (car buffer-extent))
	      (>= position (cdr buffer-extent)))
      (setf buffer (make-array buffer-size :element-type (stream-element-type file-stream)))
      (cl:read-sequence buffer file-stream)
      (setf buffer-extent (cons position (min (+ position buffer-size)
					      (1- (file-length file-stream))))))))

(defun buffer-read-char (position buffer buffer-extent)
  (assert (<= (car buffer-extent) position (cdr buffer-extent))
	  nil "Out of range")
  (assert (>= position 0) nil "Out of range")
  (aref buffer (- position (car buffer-extent))))

(defgeneric read-char (stream &optional eof-error-p eof))
(defgeneric read-line (stream &optional eof-error-p eof))

(defmethod read-char ((stream buffered-file-stream) &optional (eof-error-p t) eof)
  (with-slots (buffer buffer-extent position file-stream) stream
    (when (>= position (file-length file-stream))
      (if eof-error-p
	  (error "End of file")
	  (return-from read-char eof)))
    (fill-buffer-if-needed stream)
    (prog1
	(buffer-read-char position buffer buffer-extent)
      (incf position))))

(defmethod read-line ((stream buffered-file-stream) &optional (eof-error-p t) eof)
  (let ((chars
	  (loop for char := (read-char stream eof-error-p eof)
		while (and (not (eq char eof))
			   (not (char= char #\newline)))
		collect char)))
    (when chars
      (coerce chars 'string))))

(defun test-1 ()
  (with-open-file (f #p"/home/marian/work/bonanza-aa"
		     :external-format :utf8)
    (let ((bfs (make-instance 'buffered-file-stream
			      :file-stream f))
	  (i 0))
      (loop for char := (read-char bfs nil nil)
	    do (incf i)
	    while char)
      i)))

(defun test-2 ()
  (with-open-file (f #p"/home/marian/work/bonanza-aa"
		     :external-format :utf8)
    (let ((bfs (make-instance 'buffered-file-stream
			      :file-stream f)))
      (loop for line := (read-line bfs nil nil)
	    do (print line)
	    while line))))

(defclass reverse-buffered-file-stream (buffered-file-stream)
  ())

(defmethod initialize-instance :after ((stream reverse-buffered-file-stream) &rest initargs)
  (declare (ignore initargs))
  (with-slots (position file-stream) stream
    (setf position (1- (file-length file-stream)))))

(defmethod read-char ((stream reverse-buffered-file-stream) &optional (eof-error-p t) eof)
  (with-slots (buffer buffer-extent position file-stream) stream
    (when (< position 0)
      (if eof-error-p
	  (error "End of file")
	  (return-from read-char eof)))
    (fill-buffer-if-needed stream)
    (prog1
	(progn
	  ;;(file-position file-stream position)
	  (buffer-read-char position buffer buffer-extent))
      (decf position))))

(defmethod fill-buffer-if-needed ((stream reverse-buffered-file-stream))
  (with-slots (buffer buffer-extent position buffer-size file-stream) stream
    (when (or (null buffer)
	      (< position (car buffer-extent))
	      (>= position (cdr buffer-extent)))
      (setf buffer (make-array buffer-size :element-type (stream-element-type file-stream)))
      (let ((buffer-start (max (- position (1- buffer-size)) 0)))
	(file-position file-stream buffer-start)
	(cl:read-sequence buffer file-stream)
	(setf buffer-extent (cons buffer-start position))))))

(defun test-3 ()
  (with-open-file (f #p"/home/marian/work/bonanza-aa"
		     :external-format :utf8)
    (let ((bfs (make-instance 'reverse-buffered-file-stream
			      :file-stream f))
	  (i 0))
      (loop for char := (read-char bfs nil nil)
	    do (incf i)
	    while char)
      i)))

(defmethod stream-at-end ((stream reverse-buffered-file-stream))
  (with-slots (position) stream
    (< position 0)))

(defun test-4 ()
  (with-open-file (f #p"/home/marian/work/bonanza-aa"
		     :external-format :utf8)
    (let ((bfs (make-instance 'reverse-buffered-file-stream
			      :file-stream f)))
      (loop for line := (read-line bfs nil :eof)
	    do (print (nreverse line))
	    while
	    (not (stream-at-end bfs))
	    ))))
