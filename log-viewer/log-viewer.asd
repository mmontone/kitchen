(asdf:defsystem #:log-viewer
  :description "Log parser and viewer tool."
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license  "GPL"
  :version "0.0.1"
  :serial t
  :components ((:file "log-viewer")
	       (:file "buffered-file-stream")
	       (:file "source")
               (:file "parser"))
  :depends-on (:alexandria :cl-json :chronicity))
