(require :croatoan)

(defpackage :log-viewer/ncurses
  (:use :cl :log-viewer :croatoan)
  (:documentation "ncurses log-viewer UI"))

(in-package :log-viewer/ncurses)

(defparameter *log* (log-viewer::test-parse-hunchentoot-log
                     #p"/home/marian/work/bonanza-aa"))

(defmacro with-window* ((win &rest options) &body body)
  "Create a window, evaluate the forms in the body, then cleanly close the window.

Pass any arguments to the initialisation of the window object.

Example:

(with-window (win :input-echoing t
  body)"
  `(let ((,win (make-instance ',(or (getf options :window-class) 'croatoan:window)
                              ,@(alexandria:remove-from-plist options :window-class))))
     (unwind-protect
          (progn
            ,@body)
       (croatoan::close ,win))))

(defun test1 ()
  (with-screen (scr :input-echoing nil :input-blocking t :enable-colors t :bind-debugger-hook nil)
    (with-window (win :position '(0 0) :dimensions (list (height scr) (width scr)) :enable-function-keys t :enable-scrolling t)
      (setf (background win) (make-instance 'complex-char :color-pair '(:white :black)))
      (let ((y 0)
            (pad (make-instance 'pad :height (height win) :width (width win)))
            (scroll-y 0))
        (dolist (log-item *log*)
          (put-string pad y 0 (log-viewer::message log-item))
          (incf y))

        (flet ((refresh* ()
                 ;; the background screen has to be touched and refreshed on every move,
                 ;; otherwise we will see parts of the previously displayed pad still there.
		 (format t "scroll: ~a" scroll-y)
                 (touch win)
                 (refresh win)
                 (refresh pad scroll-y 0 0 0 (height pad) (width pad))))
	  
	  (refresh*)

          (event-case (win event)
            (#\q (return-from event-case))
            (#\w
             (print (cursor-position-y win))
	     (change-attributes pad (width pad) '() :y (cursor-position-y win) :x 0)
             (if (zerop (cursor-position-y win))
                 (setf scroll-y (max (1- scroll-y) 0))
                 (move-direction win :up))
	     (change-attributes pad (width pad) '(:reverse) :y (cursor-position-y win) :x 0)
             (refresh*))
            (#\s
             (print (cursor-position-y win))
	     (change-attributes pad (width pad) '() :y (cursor-position-y win) :x 0)
             (if (= (cursor-position-y win) (1- (height pad)))
                 (incf scroll-y)
                 (move-direction win :down))
	     (change-attributes pad (width pad) '(:reverse) :y (cursor-position-y win) :x 0)
             (refresh*)
             )))))))
