(defpackage :log-viewer/ltk
  (:use :cl :log-viewer :nodgui)
  (:documentation "ltk log-viewer UI"))

(in-package :log-viewer/ltk)

(require :nodgui)

(defun format-timestamp (timestamp)
  (local-time:format-timestring nil (local-time:universal-to-timestamp timestamp)
                                :format local-time:+asctime-format+))

(defparameter *log* (log-viewer::test-parse-monolog-log #p"/home/marian/work/50plusstreffen/storage/logs/laravel-2022-04-04.log"))

(defparameter *log* (log-viewer::test-parse-hunchentoot-log #p"/home/marian/work/bonanza-aa"))

(defun test1 ()
  (with-nodgui ()
    (let ((tree (make-instance 'scrolled-treeview
                               :columns (list "message" "log-level")))
          (detail (make-instance 'nodgui:text))
          (id 0))
      ;; setup headers
      (treeview-heading tree +treeview-first-column-id+
			:text "timestamp")
      (dolist (log-item *log*)
        (treeview-insert-item
         tree
         :id (princ-to-string id)
         :text (princ-to-string (log-viewer::timestamp log-item)) ;; first column
         ;; second and third columns
         :column-values (list (log-viewer::message log-item)
                              (log-viewer::log-level log-item)))
        (incf id))

      (bind (treeview tree) "<<TreeviewSelect>>"
        (lambda (event)
          (declare (ignore event))
          (let ((log-item (nth (parse-integer (id (first (treeview-get-selection (treeview tree))))) *log* )))
            (setf (text detail) (log-viewer::source log-item)))))

      (let* ((frame (make-instance 'frame))
             (log-level-combo (make-instance 'combobox
                                             :text "all"
                                             :values '("all" "info" "warning" "error" "debug")
                                             :master frame))
             (category-combo (make-instance 'combobox
                                            :text "all"
                                            :values '("all" "info" "warning" "error" "debug")
                                            :master frame))
             (search-input (make-instance 'text :master frame :height 1)))

        (bind log-level-combo "<KeyRelease>"
          (lambda (event)
            (declare (ignore event))
            (format t "newsel:~a~%" (text log-level-combo))
            (finish-output)))

        (bind log-level-combo "<<ComboboxSelected>>"
          (lambda (event)
            (declare (ignore event))
            (format t "newsel:~a~%" (text log-level-combo))
            (finish-output)))


        (pack frame :fill :x :expand t)
        (pack (make-instance 'label :master frame :text "Log level:") :side :left)
        (setf (master log-level-combo) frame)
        (pack log-level-combo :side :left :padx 5)

        (pack (make-instance 'label :master frame :text "Category:") :side :left)
        (setf (master category-combo) frame)
        (pack category-combo :side :left)

        (pack (make-instance 'label :master frame :text "Search:") :side :left)
        (pack search-input :side :left)
        )

      (pack tree :fill :x :expand t)
      (pack detail :fill :x :expand t)

      )))
