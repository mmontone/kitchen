(require 'slime)
(require 'anaphora)

(defvar slime-event-handlers (make-hash-table))

(defun slime-add-event-handler (event handler)
  "Add HANDLER  to the list of EVENT handlers."

  ;; Initialize empty
  (when (not (gethash event slime-event-handlers))
    (puthash event nil slime-event-handlers))

  ;; Add handler to the list
  (puthash event (cons handler (gethash event slime-event-handlers))  slime-event-handlers))

(defun slime-reset-event-handlers (&optional event)
  (if event
      (puthash event nil slime-event-handlers)
    (setq slime-event-handlers (make-hash-table))))

(defslimefun slime-trigger-event (event &rest args)
  (message "SLIME event triggered: %s" event)
  (let ((handlers (gethash event slime-event-handlers)))
    (dolist (handler handlers)
      (apply handler event args))))

(slime-add-event-handler
 :my-event
 (lambda (event)
   (message "Hello my event!")))

(slime-reset-event-handlers :my-event)

(slime-trigger-event :my-event)

(slime-add-event-handler
 :one-arg
 (lambda (event arg)
   (message "One arg event: %s" arg)))

(slime-trigger-event :one-arg 333)

(provide 'slime-events)
