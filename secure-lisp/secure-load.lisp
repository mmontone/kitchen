(in-package :secure-cl)

;; From SBCL source code:

(cl:shadow 'load)

(cl:defun load-file
    (pathspec &key (verbose *load-verbose*) (print *load-print*)
                (if-does-not-exist t) (external-format :default))
  "Load the file given by FILESPEC into the Lisp environment, returning
   T on success."
  (flet ((load-stream (stream faslp)
           (when (and (sb-fasl::fd-stream-p stream)
                      (eq (sb-impl::fd-stream-fd-type stream) :directory))
             (error 'simple-file-error
                    :pathname (pathname stream)
                    :format-control
                    "Can't LOAD a directory: ~s."
                    :format-arguments (list (pathname stream))))
           (flet ((thunk ()
                            (let (;; Bindings required by ANSI.
                                  (*readtable* *readtable*)
                                  (*package* (sb-fasl::sane-package))
                                  ;; KLUDGE: I can't find in the ANSI spec where it says
                                  ;; that DECLAIM/PROCLAIM of optimization policy should
                                  ;; have file scope. CMU CL did this, and it seems
                                  ;; reasonable, but it might not be right; after all,
                                  ;; things like (PROCLAIM '(TYPE ..)) don't have file
                                  ;; scope, and I can't find anything under PROCLAIM or
                                  ;; COMPILE-FILE or LOAD or OPTIMIZE which justifies this
                                  ;; behavior. Hmm. -- WHN 2001-04-06
                                  (sb-c::*policy* sb-c::*policy*)
                                  (sb-c::*handled-conditions* sb-c::*handled-conditions*))
                              (if faslp
                                  (sb-fasl::load-as-fasl stream verbose print)
                                  (sb-c:with-compiler-error-resignalling
                                    (load-as-source stream :verbose verbose
                                                           :print print))))))
	     (DECLARE (TRULY-DYNAMIC-EXTENT (FUNCTION THUNK)))
             (sb-fasl::call-with-load-bindings #'thunk stream))))

    ;; Case 1: stream.
    (when (streamp pathspec)
      (return-from load (load-stream pathspec (sb-fasl::fasl-header-p pathspec))))

    (let ((pathname (pathname pathspec)))
      ;; Case 2: Open as binary, try to process as a fasl.
      (with-open-stream
          (stream (or (cl:open pathspec :element-type '(unsigned-byte 8)
					:if-does-not-exist nil)
                      (when (null (pathname-type pathspec))
                        (let ((defaulted-pathname
                                (sb-fasl::probe-load-defaults pathspec)))
                          (if defaulted-pathname
                              (progn (setq pathname defaulted-pathname)
                                     (open pathname
                                           :if-does-not-exist
                                           (if if-does-not-exist :error nil)
                                           :element-type '(unsigned-byte 8))))))
                      (if if-does-not-exist
                          (error 'simple-file-error
                                 :pathname pathspec
                                 :format-control
                                 "~@<Couldn't load ~S: file does not exist.~@:>"
                                 :format-arguments (list pathspec))
                          (return-from load nil))))
        (let* ((real (probe-file stream))
               (should-be-fasl-p
                 (and real (string-equal (pathname-type real) sb-fasl::*fasl-file-type*))))
          ;; Don't allow empty .fasls, and assume other empty files
          ;; are source files.
          (cond ((or (and should-be-fasl-p (eql 0 (file-length stream)))
                     (sb-fasl::fasl-header-p stream :errorp should-be-fasl-p))
                 (load-stream stream t))
                (t
                 ;; Case 3: Open using the given external format, process as source.
                 (with-open-file (stream pathname :external-format external-format
                                                  :class 'form-tracking-stream)
                   (load-stream stream nil)))))))))

;;;; LOAD-AS-SOURCE

;;; something not EQ to anything we might legitimately READ
(defvar *eof-object* (make-symbol "EOF-OBJECT"))

;;; Load a text stream.  (Note that load-as-fasl is in another file.)
;; We'd like, when entering the debugger as a result of an EVAL error,
;; that the condition be annotated with the stream position.
;; One way to do it is catch all conditions and encapsulate them in
;; something new such as a LOADER-EVAL-ERROR and re-signal.
;; The printer for the encapsulated condition has the data it needs to
;; show the original condition and the line/col. That would unfortunately
;; interfere with handlers that were bound around LOAD, since they would
;; only receive the encapsulated condition, and not be able to test for
;; things they're interested in, such as which redefinition warnings to ignore.
;; Instead, printing a herald for any SERIOUS-CONDITION approximates
;; the desired behavior closely enough without printing something for warnings.
;; TODO: It would be supremely cool if, for toplevel PROGN, we could
;; indicate the position of the specific subform that failed
(cl:defun load-as-source (stream &key verbose print (context "loading"))
  (sb-fasl::maybe-announce-load stream verbose)
  (let* ((pathname (ignore-errors (sb-fasl::translate-logical-pathname stream)))
         (native (when pathname (sb-fasl::native-namestring pathname))))
    (with-simple-restart (abort "Abort ~A file ~S." context native)
     (labels ((condition-herald (c)
                (declare (ignore c)) ; propagates up
                (when (sb-fasl::form-tracking-stream-p stream)
                  (let* ((startpos
                          (sb-fasl::form-tracking-stream-form-start-char-pos stream))
                         (point (sb-fasl::line/col-from-charpos stream startpos)))
                    (format *error-output* "~&While evaluating the form ~
 starting at line ~D, column ~D~%  of ~S:"
                            (car point) (cdr point)
                            (or pathname stream)))))
              (eval-form (form index)
               (with-simple-restart (continue "Ignore error and continue ~A file ~S."
                                              context native)
                 (loop
                  (cl:handler-bind ((serious-condition #'condition-herald))
                   (cl:with-simple-restart (retry "Retry EVAL of current toplevel form.")
                     (if print
                         (let ((results (multiple-value-list (sb-fasl::eval-tlf form index))))
                           (sb-fasl::load-fresh-line)
                           (format t "~{~S~^, ~}~%" results))
                         (sb-fasl::eval-tlf form index)))
                    (return))))))
       (let ((sb-c::*current-path* nil)
             (sb-impl::*eval-source-info* nil)
             (sb-impl::*eval-tlf-index* nil)
             (sb-impl::*eval-source-context* nil))
         (locally (declare (optimize (sb-c::type-check 0)))
           (setf sb-c::*current-path* (sb-fasl::make-unbound-marker)))
         (if pathname
             (let* ((info (sb-c::make-file-source-info
                           pathname (stream-external-format stream)))
                    (sb-c::*source-info* info))
               (locally (declare (optimize (sb-c::type-check 0)))
                 (setf sb-c::*current-path* (sb-fasl::make-unbound-marker)))
               (setf (sb-c::source-info-stream info) stream)
               (sb-c::do-forms-from-info ((form current-index) info
                                          'sb-c::input-error-in-load)
                 (sb-c::with-source-paths
                   (sb-c::find-source-paths form current-index)
                   (eval-form form current-index))))
             (let ((sb-c::*source-info* nil))
               (loop for form =
		     ;; WE USE OUR SECURE READER HERE
                     (handler-case (read stream nil *eof-object*)
                       ((or reader-error end-of-file) (c)
                         (error 'sb-c::input-error-in-load :stream stream
                                                           :condition c)))
                     until (eq form *eof-object*)
                     do (sb-c::with-source-paths
                          (eval-form form nil)))))))))
  t)

