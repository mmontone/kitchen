(defpackage :secure-cl-repl
  (:use :cl))

(in-package :secure-cl-repl)

(defun repl ()
  "Start a secure REPL."
  (loop while t do
    (with-simple-restart (return-to-repl "Return to REPL")
      (format t "~a> " (package-name secure-cl::*package*))
      (finish-output *standard-output*)
      (print (eval (secure-cl:read)))
      (finish-output *standard-output*)
      (terpri))))

(defun secure-toplevel ()
  (setf *package* (find-package :secure-cl-user))
  (setf secure-cl::*package* (find-package :secure-cl-user))
  (repl))

(sb-ext:save-lisp-and-die  "secure-lisp"
			   :toplevel #'secure-toplevel
			   :executable t)
