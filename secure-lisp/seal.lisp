(defpackage seal
  (:use :cl))

(in-package :seal)

;; Below is an implementation of a general authentication facility (essentially the
;; same as in [44]). Each call to new-seal returns a new triple of objects (seal, unseal,
;; sealed?). Each such triple represents a new abstract data type. seal encloses an
;; object in a capsule that can be authenticated; unseal is an extraction operator that
;; reveals the encapsulated object iff the capsule is authentic (i.e. was made by this
;; seal); and sealed? is a predicate that returns true iff the capsule is authentic.
;; It is assumed that assq (association list lookup) can determine cell identity. Cells
;; (other than the one holding the association list) are used only for their identities, not
;; for holding references to objects.

(defun make-seal ()
  (let ((instances nil))
    (let ((seal
	    (lambda (rep)
	      (let ((abs (gensym)))
		(setf instances
		      (cons (cons abs rep)
			    instances))
		abs)))
	  (unseal
	    (lambda (abs)
	      (let ((probe
		      (assoc abs instances)))
		(if probe
		    (cdr probe)
		    (error "invalid argument ~s" abs)))))
	  (sealed?
	    (lambda (x)
	      (and (assoc x instances) t))))
      (list :seal seal :unseal unseal :sealed? sealed?))))
