(cl:defpackage :os/files
  (:use :secure-cl)
  (:shadow :open)
  (:export :open))

(in-package :os/files)

(define-condition permission-requirement (error)
  ())

(define-condition file-permission-requirement (permission-requirement)
  ((file :initarg :file)
   (direction :initarg :direction)))

(defclass permission ()
  ((subject :initarg :subject
	    :accessor permission-subject)
   (access :initarg :access
	   :accessor permission-access
	   :type (member :read :write :read-write))))

(defun get-file-permission (file direction)
  ;; Look for permission in current environment
  ;; If not found, signal an exception.
  ;; If it is accepted by user, then permission is created
  )

(defun check-file-permission (filename direction permission)
  (not (null permission)))

(defun open (filename &key (direction :input) (element-type 'base-char)
                        if-exists if-does-not-exist (external-format :default)
                        (class 'sb-sys:fd-stream))
  (restart-case
      (error 'file-permission-requirement
              :file filename
              :direction direction)
    (fullfill-permission-requirement (permission)
      (when (check-file-permission filename direction permission)
        (cl:open filename :direction direction
                          :element-type element-type
                          :if-exists if-exists
                          :if-does-not-exist if-does-not-exist
                          :external-format external-format
                          :class class)))))

(os/files:open #p"/home/marian/src/test.lisp")

(defmacro with-permission (p &body body)
  `(cl:handler-bind ((permission-requirement
		       (lambda (c)
			 (declare (ignore c))
			 (invoke-restart 'fullfill-permission-requirement
					 ,p))))
     ,@body))

(with-permission t
  (os/files:open #p"/home/marian/src/test.lisp"))

(with-permission nil
  (os/files:open #p"/home/marian/src/test.lisp"))
