;; Idea for permissions
;; Signal a PERMISSION-REQUIRED condition, that can only be restarted using a
;; an object that satisfies the permission.

(cl:defpackage :secure-lisp
  (:use :cl)
  (:shadow
   #:peek-char
   #:read
   #:read-preserving-whitespace
   #:read-from-string
   #:read-delimited-list
   #:*package*
   #:load
   #:compile
   #:compile-file
   #:eval
   #:in-package
   #:defpackage
   #:defmethod
   #:slot-value
   #:handler-bind
   #:inspect
   #:symbol-function))

(cl:defpackage :secure-common-lisp
  (:nicknames :secure-cl)
  (:use :cl)
  (:shadow
   #:peek-char
   #:read
   #:read-preserving-whitespace
   #:read-from-string
   #:read-delimited-list
   #:*package*
   #:load   
   #:compile
   #:compile-file
   #:eval
   #:in-package
   #:defpackage
   #:defmethod
   #:trace ;; disallowed on non defined in own env
   #:slot-value
   #:handler-bind
   #:inspect
   #:fdefinition
   #:find-package
   #:symbol-function)
  #.(list* :export
           :current-package
	   :load-file
           (let (external-symbols
                 (dont-export '(list "*PACKAGE*")))
             (do-external-symbols (symbol :cl)
               (when (not (member symbol dont-export :test 'string=))
                 (push symbol external-symbols)))
             external-symbols)))

(cl:in-package :secure-cl)

(defvar *package* (cl:find-package :secure-lisp))

(defun external-symbol-p (symbol &optional (package (symbol-package symbol)))
  (let ((symbol-name (cond
                       ((symbolp symbol) (symbol-name symbol))
                       ((stringp symbol) symbol)
                       (t (error "Invalid symbol: ~s" symbol)))))
    (do-external-symbols (external-symbol package)
      (when (string= symbol-name (symbol-name external-symbol))
        (return-from external-symbol-p t))
      nil)))

(cl:defmethod eclector.reader:interpret-symbol
    ((client (eql :secure-lisp))
     input-stream
     package-indicator
     symbol-name
     internp)
  (prog (package symbol)
   package
     (setf package (case package-indicator
                     (:current *package*)
                     (:keyword (find-package "KEYWORD"))
                     (t        (or (find-package package-indicator)
                                   (multiple-value-bind (value kind)
                                       (eclector.reader:package-does-not-exist
                                        input-stream package-indicator symbol-name)
                                     (ecase kind
                                       (symbol
                                        (return value))
                                       (package
                                        value)
                                       (:package-name
                                        (setf package-indicator value)
                                        (go package))))))))
   symbol
     (setf symbol (if internp
                      (progn
                        (when (not (member package-indicator '(:current :keyword)))
                          (when (not (external-symbol-p symbol-name package))
                            (error "Cannot access not external symbol")))
                        (intern symbol-name package))
                      (multiple-value-bind (symbol status)
                          (find-symbol symbol-name package)
                        (cond ((null status)
                               (eclector.reader:symbol-does-not-exist
                                input-stream package symbol-name))
                              ((eq status :internal)
                               (error "Cannot access not external symbol")
                               (eclector.reader:symbol-is-not-external
                                input-stream package symbol-name symbol))
                              (t
                               symbol)))))
   done
     (return symbol)))

(defun read (&optional (input-stream cl:*standard-input*) (eof-error-p t) eof-value recursive-p)
  "Read the next Lisp value from STREAM, and return it."
  (let ((eclector.base:*client* :secure-lisp)
        (eclector.reader::*read-eval* nil)) ;; disallow evaluation at read time
    (eclector.reader:read input-stream eof-error-p eof-value recursive-p)))

(defun read-from-string (string &optional (eof-error-p t)
                                  (eof-value nil)
                         &key (start 0)
                           (end nil)
                           (preserve-whitespace nil))
  (let ((eclector.base:*client* :secure-lisp)
        (eclector.reader::*read-eval* nil) ;; disallow evaluation at read time
        )
    (eclector.reader:read-from-string string eof-error-p eof-value
                                      :start start
                                      :end end
                                      :preserve-whitespace preserve-whitespace)))

;; (read-from-string "cl:defun")

;; ;; Success:
;; (read-from-string "alexandria::flatten")

;; ;; Fail:
;; (read-from-string "alexandria::defun")

;; ;; contrary to:
;; (cl:read-from-string "alexandria::defun")

(defun load (pathspec &key (verbose *load-verbose*) (print *load-print*) (if-does-not-exist t) (external-format :default))
  )

(defun compile (name &optional (definition (or (and (symbolp name) (macro-function name)) (fdefinition name))))
  )

(defclass secure-env-client (clostrum/simple:simple-client)
  ())

(defvar *clostrum-client* (make-instance 'secure-env-client))

(defclass secure-run-time-environment (clostrum/simple:simple-run-time-environment)
  ((name :initarg :name
	 :accessor env-name)
   (permissions :documentation "List of permissions granted to this environment.")
   (defined-packages
       :initform nil
       :accessor defined-packages
       :documentation "Packages defined in this environment.")))

(defclass secure-compilation-environment (clostrum/simple:simple-compilation-environment)
  ((name :initarg :name
	 :accessor env-name)
   (permissions :documentation "List of permissions granted to this environment.")
   (defined-packages
       :initform nil
       :accessor defined-packages
       :documentation "Packages defined in this environment.")))

(defvar *compilation-env* (make-instance 'secure-compilation-environment
                                         :parent nil))

(defvar *run-time-env* (make-instance 'secure-run-time-environment))

(defvar *env* *run-time-env*)

(defun in-package (string-designator)
  "Switch to package designed by STRING-DESIGNATOR.
A package switch in secure lisp is only allowed if the package was defined in the current environment."
  (if (find string-designator (defined-packages *env*))
      (setf *package* (find string-designator (defined-packages *env*)))
      (error "Package ~s was not defined in this environment" string-designator)))

(defun find-defined-package (package-designator)
  "Find package defined in the current environment."
  (find package-designator (defined-packages *env*)))

(defmacro with-package (package-designator &body body)
  `(let ((*package* (find-defined-package package-designator)))
     ,@body))

(defun eval (original-exp)
  (cl:eval original-exp))

;; DEFMETHOD
;; DEFMETHOD on generic functions not defined in current environment
;; can not be allowed because method combinations allow advicing external methods
;; and it is possible to do malicious things with that. Like:

;; (defmethod external-package:some-method :before (&rest args)
;;   (setf *captured-args-from-other-programs* args))

;; TODO: look at MOP protocol for adding methods and tune that instead ...
;; CLOSER-MOP:ADD-DIRECT-METHOD should not be allowed to be called ..
(defmacro defmethod (name &rest args)
  "Secure version of CL:DEFMETHOD."
  (when (not (find (symbol-package name) (defined-packages *env*)))
    (error "Cannot define method on foreign packages"))
  (error "TODO"))

(cl:defmethod (setf clostrum:fdefinition)
    (function (client secure-env-client)
     (env secure-compilation-environment) symbol)
  (when (not (find (symbol-package symbol) (defined-packages env)))
    (error "Cannot set function definitions outside this environment"))
  (funcall #'(setf clostrum:fdefinition) function
           client (clostrum:parent env) symbol))

(cl:defun slot-value (object slot-name)
  (when (and (not (external-symbol-p slot-name))
             (not (find (symbol-package slot-name) (defined-packages *env*))))
    (error "Access to slot value not allowed"))
  (cl:slot-value object slot-name))

;; A secure handler-bind should not be able to catch and handle permission errors
(cl:defmacro handler-bind (bindings &body forms)
  (error "TODO"))

(setf *debugger-hook* nil) ;; debugger should be set to a secure debugger

(shadow 'cl:inspect)
;; inspector should be set to a secure inspector too
(defun inspect (object)
  (error "TODO"))

(shadow 'cl:fdefinition)
(defun fdefinition (name)
  (clostrum:fdefinition *clostrum-client* *run-time-env* name))

(shadow '(setf cl:fdefinition))
(cl:defmethod (setf fdefinition) (value name)
  (when (external-symbol-p name)
    (error "Not allowed"))
  (setf (clostrum:fdefinition *clostrum-client* *run-time-env* name) value))

(defun symbol-function (symbol)
  (cl:symbol-function symbol))

(shadow '(setf cl:symbol-function))

;; TODO: not working:

(cl:defmethod (setf symbol-function) (value symbol)
   (when (external-symbol-p symbol)
     (error "Not allowed"))
   (setf (cl:symbol-function symbol) value))

(shadow 'find-package)

(defun find-package (package-designator)
  ;; When asked for COMMON-LISP package, return SECURE-COMMON-LISP package instead
  ;; (let ((package (cond
  ;;               ((stringp package-designator)
  ;;                (clostrum:find-package *clostrum-client* *run-time-env* package-designator))
  ;;               ((symbolp package-designator)
  ;;                (clostrum:find-package *clostrum-client* *run-time-env* (symbol-name package-designator)))
  ;;               ((cl:packagep package-designator)
  ;;                package-designator)
  ;;               (t (error "Invalid package designator: ~s" package-designator)))))
  (let ((package (cl:find-package package-designator)))
    (if (string= (package-name package) "COMMON-LISP")
        (find-package "SECURE-COMMON-LISP")
        package)))

(defun current-package ()
  *package*)

(cl:defpackage :secure-common-lisp-user
  (:nicknames :secure-cl-user)
  (:use :secure-common-lisp)
  (:export #:current-package
           #:in-package))
