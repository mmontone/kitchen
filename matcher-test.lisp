(defun matcher-fib (x)
  (match x
    (0 0)
    (1 1)
    (n (+ (matcher-fib (- n 1))
          (matcher-fib (- n 2))))))

(defun trivia-fib (x)
  (trivia:match x
    (0 0)
    (1 1)
    (n (+ (matcher-fib (- n 1))
          (matcher-fib (- n 2))))))

(when-match (list 1 2 3) (list _ x _)
  x)

(when-match (list 1 2 3) #'listp
  33)
