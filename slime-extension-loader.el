(require 'slime)

(defslimefun slime-load-extension (file)
  (load-file file))

(provide 'slime-extension-loader)
