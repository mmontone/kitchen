(defpackage :abcl-syntax
  (:use :cl)
  (:export #:with-java-syntax))

(in-package :abcl-syntax)

(defun lisp-to-camel-case (string)
  "Take a string with Lisp-style hyphentation and convert it to camel
case.  This is an inverse of CAMEL-CASE-TO-LISP."
  (loop with i = 0 and l = (length string)
     with cc-string = (make-string l) and cc-i = 0
     with init = t and cap = nil and all-caps = nil
     while (< i l)
     do (let ((c (aref string i)))
          (unless (case c
                    (#\* (if init (setq cap t)))
                    (#\+ (cond
                           (all-caps (setq all-caps nil init t))
                           (init (setq all-caps t))))
                    (#\- (progn
                           (setq init t)
                           (cond
                             ((or all-caps
                                  (and (< (1+ i) l)
                                       (char= (aref string (1+ i)) #\-)
                                       (incf i)))
                              (setf (aref cc-string cc-i) #\_)
                              (incf cc-i))
                             (t (setq cap t))))))
            (setf (aref cc-string cc-i)
                  (if (and (or cap all-caps) (alpha-char-p c))
                      (char-upcase c)
                      (char-downcase c)))
            (incf cc-i)
            (setq cap nil init nil))
          (incf i))
     finally (return (subseq cc-string 0 cc-i))))

(defun jprop (member obj))
(defun jcall (method obj &rest args))

(defun split-dot-sym (sym)
  (loop for piece in (cl-ppcre:split "\\." (string sym))
    collect (intern piece (or (symbol-package sym) *package*))))

(defun translate-dot-sym (sym)
  (cond
    ((eq (search ".-" (symbol-name sym)) 0)
     ;; property access
     (list 'jprop (lisp-to-camel-case (subseq (symbol-name sym) 2))))
    ((eq (search "-" (symbol-name sym)) 0)
     ;; property access
     (list 'jprop (lisp-to-camel-case (subseq (symbol-name sym) 1))))
    ((char= (aref (symbol-name sym) 0) #\.)
     ;; method access
     (list 'jcall (lisp-to-camel-case (subseq (symbol-name sym) 1))))
    ((position #\. (symbol-name sym))
     ;; dot in the middle
     (let* ((pieces (split-dot-sym sym))
	    (fns (loop for sym in (rest pieces)
		       collect `(quote ,sym))))
       `(access ,(first pieces) ,@fns)))
    (t ;; no dot
     sym
     )))

(translate-dot-sym '.foo)
(translate-dot-sym '.-foo)
(translate-dot-sym '-foo)
(translate-dot-sym 'obj.foo)
(translate-dot-sym '.to-string)

(defun splice (thing list)
  (if (listp thing)
      (append thing list)	    
      (cons thing list)))

(defun dot-translate-walker (form)
  (typecase form
    (cons
     (let ((first (dot-translate-walker (car form)))
	   (rest (mapcar 'dot-translate-walker (cdr form))))
       (if (and (listp first) (member (car first) '(jcall jprop access)))
	   (splice first rest)
	   (cons first rest))))
     (symbol (translate-dot-sym form))
     (atom form)))

(dot-translate-walker '(.method1 obj))
(dot-translate-walker '(-prop1 obj))
(dot-translate-walker '(.method1 (-prop1 obj)))

(defun name-has-dot? (n)
  (cl-ppcre:all-matches "\\." (string n)))

(defun replace-dot-calls (forms)
  (dot-translate-walker forms))

(replace-dot-calls '(progn (.method1 obj)
		     (-to-string obj)))
(replace-dot-calls '(.method1 obj))


(defmacro with-java-syntax (() &body body)
  (replace-dot-calls `(progn ,@body)))

(with-java-syntax ()
  (.method1 obj x t z)
  (-to-string lala))

;; Use with arrows?
;; We need a proper walker if we want this ...
(with-java-syntax ()
  (arrows:-> obj
    (.doSomething "x" (format nil "lal"))
    .toString))

;; Solution:? Custom arrow ...
(defmacro -> (obj &body body)
  (let ((progn (replace-dot-calls `(progn ,@body))))
    `(arrows:-> ,obj ,@(rest progn))))

(-> obj
  (.do-something "x" (format nil "lala"))
  .to-string)

(with-java-syntax ()
  (let ((x 22))
    (.print obj x)
    (format nil "~a" (.as-string x))))

(defpackage :jcl
  (:use :cl)
  (:shadow :defun :defmethod)
  (:export #:defun
	   #:defmethod))

(in-package :jcl)

(cl:defmacro defun (name args &body body)
  `(cl:defun ,name ,args
     (abcl-syntax:with-java-syntax ()
       ,@body)))

(cl:defmacro defmethod (name args &body body)
  `(cl:defmethod ,name ,args
     (abcl-syntax:with-java-syntax ()
       ,@body)))

(defpackage java-syntax-test
  (:use :cl))

(in-package :java-syntax-test)

(jcl:defun my-java-func (obj)
  (.print obj))

(jcl:defmethod print-object ((obj my-obj) stream)
  (print-unreadable-object (obj stream :type t :identity t)
    (format stream "~a" (.name obj))))
