(require :swank)

(defpackage :slime-actions
  (:use :cl)
  (:export :def-slime-action))

(in-package :slime-actions)

(defvar *slime-actions* (make-hash-table)
  "SLIME actions.")

(defclass slime-action ()
  ((name :initarg :name
         :type symbol
         :accessor action-name)
   (description :initarg :description
                :type string
                :accessor action-description)
   (label :initarg :label
          :type (or null string)
          :accessor action-label
	  :initform nil)
   (group :initarg :group
          :type symbol
          :accessor action-group)
   (args :initarg :args
	 :initform nil
	 :accessor action-args)
   (action :initarg :action
           :type (or function symbol)
           :accessor action)))

(defclass slime-var ()
  ((name :initarg :name
	 :accessor var-name)
   (description :initarg :description
		:accessor var-description)
   (type :initarg :type
	 :accessor var-type
	 :type symbol)
   (group :initarg :group
	  :accessor var-group)))

(defun quote-plist-values (plist)
  (loop for key in plist by #'cddr
	for value in (rest plist) by #'cddr
	collect key
	collect `(quote ,value)))

(defmacro def-slime-action (name-and-options args &body body)
  (let* ((name (if (listp name-and-options)
		  (first name-and-options)
		  name-and-options))
	 (options (when (listp name-and-options)
		    (rest name-and-options)))
	 (opts (quote-plist-values options))
	 (description (when (stringp (first body))
			(first body))))
    `(progn
       
     ;; Create Lisp side function
     (defun ,name ,(mapcar 'first args)
       ,@body)

     ;; Add function to the list of actions in *SLIME-ACTIONS*
     (let ((action (make-instance 'slime-action :name ',name
                                                :action ',name
						:description ,description
                                                ,@opts)))
       (setf (gethash ',name *slime-actions*) action))
     
     ;; Notify Emacs side
     (when swank::*emacs-connection*
       (swank:ed-rpc-no-wait 'slime-trigger-event
			     :def-slime-action
			     ',name '(:description ,description
				      ,@options))))))

(defmacro def-slime-var (name type group)
  )

(def-slime-action (asdf-test-system :group asdf)
    ((system-name (slime-read-system-name)))
     "Test an ASDF system"
  (asdf:test-system system-name))

(def-slime-action (asdf-load-system :group asdf)
    ((system-name (slime-read-system-name)))
  "Load an ASDF system"
  (asdf:load-system system-name))

(def-slime-action (quickload :group quicklisp)
    ((system-name (slime-read-system-name)))
  "Load system via Quicklisp"
  (quicklisp:quickload system-name))


(defun group-slime-actions ()
  (let ((groups (make-hash-table)))
    (loop for action being the hash-values of *slime-actions*
	  do
	     (when (not (gethash (action-group action) groups))
	       (setf (gethash (action-group action) groups) nil))
	     (push action (gethash (action-group action) groups)))
    groups))

(defun emacs-get-grouped-slime-actions ()
  (flet ((serialize-action (action)
	   (list (cons :name (action-name action))
		 (cons :group (action-group action))
		 (cons :description (action-description action))
		 (cons :args (action-args action))
		 (cons :action (action action))
		 (cons :label (action-label action)))))
  (let ((grouped-actions (group-slime-actions))
	(serialized))
    (maphash (lambda (group actions)
	       (push (cons group (mapcar #'serialize-action actions))
		     serialized))
	     grouped-actions)
    serialized)))
	       

(get-grouped-actions)

(emacs-get-grouped-slime-actions)
