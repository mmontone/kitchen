;; Idea to try. An on-demand workers threads pool.
;; New worker threads are created depending on demanded processing.
;; For IO tasks.

;; If workerCount < corePoolSize, Then create and start a thread to execute the newly submitted task .
;; If workerCount >= corePoolSize, And the blocking queue in the thread pool is not full , Then add the task to the blocking queue .
;; If workerCount >= corePoolSize && workerCount < maximumPoolSize, And the blocking queue in the thread pool is full , Then create and start a thread to execute the newly submitted task .
;; If workerCount >= maximumPoolSize, And the blocking queue in the thread pool is full , The task is handled according to the rejection strategy , The default way is to throw exceptions directly .

;; A thread should be killed if idle for N seconds.
;; Use timeout and kill himself with sb-concurrency:receive-message

(require :sb-concurrency)

(defpackage :tgmr-dyn-pool
  (:use :cl))

(in-package :tgmr-dyn-pool)

(defclass dyn-pool ()
  ((queue :accessor queue
          :initform (sb-concurrency:make-mailbox)
          :documentation "Queue with requested connections.")
   (min-pool-size
    :initarg :min-pool-size
    :initform 5
    :accessor min-pool-size
    :documentation "Minimum number of threads in the pool.")
   (max-pool-size
    :initarg :max-pool-size
    :initform 50
    :accessor max-pool-size
    :documentation "Maximum number of threads in the pool.")
   (worker-idle-maxtime :initarg :worker-idle-maxtime
                        :accessor worker-idle-maxtime
                        :initform 5
                        :documentation "Seconds a worker thread waits for tasks to perform.")
   (running-threads :initform (make-hash-table :synchronized t :test 'equalp)
                    :accessor running-threads)
   (max-queue-size
    :initarg :max-queue-size
    :accessor max-queue-size
    :initform 10000
    :documentation "Maximum number of connections allowed in the queue.")
   (contention-threshold
    :initarg :contention-threshold
    :accessor contention-threshold
    :initform 30
    :documentation "Number of connections that can wait before creating a new worker thread.")))

(defun worker (pool name)
  (with-slots (queue worker-idle-maxtime running-threads) pool
    ;; wait for a task to perform, for a maximum of WORKER-IDLE-MAXTIME seconds
    (loop for msg := (sb-concurrency:receive-message
                      queue
                      :timeout worker-idle-maxtime)
          while msg
          do (funcall msg))
    ;; timeout. exit, and remove the worker from the RUNNING-THREADS collection.
    (remhash name running-threads)))

(defun process-connection (taskmaster socket)
  (with-slots (pool) taskmaster
    (with-slots (queue running-threads min-pool-size
                 max-pool-size max-queue-size contention-threshold) pool

      (let ((running-threads-count (hash-table-count running-threads))
            (queued-connections-count (sb-concurrency:mailbox-count queue)))

	;; Check if connection limits have been surpased
        (when (and (>= running-threads-count max-pool-size)
                   (>= queued-connections-count max-queue-size ))
          ;; Deny the connection
          (tbnl::too-many-taskmaster-requests taskmaster socket)
          (tbnl::send-service-unavailable-reply taskmaster socket)
          (return-from process-connection))

        ;; Enqueue connection so that it gets handled by some worker thread.
        (sb-concurrency:send-message
         queue
         (lambda ()
           (tbnl:process-connection (tbnl:taskmaster-acceptor taskmaster) socket)))
        ;; Start worker if needed
        (when (or (< running-threads-count min-pool-size)
                  (and (< running-threads-count max-pool-size)
                       (>= queued-connections-count contention-threshold)))
          (let ((worker-name (princ-to-string (gensym "worker-"))))
            (setf (gethash worker-name running-threads)
                  (bt:make-thread (lambda ()
                                    (worker pool worker-name))
                                  :name worker-name))))))))

(defclass dyn-pool-tmgr (tbnl:multi-threaded-taskmaster)
  ((pool :initarg :pool
         :initform (make-instance 'dyn-pool)
         :accessor pool)))

(defmethod tbnl:handle-incoming-connection ((taskmaster dyn-pool-tmgr) socket)
  (process-connection taskmaster socket))

(defmethod tbnl::too-many-taskmaster-requests ((taskmaster dyn-pool-tmgr) connection)
  (declare (ignore connection))
  (tbnl::acceptor-log-message (tbnl:taskmaster-acceptor taskmaster)
                              :warning "Can't handle a new request, too many request threads already"))

;; This is required by tbnl:multi-threaded-taskmaster api. Not needed, I think ...

(defmethod tbnl::decrement-taskmaster-accept-count ((taskmaster dyn-pool-tmgr))
  ;;(tbnl:ensure-dispatcher-process taskmaster)
  ;;(decf (tbnl::taskmaster-accept-count taskmaster))
  )

(defmethod tbnl::increment-taskmaster-accept-count ((taskmaster dyn-pool-tmgr))
  ;;(ensure-dispatcher-process taskmaster)
  ;;(incf (taskmaster-accept-count taskmaster))
  )

(defmethod tbnl::decrement-taskmaster-thread-count ((taskmaster dyn-pool-tmgr))
  ;;(ensure-dispatcher-process taskmaster)
  ;;(decf (taskmaster-thread-count taskmaster))
  )

(defmethod tbnl::increment-taskmaster-thread-count ((taskmaster dyn-pool-tmgr))
  ;;(ensure-dispatcher-process taskmaster)
  ;;(incf (taskmaster-thread-count taskmaster))
  )

(defvar *acceptor*)

(defun start-server (&rest args)
  (setf *acceptor*
        (tbnl:start
         (apply #'make-instance 'tbnl:easy-acceptor
                :access-log-destination nil
                :taskmaster (make-instance 'dyn-pool-tmgr)
                args))))

(defun stop-server ()
  (tbnl:stop *acceptor*))

(tbnl:define-easy-handler (say-yo :uri "/yo") (name)
  (setf (hunchentoot:content-type*) "text/plain")
  (format nil "Hey~@[ ~A~]!" name))

;; You can use wrk tool for testing
;; wrk -t4 -c100 -d15 "http://localhost:4242/yo"
