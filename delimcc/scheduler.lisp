(require :bordeaux-threads)
(require :sb-concurrency)
(require :sb-queue)
(require :blackbird)

(defpackage :scheduler
  (:use :cl))

(in-package :scheduler)

(defvar *queue* nil)

(defun test-queue ()
  (loop repeat 100
      do
	 (bt:make-thread
	  (lambda ()
	    (loop repeat 10
		  do (push (random 100) *queue*))))))

(defvar *events* (sb-queue:make-queue))

(defstruct event
  condition
  function)

(defun schedule-event (condition function)
  (bb:create-promise
   (lambda (resolve reject)
     (declare (ignore reject))
     (sb-queue:enqueue (make-event :condition condition
				   :function (lambda ()
					       (funcall resolve (funcall function))))
		       *events*))))

(defun wait-condition (condition)
  (bb:create-promise
   (lambda (resolve reject)
     (declare (ignore reject))
     (sb-queue:enqueue (make-event :condition condition
				   :function resolve)
		       *events*))))

(defun scheduler ()
  (loop
    (loop for done := 0 then 0 
	  for event := (sb-queue:dequeue *events*)
	    then (sb-queue:dequeue *events*)
	  while event
	  do
	     (if (funcall (event-condition event))
		 (progn
		   (funcall (event-function event))
		   (incf done))
		 ;; else, push the event back
		 (sb-queue:enqueue event *events*))
	     (when (zerop done)
	       (sleep 0.02)))
    (sleep 0.02)))

(defvar *scheduler*)

(defun start-scheduler ()
  (setf *scheduler* (bt:make-thread 'scheduler :name "scheduler")))

(defun stop-scheduler ()
  (bt:destroy-thread *scheduler*))

;; (defun wait (seconds next)
;;   (let ((timeout (+ (get-universal-time) seconds)))
;;     (schedule-event (lambda ()
;; 		      (>= (get-universal-time) timeout))
;; 		    next)))

(defun wait (seconds)
  (let ((timeout (+ (get-universal-time) seconds)))
    (wait-condition (lambda ()
		      (>= (get-universal-time) timeout)))))

(defun test1 ()
  (loop repeat 100
	do
	   (bb:attach (wait (random 20))
		      (lambda ()
			(print "test1")))))

(defun test2 ()
  (loop repeat 100
	do
	   (bb:attach (wait-condition (constantly t))
		      (lambda ()
			(print "test2")))))

(defmacro await ((var call) &body body)
  `(bb:alet ((,var (the (values bb:promise &optional) ,call)))
     ,@body))

(defun heavy-task (&optional name)
  (let ((name (or name (princ-to-string (gensym "task-")))))
    (format t "doing task ~a~%" name)
    (bb:attach (wait (random 20))
	       (lambda ()
		 (format t "task ~a done~%" name)
		 (random 100) ;; the result
		 ))))

(defun async-test ()
  (print "starting~%")
  (await (result (heavy-task))
    (format t "task result: ~a~%" result)))

(defun async-test-2 (times)
  (loop repeat times
	do
	(await (result (heavy-task))
	  (format t "task result: ~a~%" result))))

(defun light-task (&optional name)
  (let ((name (or name (princ-to-string (gensym "task-")))))
    (format t "doing task ~a~%" name)
    (bb:attach (wait-condition (constantly t))
	       (lambda ()
		 (format t "task ~a done~%" name)
		 (random 100) ;; the result
		 ))))

(defun async-test-3 (times)
  (loop repeat times
	do
	(await (result (light-task))
	  (format t "task result: ~a~%" result))))

(defun async-test-4 (times)
  (bb:alet ((results (bb:all
		     (loop repeat times
			   collect
			   (await (result (light-task))
			     (format t "task result: ~a~%" result)
			     result)))))
    (format t "finished!!: ~a" results)))

(defun async-test-5 (times)
  (await (results (bb:all
		   (loop repeat times
			 collect
			 (await (result (light-task))
			   (format t "task result: ~a~%" result)
			   result))))
    (format t "finished!!: ~a" results)))

;;  workers for IO

(defvar *tasks* (sb-concurrency:make-mailbox :name "tasks"))

(defun start-io (function)
  (let ((finished nil)
	(result nil))
    (bb:create-promise
     (lambda (resolve reject)
       (declare (ignore reject))
       
       ;; Push task
       (sb-concurrency:send-message
	*tasks*
	(lambda ()
	  (setf result (funcall function))
	  (setf finished t)))
       
       ;; Push event
       (sb-queue:enqueue 
	(make-event :condition (lambda () finished)
		    :function (lambda ()
				(funcall resolve result)))
	*events*)))))

(defun run-worker (i)
  (loop
    do
       (let ((task (sb-concurrency:receive-message *tasks*)))
	 (funcall task))))

(defun start-worker (i)
  (bt:make-thread (lambda () (run-worker i))
		  :name (format nil "worker-~a" i)))

(defparameter *workers* nil)

(defun start-workers (n)
  (loop for i from 1 to n
	do (push (start-worker i) *workers*)))

(defun stop-workers ()
  (mapcan #'bt:destroy-thread *workers*)
  (setf *workers* nil))

(defun io-task ()
  (start-io (lambda ()
	      (let ((seconds (random 20)))
		(sleep seconds)
		seconds))))

(defun io-test-1 (n)
  (loop repeat n
	do
	(await (result (io-task))
	  (format t "task result: ~a~%" result))))

(defmacro with-capture-dynenv (vars &body body)
  (let ((lvars
	  (mapcar (lambda (var)
		    (gensym (format nil "~a-" var)))
		  vars)))
    `(let ,(loop for var in vars
		 for lvar in lvars
		 collect (list lvar var))
       (macrolet ((with-restored-dynenv (&body body)
		    `(let ,',(loop for var in vars
				for lvar in lvars
				collect (list var lvar))
		      ,@body)))
	 ,@body))))

(defvar *my-var*)

;; Without capturing dynamic bindings:

(defparameter *my-closure*
  (let ((*my-var* 22))
    (lambda ()
      (print *my-var*))))

(funcall *my-closure*) ;; => error. dynvar not bound! :)

;; Capturing dynamic bindings:
(defparameter *my-closure*
  (let ((*my-var* 22))
    (with-capture-dynenv (*my-var*)
      (lambda ()
	(with-restored-dynenv
	    (print *my-var*))))))

(funcall *my-closure*)
