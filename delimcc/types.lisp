(in-package :delimcc)

(defclass funcallable/cc (#+allegro c2mop:funcallable-standard-object)
  ((function :accessor f/cc-function
	     :initarg :function
	     :documentation "A continuation function."))
  (:metaclass c2mop:funcallable-standard-class)
  (:documentation "A structure that represents a funcallable object"))

(defmethod documentation ((obj funcallable/cc) (doc-type (eql 'function)))
  (documentation (f/cc-function obj) doc-type))

(defmethod (setf documentation) (value (obj funcallable/cc) (doc-type (eql 'function)))
  (setf (documentation (f/cc-function obj) doc-type) value))

(defmethod describe-object :after ((obj funcallable/cc) stream)
  (format stream "Function: ~A~%Documentation: ~A" (ignore-errors (f/cc-function obj))
          (documentation obj 'function)))

#+nil(defun make-funcallable (function)
  "Creates an instance of FUNCALLABLE/CC."
  (let ((inst (make-instance 'funcallable/cc :function function)))
    (c2mop:set-funcallable-instance-function
     inst
     #-cmu (lambda (&rest args)
	     (apply (f/cc-function inst) #'values args))
     #+cmu (compile nil
		    `(lambda (&rest args)
		       (apply (f/cc-function ,inst) #'values args))))
    inst))

(defun make-funcallable (function)
  "Creates an instance of FUNCALLABLE/CC."
  (let ((inst (make-instance 'funcallable/cc :function function)))
    (c2mop:set-funcallable-instance-function
     inst
     #-cmu (lambda (&rest args)
	     (apply (f/cc-function inst) args))
     #+cmu (compile nil
		    `(lambda (&rest args)
		       (apply (f/cc-function ,inst) args))))
    inst))

(declaim (inline fdesignator-to-function/cc))
(defun fdesignator-to-function/cc (fdesignator)
  "Converts a function designator to a function/cc."
  (let ((lookup (if (symbolp fdesignator)
		    (fdefinition fdesignator)
		    fdesignator)))
    (typecase lookup
      (funcallable/cc (f/cc-function lookup))
      (t (lambda(k &rest args)
	   (multiple-value-call k (apply lookup args)))))))

;; Wrap continuation objects

(defun return/cc (x)
  (make-funcallable
   (lambda (cont)
     (funcall cont x))))

(defmacro trigger/cc (what)
  `(make-funcallable
    (lambda (cont)
      (funcall cont (funcall (lambda () ,what))))))

(defun bind/cc (c f)
  (make-funcallable
   (lambda (cont)
     (funcall c (lambda (x)
                  (funcall (funcall f x) cont))))))

(serapeum:-> then/cc (funcallable/cc funcallable/cc) funcallable/cc)
(defun then/cc (c1 c2)
  (check-type c1 funcallable/cc)
  (check-type c2 funcallable/cc)
  (make-funcallable
   (lambda (cont)
     (funcall c1
              (lambda (x)
		(declare (ignore x))
		(funcall c2 cont))))))
