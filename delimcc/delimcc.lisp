(require :blackbird)

(defpackage :delimcc
  (:use :cl))

(in-package :delimcc)

(defun return/cc (x)
  (lambda (cont)
    (funcall cont x)))

(defun bind/cc (c f)
  (lambda (cont)
    (funcall c (lambda (x)
                 (funcall (funcall f x) cont)))))

(defun then/cc (c1 c2)
  (lambda (cont)
    (funcall c1
             (lambda (x)
               (declare (ignore x))
               (funcall c2 cont)))))

(funcall
 (then/cc (return/cc (progn (print "lala") 22))
          (return/cc (progn (print "gadg") 4545)))
 #'identity)

(return/cc 22)

(funcall (return/cc 33) #'identity)

(funcall (bind/cc (return/cc 23)
                  (lambda (x)
                    (return/cc (+ x 44))))
         #'identity)

(funcall (bind/cc (return/cc 23)
                  (lambda (x)
                    (bind/cc (return/cc (+ x 44))
                             (lambda (y)
                               (return/cc (+ y 44))))))
         #'identity)

(defmacro let/cc (bindings &body body)
  (labels ((expand-bind (bs)
             (if (not bs)
                 ;;`(return/cc (progn ,@body))
		 `(progn ,@body)
                 (let ((binding (first bs)))
                   `(bind/cc ,(second binding)
                             (lambda (,(first binding))
                               ,(expand-bind (rest bs))))))))
    (expand-bind bindings)))

(let ((cc
        (let/cc ((x (return/cc 44))
                 (y (return/cc (+ x 56))))
          (return/cc y))))
  (funcall cc #'identity))

(funcall (let/cc ((x (return/cc 44)))
           (return/cc (+ x 577)))
         #'identity)

(defun call/cc (f)
  (lambda (cont)
    (funcall f cont)))

(defparameter *cc* nil)

(let ((cc
        (let/cc ((x (return/cc 44))
                 (z (call/cc (lambda (cont) (setf *cc* cont) x)))
                 (y (return/cc (+ x 56))))
          (return/cc y))))
  (funcall cc #'identity))

(funcall *cc* nil)

(let* ((i 22))
  (let/cc ((x (return/cc 44))
           (z (call/cc (lambda (cont) (setf *cc* cont) x)))
           (y (return/cc (incf i (+ z 56)))))
    (return/cc y))
  (funcall *cc* #'identity))

;; reset (3 + shift (λk → 5∗2)) − 1

(defun eval/cc (cont)
  (funcall cont #'identity))


(defun reset (f)
  (eval/cc f))

(defun shift (f)
  (call/cc f))

(- (reset
    (let/cc
        ((x (return/cc 3))
         (y (shift (lambda (k)
                     (* 5 2)))))
      (return/cc (+ 3 y))))
   1)

(- (eval/cc
    (let/cc
        ((x (return/cc 3))
         (y (call/cc (lambda (k)
                       (declare (ignore k))
                       (* 5 2)))))
      (return/cc (+ x y))))
   1)

(- (eval/cc
    (let/cc
        ((x (return/cc 3))
         (y (call/cc (lambda (k)
                       (funcall k (* 5 2))))))
      (return/cc (+ x y))))
   1)

(defmacro defun/cc (name args &body body)
  `(defun ,name ,args
     (lambda (_cont)
       (funcall (progn ,@body) _cont))))

(defun/cc my-func (x y)
  (return/cc (+ x y)))

(defmacro progn/cc (&body body)
  (labels ((expand-bind (bs)
             (cond
	       ((= (length bs) 0)
		`(return/cc nil))
	       ((= (length bs) 1)
		(first bs))
	       (t
		`(then/cc ,(first bs)
                          ,(expand-bind (rest bs)))))))
    (expand-bind body)))

(progn/cc)
(progn/cc (return/cc 22))
(progn/cc (return/cc 44)
  (return/cc 5656))

(eval/cc
 (progn/cc (return/cc 44)
   (return/cc 5656)
   (return/cc 656)))

(eval/cc
 (progn/cc
   (return/cc 3434)
   (return/cc 4545)))

(eval/cc
 (progn/cc
   (return/cc (print "lala"))
   (return/cc (print "jojo"))))

(defun/cc abort/cc ()
  (call/cc (lambda (cont)
             (declare (ignore cont))
             (print "abort"))))

(eval/cc
 (progn/cc
   (return/cc (print "lala"))
   (abort/cc)
   (return/cc (print "jojo"))))

(defmacro trigger/cc (what)
  `(lambda (cont)
     (funcall cont (funcall (lambda () ,what)))))

(eval/cc
 (progn/cc
   (return/cc (print "lala"))
   (return/cc (print "5656"))
   (abort/cc)
   (return/cc (print "jojo"))))

(eval/cc
 (progn/cc
   (trigger/cc (print "lala"))
   (trigger/cc (print "5656"))
   (abort/cc)
   (trigger/cc (print "jojo"))))

(defun wrap/cc (function &rest args)
  (lambda (cont)
    (funcall cont (apply function args))))

;; (defmacro while/cc (condition &body body)
;;   (let ((tag (gensym "while/cc-")))
;;     `(tagbody ,tag
;;         (when (not ,condition)
;;           (return-from ,tag))
;;         ,@body
;;         (go ,tag))))

;; (defmacro while/cc (condition &body body)
;;   `(bind/cc (return/cc ,condition)
;; 	    (lambda (c)
;; 	      (if c
;; 		  (progn/cc
;; 		    ,@body
;; 		    (while/cc ,condition (progn/cc ,@body)))
;; 		  ;; else
;; 		  (return/cc nil)))))

(defmacro while/cc (condition &body body)
  `(while/cc* (lambda () ,condition)
	      (lambda () (progn/cc ,@body))))

(defun while/cc* (conditionf bodyf)
  (bind/cc (return/cc (funcall conditionf))
	   (lambda (c)
	     (if c
		 (progn/cc
		   (funcall bodyf)
		   (while/cc* conditionf bodyf))
		  ;; else
		  (return/cc nil)))))

(defmacro if/cc (condition then else)
  `(bind/cc (return/cc ,condition)
	    (lambda (x)
	      (if x ,then ,else))))

;; (defun iterate/cc (cont condition function)
;;   (if (funcall condition)
;;       (funcall function 
;;                (lambda (x)
;; 		 ;;(declare (ignore x))
;; 		 (print x)
;; 		 (iterate/cc cont condition function)))
;;       (funcall cont)))

(defun yield (x)
  (format t "yield: ~a~%" x)
  (call/cc (lambda (c)
             (cons x c))))

(defstruct (generator (:constructor make-generator%))
  value
  continuation)

(defun generate (generator)
  (destructuring-bind (r . cont)
      (funcall (generator-continuation generator) #'identity)
    (setf (generator-value generator) r)
    (setf (generator-continuation generator) cont)
    r))

(defun/cc fibonacci ()
  (let ((a 0)
        (b 1))
    (while/cc t
      (yield a)
      (lift/cc
	(setf b (+ a b))
	(setf a (- b a))))))

(defun make-generator (function)
  (make-generator% :value nil :continuation function))

(defparameter *f* (make-generator (fibonacci)))

(generate *f*)

(defparameter *fibers* nil)

(defstruct lock
  value)

(defstruct continuation
  value)

(defun fork/cc (function)
  (push function *fibers*)
  (return/cc nil))

(defmacro lambda/cc (args &body body)
  `(lambda ,args
     (progn/cc ,@body)))

(eval/cc
 (progn/cc
   (return/cc (print "lala"))
   (fork/cc (lambda/cc ()
	      (return/cc (print "concurrent"))))
   ))

(return/cc "lala")

(bb:promisify 22)

(let ((p (bb:create-promise
	  (lambda (resolve reject)
	    (funcall resolve "lala")))))
  (bb:attach p (lambda (x)
		 (print x))))

;; await simple attaches the current continuation to the returned promise
(defmacro await (what)
  `(let/cc ((promise (return/cc (the bb:promise ,what))))
     (call/cc (lambda (cont)
		(bb:attach promise cont)))))

(defun make-foo-promise ()
  (bb:promisify 22))

(eval/cc
 (progn/cc
   (await (make-foo-promise))
   (trigger/cc (print "lala"))))

(defparameter *effect* nil)

(defun make-effect ()
  (bb:create-promise
   (lambda (resolve reject)
     (declare (ignore reject))
     (setf *effect* resolve))))

(eval/cc
 (let/cc ((x (await (make-effect))))
   (trigger/cc (format t "~%effect!: ~a" x))))

 
(funcall *effect* "asdfsdf")

(defmacro lift/cc (&body body)
  `(lambda (cont)
     (funcall cont (funcall (lambda () ,@body)))))

(eval/cc
 (let/cc ((x (await (make-effect))))
   (lift/cc
     (print "effect triggered")
     (format t "~%effect!: ~a" x))))

(funcall *effect* "asdfsdf")

(defmacro await-bind (call &body body)
  `(let/cc ((,(first call) (await ,(second call))))
     ,@body))

(defmacro awaiting ((var call) &body body)
  `(let/cc ((,var (await ,call)))
     ,@body))

(eval/cc
 (await-bind (x (make-effect))
   (lift/cc
     (print "effect triggered")
     (print "lala")
     (format t "~%effect!: ~a" x))))

(funcall *effect* "asdfsdf")

(eval/cc
 (awaiting (x (make-effect))
   (lift/cc
     (print "effect triggered")
     (print "lala")
     (format t "~%effect!: ~a" x))))

(funcall *effect* "asdfsdf")

