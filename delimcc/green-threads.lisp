(in-package :delimcc)

(gt:make-thread
  (lambda ()
    (format t "Do Something~%")
    (gt:make-thread
      (lambda ()
        (format t "Do Something Else.~%")))
    (gt:queue-next ;; queue next action and yield
      (lambda ()
        (format t "Do More.~%")))))

(gt:make-thread
  (cl-cont:with-call/cc
    (lambda ()
      (format t "Do Something~%")
      (cl-cont:without-call/cc
        (gt:make-thread
          (cl-cont:with-call/cc
            (lambda ()
              (format t "Do Something Else~%")))))
      (gt:thread-yield) ;; allow other threads to run
      (format t "Do More~%"))))

(defun evaluator/cc (cont)
  (lambda ()
    (funcall cont #'identity)))

(gt:make-thread
 (evaluator/cc
  (progn/cc
    (trigger/cc (format t "Do Something~%"))
    (call/cc (lambda (cont)
               (gt:make-thread
		(evaluator/cc (trigger/cc (format t "Do Something Else~%"))))
	       ;; continue
	       (funcall cont nil)))
    (lift/cc
      (gt:thread-yield) ;; allow other threads to run
      (format t "Do More~%")))))

(gt:with-green-thread
  (format t "Do Something~%")
  (gt:with-green-thread
    (format t "Do Something Else~%"))
  ;;(gt:thread-yield) ;; allow other threads to run
  (format t "Do More~%"))

;; equivalent of with-green-thread
(defmacro gt/fork (&body body)
  `(call/cc (lambda (cont)
	      (gt:make-thread
	       (evaluator/cc (lift/cc ,@body)))
	      (funcall cont nil)
	      
	      )))

(gt:make-thread
 (evaluator/cc
  (progn/cc
    (trigger/cc (format t "Do Something~%"))
    (gt/fork
      (format t "Do Something Else~%"))
    (progn/cc
      ;;(trigger/cc (gt:thread-yield)) ;; allow other threads to run
      (trigger/cc (format t "Do More~%"))))))

(defparameter *a* nil)
(defparameter *b* nil)

(setf *a*
      (progn/cc
	(trigger/cc (format t "Do Something~%"))
	(call/cc (lambda (cont)
		   (setf *b*
			 (lift/cc (format t "Do Something Else~%")))
		   (funcall cont nil)))
	(progn/cc
	  (trigger/cc (format t "yield~%")) ;; allow other threads to run
	  (trigger/cc (format t "Do More~%")))))

(eval/cc *a*)
(eval/cc *b*)
