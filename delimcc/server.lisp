(require :chanl)

(defpackage :delimcc/server
  (:use :cl :delimcc))

(in-package :delimcc/server)

;; various background tasks managers (threads)
;; main loop pushes tasks to a queue, a promise object is generated.
;; background threads pop from that queue, and execute.
;; when task is done, thread pushes the its promise to a resolved queue.
;; main loop pops from resolved queue and executes promise (continuation)

;; something like that ...

(require :sb-concurrency)

;;(defparameter *tasks* (make-instance 'chanl:queue-channel))
(defparameter *tasks* (sb-concurrency:make-mailbox :name "tasks"))
;;(defparameter *done* (make-instance 'chanl:queue-channel))
(defparameter *done* (sb-concurrency:make-mailbox :name "done"))

(defparameter *workers* nil)
(defparameter *task-scheduler* nil)

(defun worker (name)
  (format t "Started worker: ~a~%" name)
  (loop while t
	do
	   (progn
	     (format t "worker ~a: waiting for task ..~%" name)
	     (let ((tsk (sb-concurrency:receive-message *tasks*)
		     #+nil(chanl:recv *tasks* :blockp t)))
	       (format t "worker ~a: received task: ~a~%"
		       name tsk)
	       (access:with-access (task resolve reject) tsk
		 (let ((result (funcall task)))
		   (format t "worker ~a: finished task: ~a with value: ~a~%"
			   name tsk result)
		   #+nil(chanl:send *done* (list :result result :resolve resolve :reject reject)
				    :blockp nil)
		   (sb-concurrency:send-message *done* (list :result result :resolve resolve :reject reject))
		   ))))))

(defun do-task (task)
  (let ((promise (bb:create-promise
		  (lambda (resolve reject)
		    (format t "Enqueuing task: ~a~%" task)
		    #+nil(chanl:send *tasks*
				(list :task task
				      :resolve resolve
				      :reject reject)
				:blockp nil)
		    (sb-concurrency:send-message *tasks*
		     (list :task task :resolve resolve
			   :reject reject)
		     )))))
    promise))

(defun task-scheduler ()
  (loop while t
	do
	   (let ((res #+nil(chanl:recv *done* :blockp t)
		      (sb-concurrency:receive-message *done*)))
	     (format t "scheduler: received result: ~a~%" res)
	     (access:with-access (result resolve reject) res
	       (funcall resolve res)))))

(defun start-task-scheduler ()
  (setf *task-scheduler* (bt:make-thread 'task-scheduler :name "task-scheduler")))

(defun start-workers (n)
  (dotimes (i n)
    (let ((name (format nil "worker-~a" i)))
      (push (bt:make-thread (lambda () (worker name))
			    :name name)
	    *workers*))))

(defun stop-task-scheduler ()
  (bt:destroy-thread *task-scheduler*))

(defun stop-workers ()
  (dolist (worker *workers*)
    (bt:destroy-thread worker))
  (setf *workers* nil))

(defun test-task-scheduler (n)
  (dotimes (i n)
    (let ((sleep (random 10))
	  (task-name (format nil "task-~a" i)))
      (let ((promise
	      (do-task (lambda ()
			 (format t "~a: sleeping ~a~%" task-name sleep)
			 (sleep sleep)))))
	(bb:attach promise
		   (lambda (&rest args)
		     (declare (ignore args))
		     (format t "~a: end of sleep~%" task-name)))))))


(defun simulate-io (&optional (duration (random 10)))
  (do-task (lambda ()
	     (format t "Simulating IO~%")
	     (sleep duration)
	     (format t "IO finished. Took: ~a seconds~%" duration)
	     duration)))

(delimcc::eval/cc
 (delimcc::await-bind (x (simulate-io))
   (format t "io finished: ~a~%" x)
   (print "trying again...")
   (delimcc::eval/cc
    (delimcc::await-bind (x (simulate-io))
      (format t "io finished 2: ~a~%" x)))))

(delimcc::eval/cc
 (delimcc::await-bind (x (simulate-io))
   (format t "io finished: ~a~%" x)))

(delimcc::eval/cc
 (delimcc::await-bind (x (simulate-io))
   (delimcc::progn/cc
     (delimcc::trigger/cc (format t "io finished: ~a~%" x))
     (delimcc::await-bind (x (simulate-io))
       (delimcc::trigger/cc (format t "io finished 2: ~a~%" x))))))

;; web server simulation

(defparameter *web-connection* (sb-concurrency:make-mailbox)
  "Receive requests")

;; we can imagine that request has a reference to the web connection socket
(defun make-request ()
  (let ((name (gensym "request-")))
    (lambda (result)
      (format t "Request resolved: ~a result: ~a~%" name result))))

(defun handle-request (request cont)
  (eval/cc
   (delimcc::await-bind (x (simulate-io))
   (delimcc::progn/cc
     (delimcc::trigger/cc (format t "io finished: ~a~%" x))
     (delimcc::await-bind (x (simulate-io))
       (delimcc::progn/cc
	 (delimcc::trigger/cc (format t "io finished 2: ~a~%" x))))))))
     

(defun start-server ()
  (loop while t
	do
	   (let ((request (sb-concurrency:receive-message *web-connection*)))
	     (handle-request request
			     (lambda (result)
			       (send-result request result))))))
