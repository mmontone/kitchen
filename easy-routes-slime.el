(require 'slime)

(defun easy-routes-show-tree ()
  (interactive)
  (let ((tree
	 (slime-eval '(cl:with-output-to-string (cl:*standard-output*)
						(cl:describe easy-routes:*routes-mapper*)))))
    (let ((buffer (get-buffer-create "*easy-routes*")))
      (with-current-buffer buffer
	(insert tree)
	(setq buffer-read-only t)
	(slime-mode))      
      (display-buffer buffer))))

(provide 'easy-routes)
