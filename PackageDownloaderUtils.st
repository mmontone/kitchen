packageFile _ CodePackageFile onFileEntry: '/home/marian/src/Cuis/Erudite/Erudite.pck.st' asFileEntry.
packageFile description.

Feature require: 'QuickViews'.

packageListBuilder _ PackageListBuilder new
	collectPackagesFromDisk ;
	yourself.
	


(QuickList on: (packageListBuilder packageList asSortedCollection: [:p1 :p2 | p1 packageName < p2 packageName]))
	actions: {'Install' -> [:package | package install].
			'Update package list' -> [:package | packageListBuilder clearPackageList; collectPackagesFromDisk]};
	printer: #packageName;
	detail: #description;
	filter: [:str :package | package packageName includesSubstring: str caseSensitive: false];
	openTitled: 'Package installer'.
	
packageListBuilder downloadPackagesFromUrls:
({
	{'name' -> 'AXAnnouncements'.
		'url' -> 'https://bitbucket.org/mmontone/mold/raw/master/AXAnnouncements.pck.st'}	
} collect: [:el | el asDictionary]).

urls _ packageListBuilder readUrlsFromFile: '/home/marian/src/Cuis/PackageInstaller/packages.csv'.

packageListBuilder downloadPackagesFromUrls: urls.

Json readFrom: (PackageListBuilder githubClient httpGet: 'https://api.github.com/repositories/11491290/contents/Packages/Theme-Themes.pck.st?ref=6a90a777c27914e46fec95ad7eabd3da20c55961') content readStream

Json readFrom: (PackageListBuilder githubClient httpGet: 'https://api.github.com/search/repositories?q=Cuis+language:Smalltalk') content readStream.

repos _ 
Json readFrom: (PackageListBuilder githubClient httpGet: 'https://api.github.com/search/repositories?q=Cuis+language:Smalltalk&page=2') content readStream.

"Fetch Cuis-Smalltalk organization repos"
repos _ 
Json readFrom: (PackageListBuilder githubClient httpGet: 'https://api.github.com/orgs/Cuis-Smalltalk/repos') content readStream.

"Fetch Cuis packages urls"
packageUrls _ 
repos collect: [:repo | |req|
	2 seconds asDelay wait.
	req _ PackageListBuilder githubGet: ('https://api.github.com/search/code?q=repo:{1}%20filename:.pck.st' format: {repo at: 'full_name'}).
	Json readFrom: req content readStream].

"Fetch Cuis Smalltalk github repos"
repos _ 
Json readFrom: (PackageListBuilder githubClient httpGet: 'https://api.github.com/search/repositories?q=Cuis+language:Smalltalk&page=1') content readStream.

"Fetch Cuis packages urls"
packageUrls _ 
repos items collect: [:repo | |req|
	2 seconds asDelay wait.
	req _ PackageListBuilder githubGet: ('https://api.github.com/search/code?q=repo:{1}%20filename:.pck.st' format: {repo at: 'full_name'}).
	Json readFrom: req content readStream].

packageListBuilder _ PackageListBuilder new
	collectPackagesFromDirectory: '/home/marian/src/Cuis/PackageInstaller/packages' asDirectoryEntry ;
	yourself.

downloadDir _ PackageListBuilder downloadDirectory .
PackageListBuilder new downloadPackagesFromUrls: urls  into:  downloadDir.

urls collect: [:url | | codePackage |
	codePackage _ CodePackageFile onFileEntry: (downloadDir // ((url at: 'name'),'.pck.st')).
	PackageSpec new
				name: (url at: 'name');
				downloadUrl: (url at: 'url');
				description: (String streamContents: 				[:s | 
					s nextPutAll: codePackage packageDescription;
						newLine;		newLine;
						nextPutAll: codePackage description]);
				requires: codePackage requires;
				provides: codePackage provides;
				yourself].
			
		
packageListBuilder _ PackageListBuilder new
	collectPackagesFromDirectory: downloadDir ;
	yourself.

"Write found packages on Github to file"	
'/home/marian/src/Cuis/PackageInstaller/github.csv' asFileEntry
	appendStreamDo: [:stream |
		packageUrls do: [:resp |
			resp items do: [:item | |downloadUrl name |
				name _ (item name subStrings: (Array with: $.)) first.
				downloadUrl _ getDownloadUrl  
								value: item repository 		full_name 
								value: item path
								value: item url.
				stream nextPutAll: name;
						nextPut: $,;
						nextPutAll: downloadUrl;
						newLine]]].
			
"Collect packages from cvs"

packageListBuilder _ PackageListBuilder new
	"collectRemotePackagesFromFile: '/home/marian/src/Cuis/PackageInstaller/github.csv' asFileEntry ;"
	collectRemotePackagesFromFile: '/home/marian/src/Cuis/PackageInstaller/packages.csv' asFileEntry ;
	yourself.		
	
packageListBuilder writePackagesListToJsonFile: '/home/marian/src/Cuis/Cuis-Smalltalk-Dev/packages.json' asFileEntry.
	
packageListBuilder _ PackageListBuilder new
	collectPackagesFromJsonFile: '/home/marian/src/Cuis/PackageInstaller/packages.json' asFileEntry ;
	yourself.				

(QuickList on: (packageListBuilder packageList asSortedCollection: [:p1 :p2 | p1 packageName < p2 packageName]))
	actions: {'Download' -> [:package | package download]};
	printer: #packageName;
	detail: #description;
	filter: [:str :package | package packageName includesSubstring: str caseSensitive: false];
	openTitled: 'Download package'.

				
uri _ 'https://api.github.com/repositories/131525469/contents/IA-EN-Dictionary.pck.st?ref=a5661d89f656457db6de19785f773441b7cac6a8'	.

'/home/marian/src/Cuis/PackageInstaller/packages.store'
	asFileEntry fileContents: 
(String streamContents: [:s |
	packageListBuilder packageList storeOn: s]).

'/home/marian/src/Cuis/PackageInstaller/packages.json'
	asFileEntry writeStreamDo: [:file |
		packageListBuilder packageList jsonWriteOn: file].

'/home/marian/src/Cuis/PackageInstaller/packages.json'
	asFileEntry readStreamDo: [:file |	
		Json readFrom: file].
	
downloader _ PackageDownloader new.

package _ downloader findPackageDescription: 'Mail-Remote'.

downloader calculatePackageDependenciesOf: package.

downloader packagesThatNeedDownload: (downloader calculatePackageDependenciesOf: package).

downloader downloadAndInstall: package.

PackageListBuilder updateGist: 'f43e8c36fa8d3954163289b40670b1d9'
	fileName: 'cuis-packages.json' 
	file: '/home/marian/src/Cuis/Cuis-Smalltalk-Dev/packages.json' asFileEntry. 
	
WebClient httpGet: 'https://raw.githubusercontent.com/Cuis-Smalltalk/Numerics/master/Math%203D.pck.st'.

url _ 'https://gist.githubusercontent.com/mmontone/f43e8c36fa8d3954163289b40670b1d9/raw/cuis-packages.csv?c=', 1000 atRandom printString.
csvFile _ '/home/marian/src/Cuis/packages.csv'.
jsonFile _  '/home/marian/src/Cuis/packages.json'.

"Download list of packages"
 
csvFile asFileEntry fileContents: (WebClient httpGet: url) content.
	
"Process the list of packages to generate json definitions"
packageListBuilder _ PackageListBuilder new
	collectRemotePackagesFromFile: csvFile asFileEntry ;
	yourself.		
	
packageListBuilder writePackagesListToJsonFile: jsonFile asFileEntry.

"Upload packages json file"
PackageListBuilder updateGist: 'f43e8c36fa8d3954163289b40670b1d9'
	fileName: 'cuis-packages.json' 
	file: jsonFile asFileEntry. 



