(defpackage :db-mapper
  (:use :cl)
  (:local-nicknames
   (:pm :postmodern))
  (:import-from :access :access)
  (:export
   :*tables*
   :table
   :find-table
   :insert-table-object
   :update-table-object
   :delete-table-object
   :update-object-sql
   :insert-field-sql
   :sql-value
   :object-field-value
   :load-table-object
   :load-table-data
   :fetch-table-object
   :load-table-field-value
   :map-table
   :create-schema
   :table-definition
   :field-definition))

(in-package :db-mapper)

(defun make-keyword (value)
  (intern (string-upcase value) :keyword))

(defun should-not-be-null (field)
  (error "~A(~A): Should not be null"
         (field-name field)
         (field-type field)))

(defparameter *tables* (make-hash-table :test #'equalp))

(defun find-table (name &optional (error-p t))
  (or (gethash name *tables*)
      (and error-p
           (error "Table ~A does not exist" name))))

(defclass table ()
  ((name :initarg :name
         :initform (error "Provide the table name")
         :accessor table-name)
   (fields :initarg :fields
           :initform nil
           :accessor table-fields)
   (options :initarg :options
            :initform nil
            :accessor table-options)))

(defmethod initialize-instance :after ((table table) &rest initargs)
  (declare (ignore initargs))
  (setf (gethash (table-name table) *tables*) table))

(defmethod print-object ((table table) stream)
  (print-unreadable-object (table stream :type t :identity t)
    (format stream "~A" (table-name table))))

(defmacro define-table (name fields &rest options)
  `(make-instance 'table
                  :name ',name
                  :fields ',fields
                  :options ',options))

(defun db-table-name (table)
  (let ((db-table (find :db-table (table-options table) :key #'car)))
    (or (and db-table (cadr db-table))
        (table-name table))))

(defun insert-object-sql (table object)
  (pm:sql-compile
   `(:insert-into ,(db-table-name table)
     :set
     ,@(loop for field in (table-fields table)
             when (field-insert-p field)
               appending
               (list (field-name field)
                     (insert-field-sql field object))))))

(defun insert-table-object (table object)
  (assert (null (object-field-value object 'id)))

  (let ((id-field (find-table-field table 'id)))
    (let ((id (pm:sequence-next (field-sequence id-field))))
      (funcall (field-writer id-field) id object)
      (handler-case
          (pm:execute (insert-object-sql table object))
        (error (e)
          (setf (object-field-value object 'id) nil)
          (error e)))))
  object)

(defun update-object-sql (table object)
  (pm:sql-compile
   `(:update ,(db-table-name table)
     :set
     ,@(loop for field in (table-fields table)
             when (field-update-p field)
               appending
               (list (field-name field)
                     (insert-field-sql field object)))
     :where (:= id ,(object-field-value object 'id)))))

(defun update-table-object (table object)
  (assert (object-field-value object 'id))
  (pm:execute (update-object-sql table object))
  object)

(defun delete-table-object (table object)
  (pm:execute
   (postmodern:sql
    (:delete-from (db-table-name table)
     :where (:= 'id (object-field-value object 'id))))))

(defun insert-field-sql (field object)
  (%insert-field-sql (second field)
                     field
                     object))

(defun find-table-field (table field-name)
  (find (string field-name)
        (table-fields table)
        :key (alexandria:compose #'symbol-name #'car)
        :test #'equalp))

(defun field-attribute (field attribute)
  (getf (cddr field) attribute))

(defun field-update-p (field)
  (destructuring-bind (name type &key (update t) &allow-other-keys) field
    (declare (ignore name))
    (and (not (member type '(many-to-many many-to-one one-to-many)))
         update)))

(defun field-insert-p (field)
  (destructuring-bind (name type &key (transient nil) &allow-other-keys) field
    (declare (ignore name))
    (not (or transient
             (member type '(many-to-many many-to-one one-to-many))))))

(defun field-accessor (field)
  (destructuring-bind (name type &key accessor &allow-other-keys) field
    (declare (ignore name type))
    accessor))

(defun field-reader (field)
  (destructuring-bind (name type &key reader &allow-other-keys) field
    (declare (ignore name type))
    reader))

(defun field-writer (field)
  (destructuring-bind (name type &key writer &allow-other-keys) field
    (declare (ignore name type))
    writer))

(defun field-slot (field)
  (destructuring-bind (name type &key slot &allow-other-keys) field
    (declare (ignore name type))
    slot))

(defun field-allow-null (field)
  (destructuring-bind (name type &key (allow-null t) &allow-other-keys) field
    (declare (ignore name type))
    allow-null))

(defun field-sequence (field)
  (destructuring-bind (name type &key sequence &allow-other-keys) field
    (declare (ignore name type))
    sequence))

(defun field-name (field)
  (first field))

(defun field-type (field)
  (second field))

(defun field-unique (field)
  (destructuring-bind (name type &key unique &allow-other-keys) field
    (declare (ignore name type))
    unique))

(defun field-primary-key (field)
  (destructuring-bind (name type &key primary-key &allow-other-keys) field
    (declare (ignore name type))
    primary-key))

(defun field-lazy-load-p (field)
  (destructuring-bind (name type &key lazy-load &allow-other-keys) field
    (declare (ignore name type))
    lazy-load))


(defun parse-gis-geography (string)
  "Parse a PostGIS geography. We assume ST_AsGeoJSON was used to fetch the GeoJSON"
  (json:decode-json-from-string string))

(defgeneric sql-value (value type)
  (:method (value type)
    value)
  (:method (value (type (eql 'keyword)))
    (assert (keywordp value))
    (symbol-name value))
  (:method (value (type (eql 'uuid)))
    (assert (or (stringp value)
                (typep value 'uuid:uuid)))
    (princ-to-string value))
  (:method (value (type (eql 'timestamp)))
    (cond
      ((integerp value)
       (net.telent.date:universal-time-to-rfc-date value))
      ((typep value 'local-time:timestamp)
       (local-time:format-timestring nil value))
      (t (error "~A is not a timestamp" value))))
  (:method (value (type (eql 'gis-geography)))
    (parse-gis-geography value))
  (:method (value (type (eql 'json)))
    (cond
      ((stringp value)
       value)
      ((listp value)
       (json:encode-json-to-string value))
      (t (error "~A is not valid JSON" value)))))

(defun object-field-value (object field)
  (let ((reader (symbol-function
                 (or (field-reader field)
                     (field-accessor field)
                     (first field)))))
    (funcall reader object)))

(defun (setf object-field-value) (new-value object field)
  (let ((writer (or (field-writer field)
                    (and (field-accessor field)
                         (fdefinition `(setf ,(field-accessor field)))
                         (fdefinition `(setf ,(first field)))))))
    (funcall writer new-value object)))

(defun compile-object-field-value (object field)
  (let ((accessor (or (field-reader field)
                      (field-accessor field)
                      (first field))))
    `(,accessor ,object)))

(defgeneric %insert-field-sql (field-type field object))

(defmethod %insert-field-sql ((field-type (eql 'serial)) field object)
  (let ((sequence (field-sequence field)))
    (or (object-field-value object field)
        (pm:sequence-next sequence))))

(defmethod %insert-field-sql ((field-type (eql 'integer)) field object)
  (let ((value (object-field-value object field)))
    (if value
        (progn
          (assert (integerp value))
          value)
        (if (field-allow-null field)
            :null
            (error "~A cannot be null in ~A"
                   (field-name field)
                   object)))))

(defmethod %insert-field-sql ((field-type (eql 'string)) field object)
  (let ((value (object-field-value object field)))
    (if value
        (progn
          (assert (stringp value))
          value)
        (if (field-allow-null field)
            :null
            (error "~A cannot be null in ~A"
                   (field-name field)
                   object)))))

(defmethod %insert-field-sql ((field-type (eql 'text)) field object)
  (let ((value (object-field-value object field)))
    (if value
        (progn
          (assert (stringp value))
          value)
        (if (field-allow-null field)
            :null
            (error "~A cannot be null in ~A"
                   (field-name field)
                   object)))))

(defmethod %insert-field-sql ((field-type (eql 'boolean)) field object)
  (let ((value (object-field-value object field)))
    (if value
        (progn
          (assert (typep value 'boolean))
          value))))

(defmethod %insert-field-sql ((field-type (eql 'keyword)) field object)
  (let ((value (object-field-value object field)))
    (if value
        (progn
          (assert (keywordp value))
          (symbol-name value))
        (if (field-allow-null field)
            :null
            (error "~A cannot be null in ~A"
                   (field-name field)
                   object)))))

(defmethod %insert-field-sql ((field-type (eql 'timestamp)) field object)
  (let ((value (object-field-value object field)))
    (if value
        (cond
          ((integerp value)
           (net.telent.date:universal-time-to-rfc-date value))
          ((typep value 'local-time:timestamp)
           (local-time:format-timestring nil value))
          (t (error "~A is not a timestamp" value)))
        (if (field-allow-null field)
            :null
            (error "~A cannot be null in ~A"
                   (field-name field)
                   object)))))

(defmethod %insert-field-sql ((field-type (eql 'gis-geography)) field object)
  (let ((value (object-field-value object field)))
    (if value
        (cond
          ((and (numberp (car value)) (numberp (cdr value)))
           (list :raw (format nil "ST_GeographyFromText('POINT(~A ~A)')"
                              (car value)
                              (cdr value))))
          ((string= (access value :type) "Point")
           (list :raw (format nil "ST_GeographyFromText('POINT(~A ~A)')"
                              (first (access value :coordinates))
                              (second (access value :coordinates)))))
          (t (error "Invalid GIS geography: ~A" value)))
        (if (field-allow-null field)
            :null
            (error "~A cannot be null in ~A"
                   (field-name field)
                   object)))))

(defmethod %insert-field-sql ((field-type (eql 'uuid)) field object)
  (let ((value (object-field-value object field)))
    (if value
        (progn
          (assert (or (stringp value)
                      (typep value 'uuid:uuid)))
          (princ-to-string value))
        (if (field-allow-null field)
            :null
            (error "~A cannot be null in ~A"
                   (field-name field)
                   object)))))

(defmethod %insert-field-sql ((field-type (eql 'json)) field object)
  (let ((value (object-field-value object field)))
    (if value
        (sql-value value 'json)
        (if (field-allow-null field)
            :null
            (error "~A cannot be null in ~A"
                   (field-name field)
                   object)))))

(defmethod %insert-field-sql ((field-type (eql 'object)) field object)
  (let ((value (object-field-value object field)))
    (if value
        (object-field-value value 'id)
        (if (field-allow-null field)
            :null
            (error "~A cannot be null in ~A"
                   (field-name field)
                   object)))))

(defmethod %insert-field-sql ((field-type (eql 'array)) field object)
  (let ((value (object-field-value object field)))
    (if value
        (progn
          (assert (or (listp value)
                      (vectorp value)))
          (etypecase value
            (list
             (coerce
              (mapcar (lambda (item)
                        (sql-value item (field-attribute field :type)))
                      value)
              'vector))
            (vector
             (coerce
              (loop for item across value
                    collect (sql-value item (field-attribute field :type)))
              'vector))))
        (if (field-allow-null field)
            :null
            (error "~A cannot be null in ~A"
                   (field-name field)
                   object)))))

(defun load-table-object (table object-or-class data)
  (let ((object (cond
                  ((symbolp object-or-class)
                   (allocate-instance (find-class object-or-class)))
                  ((typep object-or-class 'standard-class)
                   (allocate-instance object-or-class))
                  (t object-or-class))))
    (loop for field in (table-fields table)
          when (not (field-lazy-load-p field))
            do
               (let ((field-writer
                       (or (field-writer field)
                           (let ((field-slot (or (field-slot field)
                                                 (field-name field))))
                             (lambda (value object)
                               (setf (slot-value object field-slot)
                                     value))))))
                 (funcall field-writer (load-table-field field data) object)))
    object))

(defun load-table-data (table data)
  (loop for field in (table-fields table)
        collect (cons (field-name field)
                      (load-table-field field data))))

(defun table-primary-key (table)
  (loop for field in (table-fields table)
        when (field-attribute field :primary-key)
          collect field))

(defgeneric fetch-object (class &rest primary-key-value))

(defmethod fetch-table-object ((table symbol) class &rest primary-key-value)
  (apply #'fetch-table-object (find-table table) class primary-key-value))

(defmethod fetch-table-object ((table table) class &rest primary-key-value)
  "primary-key-value is a plist. Example: (:id 22)"
  (let ((data (pm:query
               (pm:sql-compile `(:select *
                                  :from ,(db-table-name table)
                                  :where (:and ,@(loop
                                                   for key in primary-key-value by #'cddr
                                                   for value in (cdr primary-key-value) by #'cddr
                                                   collect `(:= ,key ,value)))))
               :plist)))
    (when data
      (load-table-object
       table
       class
       data))))

#+nil(defun fetch-table-field (table field object)
       (let ((primary-key-value (table-primary-key table)))
         (query
          (sql-compile `(:select ,(field-name field)
                          :from ,(db-table-name table)
                          :where
                          (:and ,@(loop
                                    for field in primary-key
                                    collect `(:= ,(field-name field) ,value)))))
          :single)))

(defun compile-fetch-table-field (table field object)
  (let ((primary-key-value (loop for field in (table-primary-key table)
                                 appending `(,(field-name field)
                                             ,(compile-object-field-value object field)))))
    `(query
      (sql
       (:select ',(field-name field)
         :from ',(db-table-name table)
         :where
         (:and ,@(loop
                   for key in primary-key-value by #'cddr
                   for value in (cdr primary-key-value) by #'cddr
                   collect `(:= ',key ,value)))))
      :plist)))

(defmethod load-table-field-value ((type (eql 'string)) value)
  (string value))

(defmethod load-table-field-value ((type (eql 'text)) value)
  (string value))

(defmethod load-table-field-value ((type (eql 'keyword)) value)
  (make-keyword value))

(defmethod load-table-field-value ((type (eql 'integer)) value)
  (assert (integerp value))
  value)

(defmethod load-table-field-value ((type (eql 'serial)) value)
  (assert (integerp value))
  value)

(defmethod load-table-field-value ((type (eql 'boolean)) value)
  (if (and value (not (equalp value :null)))
      t nil))

(defmethod load-table-field-value ((type (eql 'uuid)) value)
  (if (stringp value) value
      (uuid:make-uuid-from-string (string value))))

(defmethod load-table-field-value ((type (eql 'timestamp)) value)
  (cond
    ((numberp value) (local-time:universal-to-timestamp value))
    ((stringp value) (local-time:parse-timestring value))
    (t (error "Cannot read timestamp: ~A" value))))

(defmethod load-table-field-value ((type (eql 'gis-geometry)) value)
  (parse-gis-geography value))

(defmethod load-table-field-value ((type (eql 'json)) value)
  (assert (stringp value))
  (json:decode-json-from-string value))

(defun table-field-value (field data)
  (getf data (make-keyword (string (field-name field)))))

(defun load-table-field (field data)
  (%load-table-field (field-type field) field data))

(defun compile-load-table-field (field data)
  `(%load-table-field ',(field-type field) ',field ,data))

(defmethod %load-table-field ((field-type (eql 'string)) field data)
  (let ((value (table-field-value field data)))
    (if (and value (not (equalp value :null)))
        (progn
                                        ;(assert (stringp value))
          value)
        (if (field-allow-null field)
            nil
            (should-not-be-null field)))))

(defmethod %load-table-field ((field-type (eql 'text)) field data)
  (let ((value (table-field-value field data)))
    (if (and value (not (equalp value :null)))
        (progn
          (assert (stringp value))
          value)
        (if (field-allow-null field)
            nil
            (should-not-be-null field)))))

(defmethod %load-table-field ((field-type (eql 'integer)) field data)
  (let ((value (table-field-value field data)))
    (if (and value (not (equalp value :null)))
        (progn
          (assert (integerp value))
          value)
        (if (field-allow-null field)
            nil
            (should-not-be-null field)))))

(defmethod %load-table-field ((field-type (eql 'serial)) field data)
  (let ((value (table-field-value field data)))
    (if (and value (not (equalp value :null)))
        (progn
          (assert (integerp value))
          value)
        (if (field-allow-null field)
            nil
            (should-not-be-null field)))))

(defmethod %load-table-field ((field-type (eql 'boolean)) field data)
  (let ((value (table-field-value field data)))
    (if (and value (not (equalp value :null)))
        t nil)))

(defmethod %load-table-field ((field-type (eql 'object)) field data)
  (let ((value (table-field-value field data)))
    (if (and value (not (equalp value :null)))
        (progn
          (assert (integerp value))
          (if (getf field :loader)
              (funcall (getf field :loader) value)
              ;; eager loading:
              (progn
                (fetch-object (field-attribute field :type) :id value))))
        (if (field-allow-null field)
            nil
            (should-not-be-null field)))))

(defmethod %load-table-field ((field-type (eql 'timestamp)) field data)
  (let ((value (table-field-value field data)))
    (if (and value (not (equalp value :null)))
        (cond
          ((numberp value) (local-time:universal-to-timestamp value))
          ((stringp value) (local-time:parse-timestring value))
          (t (error "Cannot read timestamp: ~A" value)))
        (if (field-allow-null field)
            nil
            (should-not-be-null field)))))

(defmethod %load-table-field ((field-type (eql 'gis-geography)) field data)
  (let ((value (table-field-value field data)))
    (if (and value (not (equalp value :null)))
        (json:decode-json-from-string
         (caar (pm:query (format nil "SELECT ST_AsGeoJSON('~A')" value))))
        (if (field-allow-null field)
            nil
            (should-not-be-null field)))))

(defmethod %load-table-field ((field-type (eql 'uuid)) field data)
  (let ((value (table-field-value field data)))
    (if (and value (not (equalp value :null)))
        (if (stringp value) value
            (uuid:make-uuid-from-string (string value)))
        (if (field-allow-null field)
            nil
            (should-not-be-null field)))))

(defmethod %load-table-field ((field-type (eql 'keyword)) field data)
  (let ((value (table-field-value field data)))
    (if (and value (not (equalp value :null)))
        (progn
          (assert (stringp value))
          (make-keyword value))
        (if (field-allow-null field)
            nil
            (should-not-be-null field)))))

(defmethod %load-table-field ((field-type (eql 'array)) field data)
  (let ((value (table-field-value field data)))
    (if (and value (not (equalp value :null)))
        (progn
                                        ;(assert (vectorp value))
          (map 'vector (lambda (x)
                         (load-table-field-value
                          (field-attribute field :type)
                          x)) value))
        (if (field-allow-null field)
            nil
            (should-not-be-null field)))))

(defmethod %load-table-field ((field-type (eql 'json)) field data)
  (let ((value (table-field-value field data)))
    (if (and value (not (equalp value :null)))
        (progn
          (assert (stringp value))
          (json:decode-json-from-string value))
        (if (field-allow-null field)
            nil
            (should-not-be-null field)))))

(defmethod %load-table-field ((field-type (eql 'many-to-many)) field data)
  nil)

(defmethod %load-table-field ((field-type (eql 'one-to-many)) field data)
  nil)

(defmethod %load-table-field ((field-type (eql 'many-to-one)) field data)
  nil)

;; Map a table to a class
(defmacro map-table (table class &rest options &key
                                                 (create-p t)
                                                 (update-p t)
                                                 (delete-p t)
                                                 (fetch-p t)
                                                 (getters-p t))
  `(progn
     ,@(when create-p
         (list
          `(defmethod create ((,class ,class) &rest args)
             (declare (ignore args))
             (insert-table-object (find-table ',table) ,class))))
     ,@(when update-p
         (list `(defmethod update ((,class ,class) &rest args)
                  (declare (ignore args))
                  (update-table-object (find-table ',table) ,class))))
     ,@(when delete-p
         (list `(defmethod delete-object ((,class ,class))
                  (delete-table-object (find-table ',table) ,class))))
     ,@(when fetch-p
         (list `(defmethod fetch-object ((,class (eql ',class)) &rest key)
                  (apply #'fetch-table-object ',table ,class key))))
     ,@(let ((table-instance (find-table table)))
         (when getters-p
           (loop for field in (table-fields table-instance)
                 appending
                 (let ((getter (generate-db-getter field class (find-table table))))
                   (when getter
                     (list getter))))))))

(defun generate-db-getter (field class table)
  (%generate-db-getter (field-type field) field class table))

(defmethod %generate-db-getter (field-type field class table)
  ;; Only generate a getter for a non relation field when it is
  ;; marked as lazy. Otherwise, the slot is populated when the object is loaded.
  (when (field-lazy-load-p field)
    (let ((accessor-name (or (field-attribute field :accessor)
                             (field-name field)))
          (slot-name (or (field-attribute field :slot)
                         (field-name field)))
          (field-value (gensym "FIELD-VALUE-"))
          (field-writer
            (or (and (field-writer field)
                     `(function ,(field-writer field)))
                (let ((field-slot (or (field-slot field)
                                      (field-name field))))
                  `(lambda (value object)
                     (setf (slot-value object ',field-slot)
                           value))))))
      `(defmethod ,accessor-name ((,class ,class))
         (if (and (slot-boundp ,class ',slot-name)
                  (slot-value ,class ',slot-name))
             ;; Return the slot data
             (slot-value ,class ',slot-name)
             ;; else
             ;; Load data for the slot
             (let ((,field-value ,(compile-load-table-field
                                   field
                                   (compile-fetch-table-field table field class))))
               (funcall ,field-writer ,field-value ,class)))))))

(defmethod %generate-db-getter ((field-type (eql 'object)) field class table)
  (let ((object-type (field-attribute field :type)))
    (let ((object-table (find-table object-type)))
      (assert object-table nil "~A table does not exist" object-type)
      (let ((accessor-name (or (field-attribute field :accessor)
                               (field-name field)))
            (slot-name (or (field-attribute field :slot)
                           (field-name field))))
        `(defmethod ,accessor-name ((,class ,class))
           (if (integerp (slot-value ,class ',slot-name))
               (let ((id (slot-value ,class ',slot-name)))
                 (setf (slot-value ,class ',slot-name)
                       (fetch-object ',object-type :id id)))
                                        ; else
               (slot-value ,class ',slot-name)))))))

(defun sql-at (table attribute)
  (intern (format nil "~A.~A" table attribute)))

(defun build-query (query &key order-by limit)
  (let ((q query))
    (when order-by
      (setq q `(:order-by ,q ,@order-by)))
    (when limit
      (setq q `(:limit ,q ,@limit)))
    q))

(defmethod %generate-db-getter ((field-type (eql 'many-to-many)) field class table)
  (let ((object-type (field-attribute field :type))
        (reference (field-attribute field :reference))
        (through (field-attribute field :through))
        (back-reference (field-attribute field :back-reference)))
    (let ((object-table (find-table object-type))
	  (accessor-name (or (field-attribute field :accessor)
                             (field-name field))))
      `(defmethod ,accessor-name ((,class ,class) &key where limit order-by)
         (let ((table (find-table ',object-type))
	       (select-query `(:select ,',(sql-at (db-table-name object-table) *)
				:from
				,',(table-name object-table)
				,',through
				:where (:and (:= ,',(sql-at through back-reference)
						 (id ,,class))
					     (:= ,',(sql-at through reference)
						 ,',(sql-at (table-name object-table) 'id))
					     ,@where))))
           (mapcar (lambda (data)
                     (load-table-object table ',object-type data))
                   (query
                    (postmodern:sql-compile (build-query select-query
							 :order-by order-by
							 :limit limit))
                    :plists)))))))

(defun sql-order-by (query &rest exprs)
  (let ((exprs (remove-if #'null exprs)))
    (or (and exprs
             (list* :order-by query exprs))
        query)))

(defun sql-limit (query amount &optional offset)
  (or
   (and (or amount offset)
        (list :limit query amount offset))
   query))

(defmethod %generate-db-getter ((field-type (eql 'many-to-one)) field class table))

(defmethod %generate-db-getter ((field-type (eql 'one-to-many)) field class table)
  (let ((object-type (field-attribute field :type))
        (back-reference (field-attribute field :back-reference)))
    (let ((object-table (find-table object-type)))
      (assert object-table nil "~A table does not exist" object-type)
      (let ((accessor-name (or (field-attribute field :accessor)
                               (field-name field))))
        `(defmethod ,accessor-name ((,class ,class) &key order-by limit offset)
           (let ((table (find-table ',object-type)))
             (mapcar (lambda (data)
                       (load-table-object table ',object-type data))
                     (query
                      (postmodern:sql-compile
                       (sql-limit
                        (apply #'sql-order-by
                               `(:select ,',(sql-at (db-table-name object-table) '*)
                                  :from ,',(db-table-name object-table)
                                  :where (:= ,',(sql-at (db-table-name object-table) back-reference)
                                             ,(id ,class)))
                               order-by)
                        limit))
                      :plists))))))))

(defun create-schema ()
  (loop
    :for table :being :the :hash-values :of *tables*
    :do
       (pm:execute (table-definition table))))

(defun schema-definition ()
  (with-output-to-string (s)
    (loop
      :for table :being :the :hash-values :of *tables*
      :do
         (write-string (table-definition table) s)
         (write-string ";" s)
         (terpri s))))

(defun table-definition (table)
  (pm:sql-compile
   `(:create-table
     ,(db-table-name table)
     ,(loop
        :for field :in (table-fields table)
        :for field-definition = (field-definition field)
        :when field-definition
          :collect field-definition)
     ,@(table-options table))))


(defun field-definition (field)
  (when (not (member (field-type field)
                     '(one-to-many many-to-many)))
    `(,(field-name field)
      :type ,(field-sql-type field)
      :unique ,(field-unique field)
      :primary-key ,(field-primary-key field)
                                        ;,@(when (eql (field-type field) 'object)
                                        ;       `(:references (,(getf field :type))))
      )))

(defun field-sql-type (field)
  (if (field-allow-null field)
      `(or db-null ,(%field-sql-type (field-type field) field))
      (%field-sql-type (field-type field) field)))

(defmethod %field-sql-type (type field)
  type)

(defmethod %field-sql-type ((type (eql 'object))
                            field)
  'integer)

(defmethod %field-sql-type ((type (eql 'uuid))
                            field)
  'string)

(defmethod %field-sql-type ((type (eql 'array))
                            field)
  `(array ,(%field-sql-type (field-attribute field :type) nil)))


(defmethod %field-sql-type ((type (eql 'keyword))
                            field)
  'string)
