(cl:defpackage :stacklang
  (:use :cl))

(in-package :stacklang)

(defparameter *definitions* (make-hash-table))
(defparameter *stack* nil)

(defmacro push-last (x stack)
  `(if (zerop (length ,stack))
       (push ,x ,stack)
       (setf (cdr (last ,stack))
	     (cons ,x nil))))

(defmacro stack-push (x)
  `(if (zerop (length *stack*))
       (push ,x *stack*)
       (setf (cdr (last *stack*))
	     (cons ,x nil))))

(defmacro stack-pop ()
  `(pop *stack*))

(defmacro stack (&rest words)
  "Evalute WORDS in stack language."
  `(let ((*stack* nil))
     (stack-eval ',words)))

(defmacro define (name words)
  `(set-word ',name
	     (lambda ()
	       (stack-eval ',words))))

(defmacro constant (name value))

(defun set-word (word function)
  (setf (getf (symbol-plist word) :word-def) function))

(defun get-word (word)
  (getf (symbol-plist word) :word-def))

(set-word 'cr (lambda () (terpri)))
(set-word 'star (lambda () (write-char #\*)))

(defun stack-eval (words)
  (dolist (word words)
    (cond
      ((or (integerp word) (stringp word))
       (stack-push word))
      ((and (symbolp word) (get-word word))
       (let ((val (funcall (get-word word))))
	 (when val
	   (setf *stack* nil)
	   (stack-push val))
	 val))
      ((and (symbolp word) (ignore-errors (symbol-function word)))
       (let ((val (apply (symbol-function word) (reverse *stack*))))
	 (setf *stack* nil)
	 (when (not (null val))
	   (if (listp val)
	       (dolist (x val)
		 (stack-push x))
	       (stack-push val)))
	 val))
      (t (error "Could not evaluate: ~a" word))))
  *stack*)

(define lift
  (hand open arm lower hand close arm raise))

(define if-example
  ())

;; st = Starting Forth
(define st-1
  (cr star cr star cr star))

(stack cr star cr star cr star)

(stack 2 3 4 5 cl:- 45 cl:+)

(define four-more
  (4 cl:+))

(stack 4 four-more)

(set-word 'dot (lambda ()
		 (print (stack-pop))
		 nil))

(stack 1 2 3 dot s dot s dot)
(stack 1 2 + 3 +)

(stack 1 2 cl:/)

;; n1 n2 -- n2 n1
(set-word 'swap (lambda ()
		  (let ((x (stack-pop))
			(y (stack-pop)))
		    (stack-push y)
		    (stack-push x))
		  nil))
;; n --
(set-word 'drop (lambda ()
		  (stack-pop)
		  nil))

(set-word 'dup (lambda ()
		 (let ((n (first *stack*)))
		   (stack-push n))
		 nil))

(set-word 's (lambda ()
	       (print *stack*)
	       nil))

(stack 1 2 3 s dot s)

(stack 1 2 s drop s drop s)

(stack { 2 < 3 } if "lala" print else "lolo" print then)

(stack { 2 3 4 } cl:+)
