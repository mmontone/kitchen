(defpackage simple-matcher
  (:use :cl)
  (:export #:match
           #:when-match
           #:if-match
           #:unless-match
           #:ematch
           #:let-match
           #:with-match
           #:lambda-match
           #:plist
           #:alist))

(in-package :simple-matcher)

(defvar *matchers* (make-hash-table))

(defun expand-matcher (pattern value body)
  (cond
    ((and (symbolp pattern)
          (string= (symbol-name pattern) "_"))
     `(progn ,@body))
    ((eq pattern nil)
     `(if (null ,value)
          (progn ,@body)
          (no-match)))
    ((symbolp pattern)
     `(let ((,pattern ,value))
        ,@body))
    ((numberp pattern)
     `(if (and (numberp ,value)
               (= , pattern ,value))
          (progn ,@body)
          (no-match)))
    ((stringp pattern)
     `(if (and (stringp ,value)
               (string= , pattern ,value))
          (progn ,@body)
          (no-match)))
    ((characterp pattern)
     `(if (and (characterp ,value)
               (char= , pattern ,value))
          (progn ,@body)
          (no-match)))
    ((functionp pattern)
     `(if (funcall ,pattern ,value)
          (progn ,@body)
          (no-match)))
    ((and (listp pattern)
          (eq (car pattern) 'cl:function))
     `(if (funcall ,pattern ,value)
          (progn ,@body)
          (no-match)))
    ((and (listp pattern)
          (not (null pattern)))
     (let ((expander (gethash (car pattern) *matchers*)))
       (if expander
           (funcall expander pattern value body)
           (error "Invalid pattern: ~s" pattern))))
    (t (error "Invalid pattern: ~s" pattern))))

;; (expand-matcher 'x '(list 1 2 3)
;;                 '((print x)))

;; (expand-matcher '(cons x y) '(cons 22 34)
;;                 '((list x y)))

;; (expand-matcher '(cons x (cons y nil)) '(cons 22 34)
;;                 '((list x y)))

;; A matcher is a predicate + a lambda expression for binding

(defun cons-matcher (pattern value body)
  (destructuring-bind (car cdr) (cdr pattern)
    (let (($value (gensym "value")))
      `(let ((,$value ,value))
         (if (consp ,$value)
             ,(expand-matcher car `(car ,$value)
                              (list (expand-matcher cdr `(cdr ,$value)
                                                    body)))
             (no-match))))))

(setf (gethash 'cons *matchers*) 'cons-matcher)

(defmacro when-match (value pattern &body body)
  `(block matcher
     (flet ((no-match ()
              (return-from matcher nil)))
       ,(expand-matcher pattern value body))))

#+nil(when-match (list 1 2) (cons x y)
       (list x y))

#+nil(when-match 22 (cons x y)
       (list x y))

(defmacro unless-match (value pattern &body body)
  `(block matcher
     (flet ((no-match ()
              (return-from matcher (progn ,@body))))
       ,(expand-matcher pattern value nil))))

#+nil(unless-match (list 1 2 3) "asdf"
       "no")

#+nil(unless-match (list 1 2 3) (list _ _ _)
       "no")

(defmacro defmatcher (name (pattern value body) &body expander-body)
  (let ((matcher-name (intern (concatenate 'string (symbol-name name) "-MATCHER"))))
    `(progn
       (defun ,matcher-name (,pattern _value ,body)
         (let ((value (gensym "value")))
           `(let ((,,value ,_value))
              ,,@expander-body)))
       (setf (gethash ',name *matchers*) ',matcher-name))))

(defmatcher list (pattern value body)
  `(if (and (listp ,value)
            (= (length ,value) ,(length (cdr pattern))))
       ,(expand-matcher (cadr pattern) `(car ,value)
                        (if (null (cddr pattern))
                            body
                            (list (expand-matcher
                                   `(list ,@(cddr pattern))
                                   `(cdr ,value)
                                   body))))
       (no-match)))

#+nil(when-match  (list 1 2) (list x y)
       (list y x))

#+nil(expand-matcher '(list x y) '(list 1 3 4)
                     '((print x)))

(defmatcher list* (pattern value body)
  `(if (listp ,value)
       ,(cond
          ((zerop (length (cdr pattern)))
           (error "Bad pattern"))
          ((= (length (cdr pattern)) 1)
           ;; last element of list*
           (expand-matcher #'listp value
                           `((let ((,(cadr pattern) ,value))
                               ,@body))))
          (t
           (expand-matcher (cadr pattern) `(car ,value)
                           (list (expand-matcher `(list* ,@(cddr pattern))
                                                 `(cdr ,value)
                                                 body)))))
       (no-match)))

(defmatcher not (pattern value body)
  `(block not-matcher
     (flet ((no-match ()
              ;; match successful
              (return-from not-matcher ,@body)))
       ,(expand-matcher (cadr pattern) value nil))
     (no-match)))

#+nil(when-match  (list 1 2 3) (list* x y z)
       (list z y x))

(defmacro if-match (value pattern then else)
  `(block if-matcher
     (block matcher
       (flet ((no-match ()
                (return-from matcher nil)))
         (let ((res ,(expand-matcher pattern value (list then))))
           (return-from if-matcher res))))
     ,else))

#+nil(if-match (cons 1 2) (cons x y)
       (list x y)
       "no")

#+nil(if-match (cons 1 2) (list x y)
       (list x y)
       "no")

(defmacro match (value &body clauses)
  (if (null clauses)
      'nil
      (destructuring-bind (pattern &rest code) (first clauses)
        `(if-match ,value ,pattern
           (progn ,@code)
           (match ,value ,@(rest clauses))))))

#+nil(match (cons 1 2))

#+nil(match (cons 1 2)
       ((list x y) (list x y)))

#+nil(match (cons 1 2)
       ((list x y) (list y x))
       ((cons x y) (cons y x)))

(defmacro ematch (value &body clauses)
  `(match ,value
     ,@clauses
     (_ (error "No match"))))

#+nil(ematch (list 1 2)
       ((list x y z) "lala"))

#+nil(ematch (list 1 2)
       ((list x y z) "lala")
       (_ "ok"))

(defmacro let-match (bindings &body body)
  (if (null bindings)
      `(progn ,@body)
      (destructuring-bind (pattern value) (car bindings)
        `(ematch ,value
           (,pattern
            (let-match ,(cdr bindings)
              ,@body))))))

#+nil(let-match (((list x y) (list 1 2)))
       (list y x))

#+nil(let-match (((list x y) (list 1 2 3)))
       (list y x))

#+nil(let-match (((list x y) (list 1 2))
                 ((cons z w) (list 1 2 3 4)))
       (list y x w z))

(defmacro with-match (pattern value &body body)
  `(if-match ,value ,pattern
     (progn ,@body)
     (error "No match")))

#+nil(with-match (list _ x _) (list 1 2 3)
       (print x))

(defmatcher alist (pattern value body)
  (let ((bindings (cdr pattern)))
    `(if (listp ,value)
         (let ,(loop for (key . var) in bindings
                     collect `(,var (cdr (assoc ,key ,value))))
           ,@body)
         (no-match))))

#+nil(with-match (alist (:x . x)) '((:x . 22) (:y . 23))
       x)

#+nil(with-match (alist (:z . z)) '((:x . 22) (:y . 23))
       z)

#+nil(with-match (alist (:y . y)) '((:x . 22) (:y . 23))
       y)

(defmatcher plist (pattern value body)
  (let ((bindings (cdr pattern)))
    `(if (listp ,value)
         (let ,(loop for key in bindings by #'cddr
                     for var in (cdr bindings) by #'cddr
                     collect `(,var (getf ,value ,key)))
           ,@body)
         (no-match))))

#+nil(with-match (plist :x x) '(:x 22 :y 23)
       x)

#+nil(with-match (plist :z z) '(:x 22 :y 23)
       z)

#+nil(with-match (plist :y y) '(:x 22 :y 23)
       y)

(defmacro lambda-match (patterns &body body)
  (let ((args (mapcar (lambda (pat)
                        (declare (ignore pat))
                        (gensym "ARG-"))
                      patterns)))
    (labels ((expand-arg-matcher (patterns args)
               (if (zerop (length patterns))
                   `(progn ,@body)
                   `(ematch ,(car args)
                      (,(car patterns)
                       ,(expand-arg-matcher (cdr patterns)
                                            (cdr args)))))))
      `(lambda ,args
         ,(expand-arg-matcher patterns args)))))

#+nil(funcall
      (lambda-match ((cons x y))
        (list y x))
      (cons 1 2))

#+nil(funcall
      (lambda-match ((cons x y) (plist :z z))
        (list y x z))
      (cons 1 2)
      '(:z 22))

#+nil(funcall
      (lambda-match (x (cons y z))
        (list x y z))
      45 (cons 1 2))

;; Should error:
#+nil(funcall
      (lambda-match (x (cons y z))
        (list x y z))
      45 33)
