(defun create-tool-bar-map-from-spec (spec &optional default-map &rest defaults)
  "Create a tool-bar map from SPEC.
DEFAULT-MAP is the default key-map to use when it is not explicitely specified in SPEC.
DEFAULTS can contain certain defaults, like :vert-only."
  (let ((map (make-sparse-keymap)))
    (dolist (item spec)
      (let* ((command (cond
                       ((symbolp item)
                        item)
                       ((listp item)
                        (first item))
                       (t (error "Invalid item: %s" item))))
             (separator-p (eql item '|))
             (from-map (or (and (listp item)
                                (let ((map-symbol (cl-getf (rest item) :map)))
                                  (and map-symbol (eval map-symbol))))
                           default-map))
             (icon (or separator-p
		       (and (listp item)
                            (cl-getf (rest item) :icon))))
	     (help (and (listp item)
			(member :help (rest item))
			(cl-getf (rest item) :help)))
	     (vert-only (or
			 (and (listp item)
			      (member :vert-only (rest item))
			      (cl-getf (rest item) :vert-only))
			 (cl-getf defaults :vert-only))))
        (if separator-p
            (define-key-after map (make-vector 1 (gensym)) menu-bar-separator)
	  ;; See 23.18.1.2 Extended Menu Items for the list of available
	  ;; properties in tool-bar-local-item
	  ;; See 23.18.6 Tool bars
	  (if from-map
	      (tool-bar-local-item-from-menu command icon map from-map
					     :help help
					     :vert-only vert-only)
	    (tool-bar-local-item icon command command map
				 :vert-only vert-only
				 :help help)))))
    map))

(defvar elisp-tool-bar-spec
  '(;; evaluation
    (eval-defun :icon "evaluate"
		:help "Evaluate top-level form (definition)")
    (eval-last-sexp :icon "evaluate"
		    :help "Evaluate expression")
    (eval-print-last-sexp :icon "evaluate"
			  :help "Eval and print expression")
    (inspect-last-sexp :icon "inspect"
		       :help "Evaluate and inspect expression")
    | ;; load
    (load-file :icon "load-file" :map nil
	       :help "Load file")
    | ;; navigation
    (xref-find-definitions :icon "jump-to"
			   :help "Go to definition")
    (describe-symbol :icon "help"
		     :help "Show help on symbol")
    (forward-sexp :icon "down"
		  :help "Navigate sexp forward")
    (backward-sexp :icon "up"
		   :help "Navigate sexp backwards")))

(add-hook 'emacs-lisp-mode-hook
	  (lambda ()
	    (setq-local tool-bar-map 
			(create-tool-bar-map-from-spec elisp-tool-bar-spec nil))))

(provide 'elisp-toolbar)
